package chapter07.chapter07_V2;

import static java.lang.System.out;

// bad example
public class PollingWithSleepT01 {

    private static int counter = 0;

    public static void main(String[] args) {
        new Thread(() -> {
            for (int i = 0; i < 500; i++) {
                counter++;
            }
        }).start();

        out.println(counter);
        while (counter < 100) {
            out.println("Not reached 100 yet");
        }
        out.println("Reached " + counter);
    }
}
