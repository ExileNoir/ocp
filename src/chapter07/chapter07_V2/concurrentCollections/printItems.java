package chapter07.chapter07_V2.concurrentCollections;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

public class printItems {
    public static void main(String[] args) throws InterruptedException {

        final BlockingDeque<Integer> deque = new LinkedBlockingDeque<>();
        deque.offer(103);
        deque.offerFirst(20,1, TimeUnit.SECONDS);
        deque.offerLast(85,7,TimeUnit.HOURS);

        System.out.println(deque.pollFirst(200,TimeUnit.NANOSECONDS));
        System.out.println(deque.pollLast(1,TimeUnit.MINUTES));
    }

}
