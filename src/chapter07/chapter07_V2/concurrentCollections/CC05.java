package chapter07.chapter07_V2.concurrentCollections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.lang.System.out;

public class CC05 {
    public static void main(String[] args) {

        // synchronize access to the data elements, such as get() && set()
        final List<Integer> list = Collections.synchronizedList(new ArrayList<>(Arrays.asList(4, 3, 92)));
        // they do NOT synchronize access on any Iterator, hence imperative to use sync block on returned collection
        synchronized (list) {
            for (int data : list) {
                out.println(data + " ");
                // unlike Concurrent Collections, Synchronized collections throw an exception
                // (like normal) if modified within iterator by single Thread
            }
        }
    }
}
