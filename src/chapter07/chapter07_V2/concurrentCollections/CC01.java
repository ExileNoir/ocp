package chapter07.chapter07_V2.concurrentCollections;

import java.util.Deque;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;

import static java.lang.System.out;

public class CC01 {
    public static void main(String[] args) {

        final Map<String, Integer> map = new ConcurrentHashMap<>();
        map.put("zebra", 52);
        map.put("elephant", 10);
        out.println(map.get("elephant"));

        for (String s : map.keySet()) {
            map.remove(s);      // works only on Concurrent, not on normal && Synchronized collections!!!
            out.println(s);
        }
        out.println(map);
        out.println();


        final Queue<Integer> queue = new ConcurrentLinkedQueue<>();
        queue.offer(31);
        queue.offer(1);
        out.println(queue.peek());
        out.println(queue.poll());

        final Deque<Integer> deque = new ConcurrentLinkedDeque<>();
        deque.offer(10);
        deque.push(4);
        out.println(deque.peek());
        out.println(deque.pop());

    }
}
