package chapter07.chapter07_V2.concurrentCollections;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import static java.lang.System.out;

/* LinkedBlockingQueue waits for the result of some of the operations.
 * * Can throw a Checked InterruptedException */
public class CC02 {
    public static void main(String[] args) {

        try {
            final BlockingQueue<Integer> blockingQueue = new LinkedBlockingQueue<>();
            blockingQueue.offer(39);
            boolean result = blockingQueue.offer(69, 4, TimeUnit.SECONDS);

            out.println(blockingQueue.poll());
            out.println(blockingQueue.poll(10, TimeUnit.SECONDS));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
