package chapter07.chapter07_V2.concurrentCollections;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

import static java.lang.System.out;

public class CC03 {
    public static void main(String[] args) {

        try {
            final BlockingDeque<Integer> blockingDeque = new LinkedBlockingDeque<>();
            blockingDeque.offer(1);
            blockingDeque.offerFirst(2, 2, TimeUnit.MINUTES);

            blockingDeque.offerLast(3, 100, TimeUnit.MICROSECONDS);
            blockingDeque.offer(4, 4, TimeUnit.SECONDS);

            out.println(blockingDeque.poll());
            out.println(blockingDeque.poll(950, TimeUnit.MILLISECONDS));

            out.println(blockingDeque.pollFirst(200, TimeUnit.NANOSECONDS));
            out.println(blockingDeque.pollLast(1, TimeUnit.SECONDS));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
