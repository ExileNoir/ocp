package chapter07.chapter07_V2.concurrentCollections;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static java.lang.System.out;

public class CC04 {
    public static void main(String[] args) {

        final List<Integer> list = new CopyOnWriteArrayList<>(Arrays.asList(4, 3, 62));
        for (final Integer item : list) {
            out.println(item + " ");
            list.add(9);
        }
        out.println(list);

        final List<Integer> list1 = new CopyOnWriteArrayList<>(Arrays.asList(1, 2, 3));
        final Iterator<Integer> it = list1.iterator();
        list1.remove(1);
        while (it.hasNext()) out.println(it.next());
        out.println(list1);

    }
}
