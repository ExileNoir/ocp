package chapter07.chapter07_V2.synchronizingDataAccess;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.System.out;

/* Not ThreadSafe on Concurrency */
public class SheepManager {

    private int sheepCount = 0;

    private /*synchronized*/ void incrementAndReport() {
        out.print(++sheepCount + " ");
    }


    public static void main(String[] args) {

        ExecutorService service = null;

        try {
            service = Executors.newFixedThreadPool(25);

            final SheepManager manager = new SheepManager();
            for (int i = 0; i < 10; i++) {
                service.submit(() -> manager.incrementAndReport());
            }
        } finally {
            if (service != null) service.shutdown();
        }
    }
}
