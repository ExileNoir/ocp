package chapter07.chapter07_V2.synchronizingDataAccess;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.System.out;

/* ThreadSafe via Atomic */
public class SheepManagerV1 {

    private AtomicInteger sheepCount = new AtomicInteger(0);

    private /*synchronized*/ void incrementAndReport() {
        synchronized (this) {        // all Threads wait at this sync block for the worker to inc and report result before entering
            out.print(sheepCount.incrementAndGet() + " ");      // atomic operation
        }
    }

    public static void main(String[] args) {

        ExecutorService service = null;

        try {
            service = Executors.newFixedThreadPool(20);

            final SheepManagerV1 manager = new SheepManagerV1();
            for (int i = 0; i < 10; i++) {
//                synchronized (manager) {     // does NOT solve the problem => synchronized the CREATION of the threads but NOT the EXECUTION of the threads
                service.submit(() -> manager.incrementAndReport());     // they will be created one at a time but still execute and perform their work at the same time
//                }
            }
        } finally {
            if (service != null) service.shutdown();
        }
    }
}
/* Result != ordered but will always give output from 1 to 10 */
/* Atomic classes ensures that the data is consistent between workers and that o values are lost due to concurrent modification */