package chapter07.chapter07_V2.cyclicBarrier;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.System.out;

public class LionPenManager {

    private void removeAnimals() {
        out.println("removing animals");
    }

    private void cleanPen() {
        out.println("cleaning the pen");
    }

    private void addAnimals() {
        out.println("adding the animals");
    }

    private void closePen() {
        out.println("closing penDoor");
    }

    public void performTask() {
        removeAnimals();
        cleanPen();
        addAnimals();
    }

    public void performTask(CyclicBarrier c1, CyclicBarrier c2, CyclicBarrier c3) {
        try {
            removeAnimals();
            c1.await();
            cleanPen();
            c2.await();
            addAnimals();
            c3.await();
            closePen();
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ExecutorService service = null;

        try {
            service = Executors.newFixedThreadPool(4);
            final LionPenManager manager = new LionPenManager();

            final CyclicBarrier c1 = new CyclicBarrier(4);        // if this n is higher then running Threads == DeadLock, code keeps hanging
            final CyclicBarrier c2 = new CyclicBarrier(4, () -> out.println("*** Pen Cleanded"));
            final CyclicBarrier c3 = new CyclicBarrier(4, () -> out.println("*** Work done"));
            for (int i = 0; i < 4; i++) {
//                service.submit(() -> manager.performTask());
                service.submit(() -> manager.performTask(c1, c2, c3));
            }
        } finally {
            if (service != null) service.shutdown();
        }
    }
}
