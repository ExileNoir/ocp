package chapter07.chapter07_V2;

import java.util.concurrent.*;

public class Exc01 {
    public static void main(String[] args) {

        ScheduledExecutorService service = null;

        try {
            service = Executors.newSingleThreadScheduledExecutor();
            service.scheduleWithFixedDelay(() -> {
                System.out.println("Open Zoo");
            }, 0, 1, TimeUnit.MINUTES);
            Future<?> result = service.submit(() -> System.out.println("wake staff"));
            System.out.println(result.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            if (service != null) service.shutdown();
        }
    }
}
