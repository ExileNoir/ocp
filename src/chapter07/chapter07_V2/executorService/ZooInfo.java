package chapter07.chapter07_V2.executorService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.System.out;

public class ZooInfo {
    public static void main(String[] args) {

        ExecutorService service = null;
        try {
            service = Executors.newSingleThreadExecutor();

            out.println("BEGIN");       // == main Thread
            service.execute(() -> out.println("Printing zoo inventory"));       // diff executors so go in order of execute
            service.execute(() -> {
                for (int i = 0; i < 3; i++) {
                    out.println("Printing record: " + i);
                }
            });
            service.execute(() -> out.println("Printing zoo inventory 1.1"));
            out.println("END");     // == main Thread
        } finally {
            if (service != null) service.shutdown();        // failing to shutdown == app will never terminate
        }
    }
}
