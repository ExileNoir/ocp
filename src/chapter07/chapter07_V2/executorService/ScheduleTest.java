package chapter07.chapter07_V2.executorService;

import java.util.concurrent.*;

public class ScheduleTest {
    public static void main(String[] args) {

        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.scheduleWithFixedDelay(() -> System.out.println("Open zoo"), 0, 1, TimeUnit.MINUTES);
        Future<?> result = service.submit(() -> System.out.println("wake staff"));
        try {
            System.out.println(result.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
