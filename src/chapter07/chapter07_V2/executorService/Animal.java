package chapter07.chapter07_V2.executorService;

import java.util.Arrays;

public class Animal {
    public static void main(String[] args) {

        System.out.println(Arrays.asList("duck", "duck")
                .parallelStream()
                .parallel()
                .reduce(0,
                        (c1, c2) -> c1 + c2.length(),
                        (s1, s2) -> s1 + s2));
    }
}
