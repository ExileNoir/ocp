package chapter07.chapter07_V2.executorService;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static java.lang.System.out;

/* Callable result */

public class AddData {
    public static void main(String[] args) {

        out.println(Runtime.getRuntime().availableProcessors());

        ExecutorService service = null;

        try {
            service = Executors.newSingleThreadExecutor();

//            final Future<Integer> result = service.submit(() -> 30 + 11);
//            Thread.sleep(1000);
            final Future<Integer> result = service.submit(() -> {
                Thread.sleep(1000);     // this is ONLY possible with CALLABLE not with Runnable (Runnable does not accept checked exc without embedded try/catch)
                return 30 + 100;
            });

            out.println(result.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            if (service != null) service.shutdown();
        }
    }
}
