package chapter07.chapter07_V2.executorService;

import java.util.concurrent.*;

import static java.lang.System.out;

public class ScheduledExecutor {
    public static void main(String[] args) {

        final ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();


        final Runnable task = () -> out.println("Hello Constance");
        final Callable<String> task2 = () -> "ExileNoir";

        final Future<?> resultOne = service.schedule(task, 20, TimeUnit.SECONDS);
        final Future<String> resultTwo = service.schedule(task2, 4, TimeUnit.MINUTES);

        try {
            out.println(resultOne.get());
            out.println(resultTwo.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            if (service != null) service.shutdown();
        }
    }
}
