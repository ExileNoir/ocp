package chapter07.chapter07_V2.executorService;

import java.util.concurrent.*;

import static java.lang.System.out;

/* Uses a Future instance to poll for results */
public class CheckResults {
    private static int counter = 0;

    public static void main(String[] args) {

        ExecutorService service = null;

        try {
            service = Executors.newSingleThreadExecutor();

            final Future<?> result = service.submit(() -> {
                for (int i = 0; i < 500; i++) {
                    counter++;
                }
            });
            result.get(10, TimeUnit.SECONDS);
            out.println(result.isDone());
            out.println("Reached!");
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            out.println("Not reached in time!");
        } finally {
            if (service != null) service.shutdown();
        }


    }
}
