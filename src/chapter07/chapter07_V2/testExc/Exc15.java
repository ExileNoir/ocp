package chapter07.chapter07_V2.testExc;

import java.util.concurrent.*;

public class Exc15 {
    public static void main(String[] args) {

        Callable c = new Callable() {
            @Override
            public Object call() throws Exception {
                return 10;
            }
        };

        ExecutorService service = Executors.newScheduledThreadPool(1);
        for (int i = 0; i < 10; i++) {
            Future f = service.submit(c);
            try {
                f.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        service.shutdown();
        System.out.println("Done");
    }
}
