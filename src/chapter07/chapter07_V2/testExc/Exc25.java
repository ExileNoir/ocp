package chapter07.chapter07_V2.testExc;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import static java.lang.System.*;

class Car{
    private String model;
    private int year;

    public Car(String model, int year) {
        this.model = model;
        this.year = year;
    }

    public int getYear(){
        return year;
    }

    public String toString(){
        return model;
    }
}

public class Exc25 {
    public static void main(String[] args) {

        final List<Car> cars = new ArrayList<>();
        cars.add(new Car("Mustang",1967));
        cars.add(new Car("Thunderbird",1967));
        cars.add(new Car("Escort",1975));

        final ConcurrentMap<Integer, List<Car>> map = cars
                .stream()
                .collect(Collectors.groupingByConcurrent(Car::getYear));
        out.println(map);
    }
}
