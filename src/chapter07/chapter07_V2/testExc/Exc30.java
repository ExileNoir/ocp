package chapter07.chapter07_V2.testExc;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import static java.lang.System.*;

class Boat {
    private void waitTillFinished(CyclicBarrier cb) {
        try {
            cb.await();
            out.println("1");
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }
    }

    public void row(ExecutorService service) {
        final CyclicBarrier cb = new CyclicBarrier(5);
        IntStream.iterate(1, i -> i + 1)
                .limit(12)      // still hangs, 2 task are in type of deadlock state waiting for 3 more
                .forEach(i -> service.submit(() -> waitTillFinished(cb)));
    }
}

public class Exc30 {
    public static void main(String[] args) {

        ExecutorService service = null;

        try {
            service = Executors.newCachedThreadPool();
            new Boat().row(service);
        } finally {
            service.shutdown();
            out.println(service.isShutdown());
        }
    }
}
