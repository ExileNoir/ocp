package chapter07.chapter07_V2.testExc;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static java.lang.System.out;

class Accountant {
    static void completePaperwork() {
        out.println("[Filling]");
    }

    static double getPi() {
        return 3.14159;
    }
}

public class Exc27 {
    public static void main(String[] args) {

        final ExecutorService service = Executors.newSingleThreadExecutor();
        final Future<?> f1 = service.submit(Accountant::completePaperwork);   //  -> Runnable
        final Future<Object> f2 = service.submit(Accountant::getPi);         // -> Callable

        try {
            out.println(f1.get() + " " + f2.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            service.shutdown();
        }
    }
}
