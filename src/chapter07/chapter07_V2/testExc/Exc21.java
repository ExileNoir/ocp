package chapter07.chapter07_V2.testExc;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static java.lang.System.out;

class Athlete {
    int stroke = 0;         // problem lies here

    public synchronized void swimming() {
        stroke++;         // ThreadSafe in manner that no write is lost
    }
}

public class Exc21 {
    public static void main(String[] args) throws InterruptedException {
        final ExecutorService service = Executors.newFixedThreadPool(10);
        final Athlete a = new Athlete();
        for (int i = 0; i < 1000; i++) {
            service.execute(() -> a.swimming());
        }
        service.shutdown();

        service.awaitTermination(100, TimeUnit.SECONDS);        // Solution: to make sure the read of stroke is done before all threads are done working.
        out.println(a.stroke);  // value is read while threads still executing
    }
}
