package chapter07.chapter07_V2.testExc;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static java.lang.System.*;

class TpsReport {
    void submitReports() {
        final ExecutorService service = Executors.newCachedThreadPool();
        Future bosses = service.submit(() -> out.print(""));
        service.shutdown();
        try {
            out.println(bosses.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}

public class Exc7 {
    public static void main(String[] args) {
        new TpsReport().submitReports();
    }
}
