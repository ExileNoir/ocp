package chapter07.chapter07_V2.testExc;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import static java.lang.System.out;

class TicketTaker {
    long ticketsSold;
    final AtomicInteger ticketsTaken;

    TicketTaker() {
        ticketsSold = 0;
        ticketsTaken = new AtomicInteger(0);
    }

    void performJob() {
        IntStream.iterate(1, p -> p + 1)
                .parallel()
                .limit(1000)
                .forEach(i -> ticketsTaken.getAndIncrement());

        IntStream.iterate(1, p -> p + 1)
                .limit(500)
                .parallel()
                .forEach(i -> ++ticketsSold);

        out.println("ticketsSold: " + ticketsSold + " || ticketsTaken: " + ticketsTaken);
    }
}

public class Exc02 {
    public static void main(String[] args) {
        new TicketTaker().performJob();
    }
}
