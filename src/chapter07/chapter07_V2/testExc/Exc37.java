package chapter07.chapter07_V2.testExc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import static java.lang.System.out;

public class Exc37 {
    public static void main(String[] args) {
        final List<Integer> db = Collections.synchronizedList(new ArrayList<>());
        IntStream.iterate(1, i -> i + 1)
                .limit(5)
                .parallel()
                .map(i -> {
                    db.add(i);
                    return i;
                })
                .forEachOrdered(out::print);

        out.println();
        db.forEach(out::print);

        // output is sometimes the same
    }
}
