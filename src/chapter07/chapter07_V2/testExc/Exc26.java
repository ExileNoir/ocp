package chapter07.chapter07_V2.testExc;

import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;

class Concat {
    public String concat1(List<String> values) {
        return values
                .parallelStream()
                .reduce("a",        // -> returns: String -> due to Identity
                        (x, y) -> x + y,
                        String::concat);
    }

    public String concat2(List<String> values) {
        return values
                .parallelStream()
                .reduce((w, z) -> z + w)     // till here returns: Optional<String>   -> due to no Identity
                .get();
    }
}

public class Exc26 {
    public static void main(String[] args) {
        final Concat c = new Concat();
        final List<String> list = Arrays.asList("Cat", "Hat");
        final String x = c.concat1(list);
        final String y = c.concat2(list);

        out.println(x + " " + y);
    }

}
