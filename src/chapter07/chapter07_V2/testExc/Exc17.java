package chapter07.chapter07_V2.testExc;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;

import static java.lang.System.out;

class Race {
    static ExecutorService service = Executors.newFixedThreadPool(8);

    static int sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 1;
    }

    static void hare() {
        try {
            Callable c = () -> sleep();
            final Collection<Callable<Integer>> r = Arrays.asList(c, c, c);
            List<Future<Integer>> results = service.invokeAll(r);
            out.println("Hare won the race!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static void tortoise() {
        try {
            Callable c = () -> sleep();
            final Collection<Callable<Integer>> r = Arrays.asList(c, c, c);
            Integer result = service.invokeAny(r);
            out.println("Tortoise won the race!");
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}

public class Exc17 {

    public static void main(String[] args) {
        Race.service.execute(() -> Race.hare());
        Race.service.execute(() -> Race.tortoise());


    }
}
