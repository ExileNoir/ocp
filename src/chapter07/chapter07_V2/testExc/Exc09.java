package chapter07.chapter07_V2.testExc;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class CartoonCat {
    private void await(CyclicBarrier cb) {
        try {
            cb.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }
    }

    void march(CyclicBarrier cb) {
//      ExecutorService service = Executors.newSingleThreadExecutor();
        ExecutorService service = Executors.newCachedThreadPool();
        for (int i = 0; i < 12; i++) {      // does 3 cycles of 4Threads (12/4==3) (Ready 3x)
            service.execute(() -> await(cb));
        }
        service.shutdown();
    }
}

public class Exc09 {
    public static void main(String[] args) {
        new CartoonCat().march(
                new CyclicBarrier(4,
                        () -> System.out.println("Ready")));
    }
}
