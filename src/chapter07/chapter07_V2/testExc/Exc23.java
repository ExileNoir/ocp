package chapter07.chapter07_V2.testExc;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

import static java.lang.System.out;

class CountSheep extends RecursiveAction {
    static int[] sheep = new int[]{1, 2, 3, 4};
    final int start;
    final int end;
    static int count = 0;       // if this is not static it will not be shared amongst instances of CountSheep subTasks

    public CountSheep(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public void compute() {
        if (end - start < 2) {
            count += sheep[start];
            return;
        } else {
            int middle = start + (end - start) / 2;
            invokeAll(new CountSheep(start, middle),
                    new CountSheep(middle, end));
        }
    }
}

public class Exc23 {
    public static void main(String[] args) {

        final ForkJoinPool pool = new ForkJoinPool();
        final CountSheep action = new CountSheep(0, CountSheep.sheep.length);
        pool.invoke(action);

        pool.shutdown();
        out.println(CountSheep.count);
    }
}
