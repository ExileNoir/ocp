package chapter07.chapter07_V2.parallelStreams;

import java.util.Arrays;

import static java.lang.System.out;

/* Parallel Stream and ForEachOrdered */
public class PS02 {
    public static void main(String[] args) {


        Arrays.asList(3, 4, 5, 6, 7, 8, 9, 2, 1)
                .parallelStream()
                .forEachOrdered(s -> out.print(s + " "));       // returns in same way as original list it came from

        out.println();

        Arrays.asList(2, 3, 4, 5, 6, 7, 8, 9, 1)
                .parallelStream()
                .forEach(s -> out.print(s + " "));

    }
}
