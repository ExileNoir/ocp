package chapter07.chapter07_V2.parallelStreams;

import java.util.Arrays;
import java.util.stream.Stream;

public class PS01 {
    public static void main(String[] args) {

        Stream<Integer> stream = Arrays.asList(1,2,3,4,5,6,7,8,9).stream();
        Stream<Integer> parallel = stream.parallel();

        Stream<Integer> parallel2 = Arrays.asList(1,2,3,4,5,6,7,8,9).parallelStream();
    }
}
