package chapter07.chapter07_V2.parallelStreams;

import java.util.Arrays;

public class PS06 {
    public static void main(String[] args) {

        System.out.println(Arrays.asList('w', 'o', 'l', 'f')
                .stream()
                .parallel()
                .reduce("",
                        (c, s1) -> c + s1,
                        (s2, s3) -> s2 + s3));
    }
}
