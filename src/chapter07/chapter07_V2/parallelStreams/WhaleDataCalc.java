package chapter07.chapter07_V2.parallelStreams;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.currentTimeMillis;
import static java.lang.System.out;

public class WhaleDataCalc {

    int processRecord(final int input) {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return input + 1;
    }

    void processAllData(final List<Integer> data) {
        data.parallelStream()           // speed increased due to parallelStream over SerialStream
                .map(this::processRecord).count();
    }


    public static void main(String[] args) {
        final WhaleDataCalc calc = new WhaleDataCalc();

        // define data
        final List<Integer> data = new ArrayList<>();
        for (int i = 0; i < 4000; i++) {
            data.add(i);
        }

        // process the data
        final long start = currentTimeMillis();
        calc.processAllData(data);
        final double time = (currentTimeMillis() - start) / 1_000.0;

        // report results
        out.println("\nTasks completed in: " + time + " seconds");
    }
}
