package chapter07.chapter07_V2.parallelStreams;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class PS05 {
    public static void main(String[] args) {

        List<String> list = Arrays.asList("w","o","l","f");
        List<String> stringList = list.stream()
                .parallel()
                .collect(Collectors.toCollection(CopyOnWriteArrayList::new));
        System.out.println(stringList);

        Set<String> stringSet = list.stream()
                .parallel()
                .collect(Collectors.toCollection(ConcurrentSkipListSet::new));
        System.out.println(stringSet);
    }
}
