package chapter07.chapter07_V2.parallelStreams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.lang.System.out;

/* Avoid STATEFUL Ops in lambda expressions */
public class PS04 {
    public static void main(String[] args) {

        final List<Integer> data = Collections.synchronizedList(new ArrayList<>());
//        List<Integer> data = new ArrayList<>();       // can produce a null value == race condition
        Arrays.asList(1, 2, 3, 4, 5, 6)
                .parallelStream()
                .map(integer -> {
                    data.add(integer);      // avoid stateful lambda exp
                    return integer;
                })
                .forEachOrdered(integer -> out.print(integer + " "));

        out.println();
        for (final Integer e : data) {
            out.print(e + " ");
        }
    }
}
/* Avoid stateful ops when using parallel streams, to avoid side effects
 * Even in Serial Streams!! BUT in this case outcome in Serial != parallel
 * */
