package chapter07.chapter07_V2.parallelStreams;

import java.util.Arrays;

import static java.lang.System.out;

public class PS03 {
    public static void main(String[] args) {

        Arrays.asList("jackas", "kangaroo", "lemur", "DickHead", "Constance")
                .parallelStream()
                .map(s -> {
                    out.println(s);              // can be different from the order toUppercase  // avoid side effects
                    return s.toUpperCase();     // increase speed as toUppercase can be done independently, although order is never guaranteed
                })
                .forEach(out::println);
    }
}
