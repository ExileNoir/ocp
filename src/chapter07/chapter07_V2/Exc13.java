package chapter07.chapter07_V2;

import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Exc13 {
    public static void main(String[] args) {

        Stream<String> cats = Stream.of("Leopard", "lync", "ocelot", "poema").parallel();
        Stream<String> bears = Stream.of("panda", "Grizzly", "polar").parallel();

        ConcurrentMap<Boolean, List<String>> data = Stream.of(cats, bears)
                .flatMap(s -> s)
                .collect(Collectors.groupingByConcurrent(s -> !s.startsWith("p")));
        System.out.println(data.get(false).size() + " " + data.get(true).size());
    }
}
