package chapter07.chapter07_V2;

public class PollingWithSleepT02 {

    private static int counter = 0;

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            for (int i = 0; i < 500; i++) {
                counter++;
            }
        }).start();

        while (counter < 100) {
            System.out.println("Not reached 100 yet");
            Thread.sleep(1000);      // 1 sec sleep
        }
        System.out.println("Reached: " + counter  +" "+Thread.currentThread().getName());
    }
}
