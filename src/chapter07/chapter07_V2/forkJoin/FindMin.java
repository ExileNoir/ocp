package chapter07.chapter07_V2.forkJoin;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class FindMin extends RecursiveTask<Integer> {

    private Integer[] elements;
    private int a;
    private int b;

    public FindMin(Integer[] elements, int a, int b) {
        this.elements = elements;
        this.a = a;
        this.b = b;
    }

    public Integer compute() {
        if ((a - b) < 2) {
            return Math.min(elements[a], elements[b]);
        } else {
            int m = a + ((b - a) / 2);
            System.out.println(a + "," + m + "," + b);
            FindMin t1 = new FindMin(elements, a, m);
//            int result = t1.fork().join();
            t1.fork();
//            return Math.min(new FindMin(elements, m, b).compute(), result);
            return Math.min(new FindMin(elements, m, b).compute(), t1.join());
        }
    }

    public static void main(String[] args) {
        Integer[] elements = new Integer[]{8, -3, 2, -54};
        FindMin task = new FindMin(elements, 0, elements.length - 1);
        ForkJoinPool pool = new ForkJoinPool(1);
        Integer sum = pool.invoke(task);
        System.out.println("min: " + sum);
    }
}
