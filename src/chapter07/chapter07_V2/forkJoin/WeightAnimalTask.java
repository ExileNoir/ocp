package chapter07.chapter07_V2.forkJoin;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

import static java.lang.System.out;

/* RecursiveTask */
public class WeightAnimalTask extends RecursiveTask<Double> {
    private Double[] weights;
    private int start;
    private int end;

    public WeightAnimalTask(Double[] weights, int start, int end) {
        this.weights = weights;
        this.start = start;
        this.end = end;
    }

    @Override
    protected Double compute() {
        if (end - start <= 3) {
            double sum = 0;
            for (int i = start; i < end; i++) {
                weights[i] = (double) new Random().nextInt(100);
                out.println("Animal Weighed: " + i);
                sum += weights[i];
            }
            return sum;
        } else {
            int middle = start + ((end - start) / 2);
            out.println("[start=" + start + ",middle=" + middle + ",end=" + end + "]");
            final RecursiveTask<Double> otherTask = new WeightAnimalTask(weights, start, middle);
            otherTask.fork();
            return new WeightAnimalTask(weights, middle, end).compute() + otherTask.join();
        }
    }

    public static void main(String[] args) {
        final Double[] weights = new Double[10];

        final ForkJoinTask<Double> task = new WeightAnimalTask(weights, 0, weights.length);
        final ForkJoinPool pool = new ForkJoinPool();
        final Double sum = pool.invoke(task);
        out.println("sum: " + sum);
    }
}
