package chapter07.lambdaExpressions;

import static java.lang.System.out;

/* Argument-defined Anonymous Inner Classes */
public class MyWonderfulClass {
    void go() {
        final Bar bar = new Bar();
        bar.doStuff(new Foo() {
            @Override
            public void foof() {
                out.println("FOOFY implement");
            }
        }); // end inner class def, arg, and b.doStuff stmt.
    }

    void go2() {
        final Bar bar = new Bar();
        bar.doStuff(() -> out.println("FOOFY implement via lambda"));

        // more explicitly obvious version below
        final Foo f = () -> out.println("FOOFY 2 implement via lambda");
        f.foof();
    }



    public static void main(String[] args) {
        final MyWonderfulClass c = new MyWonderfulClass();
        c.go();
        c.go2();
    }
}

interface Foo {
    public abstract void foof();
}

class Bar {
    void doStuff(final Foo f) {
        f.foof();
    }
}
