package chapter07.innerClasses;

class A {
    void m() {
        System.out.println("outer");
    }
}

public class TestInners {
    public static void main(String[] args) {
        new TestInners().go();

        chapter07.innerClasses.A a = new chapter07.innerClasses.A();
        a.m();

//        A aa = new A();       // cannot work if middle A is not uncommented!!
//        aa.m();

        new TestInners().new A().m();
    }

    void go() {
        new A().m();
        class A {
            void m() {
                System.out.println("inner");
            }
        }
    }

    class A {
        void m() {
            System.out.println("middle");
        }
    }
}
