package chapter07.innerClasses;

/* Anonymous Inner Classes: flavor TWO */
public interface Cookable {
    public abstract void cook();
}

class Food01 {
    final Cookable c = new Cookable() {
        @Override
        public void cook() {
            System.out.println("anonymous cookable implementer");
        }
    };
}
