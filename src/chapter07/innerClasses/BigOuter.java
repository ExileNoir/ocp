package chapter07.innerClasses;

import static java.lang.System.out;

public class BigOuter {

    static protected class Nested {
        void go() {
            out.println("hi1: Hellow i am from static nested inner class");
        }
    }
}

class Broom {
    static void broomShit() {
        out.println("broom shit");
    }

    static class B2 {
        void goB2() {
            out.println("hi2: ");
            broomShit();        // only access to static members of the outer class
        }
    }

    public static void main(String[] args) {
        final BigOuter.Nested n = new BigOuter.Nested();    // both class names if outside outer class
        n.go();

        final B2 b2 = new B2();     // access the enclosed class (we are still in outer class of B2
        b2.goB2();

    }
}