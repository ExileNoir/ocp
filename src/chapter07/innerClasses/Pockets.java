package chapter07.innerClasses;

import java.util.Arrays;
import java.util.Comparator;

public class Pockets {
    public static void main(String[] args) {
        String[] sa = {"nickel", "button", "key", "lint"};
        Sorter s = new Pockets().new Sorter();
        for (String s1 : sa) {
            System.out.print(s1 + " ");
        }
        System.out.println();
        Arrays.sort(sa, s);
        for (String s1 : sa) {
            System.out.print(s1 + " ");
        }
    }

    class Sorter implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            return o2.compareTo(o1);
        }
    }
}
