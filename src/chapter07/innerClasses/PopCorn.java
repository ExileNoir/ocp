package chapter07.innerClasses;

import static java.lang.System.out;

/* Anonymous Inner Classes: flavor ONE */
public class PopCorn {
    public void pop() {
        out.println("PopCorn");
    }

    public void hot(){
        out.println("Hot PopCorn");
    }
}

class Food {
    final PopCorn p = new PopCorn() {       // CURLY BRACES, not a semicolon!!
        @Override
        public void pop() {
            out.println("anonymous PopCorn");
        }
    };      // Curly braces to close off anonymous class definition && SEMICOLON that ends the statement of <line13>


    public static void main(String[] args) {
        new Food().p.hot();
        new Food().p.pop();
    }
}
/*
 * Point of Anonymous Inner class:
 * Making a subclass && override one or more methods of this superClass
 * */