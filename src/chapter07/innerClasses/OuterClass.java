package chapter07.innerClasses;

import static java.lang.System.out;

/* Regular Inner Classes */
public class OuterClass {

    private int x = 7;

    public void makeInner() {
        final InnerClass in = new InnerClass();   // make an Inner class instance
        in.seeOuter();
    }

    /* Inner class definition */
    private class InnerClass {   // all access modifiers are accepted: public, private, protected && static, strictfp, abstract, final
        public void seeOuter() {
            out.println("Outer x is: " + x);
            out.println("Inner class ref is: " + this);
            out.println("Outer class ref is: " + OuterClass.this);
            out.println("Outer class ref is: " + OuterClass.this.x);
        }
    }   // close Inner class

    public static void main(String[] args) {
        final OuterClass outer = new OuterClass();
        final OuterClass.InnerClass inner = outer.new InnerClass();
        inner.seeOuter();

        final OuterClass.InnerClass innerClass = new OuterClass().new InnerClass();
        innerClass.seeOuter();

    }

}   // close Outer class
