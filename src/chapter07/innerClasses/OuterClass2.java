package chapter07.innerClasses;

import static java.lang.System.out;

/* Method-Local Inner Classes */
public class OuterClass2 {

    private String x = "Outer2";

    void doStuff() {
        String z = "Local Variable";        // Must effectively final || final
        final class Inner {     // final && Abstract are only modifiers in a method-local Inner class
            String inner = "inner shit";
            public void seeOuter() {
                out.println("Outer x is: " + x);
                out.println("Local var z is: " + z);
                out.print(inner);
//                z = "changing the local variable";       //  WILL NOT COMPILE
            }   // close Inner class method
        }      // close Inner class


        final Inner inner = new Inner();    // this line MUST come after local Inner class
        inner.seeOuter();

    }         // close Outer class method

    public static void main(String[] args) {
        new OuterClass2().doStuff();
    }
}            // close Outer class
