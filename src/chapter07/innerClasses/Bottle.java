package chapter07.innerClasses;

public class Bottle {
    public static class Ship {
        private enum Sail {
            TALL {
                protected int getHeight() {
                    return 100;
                }
            },
            SHORT {
                protected int getHeight() {
                    return 2;
                }
            };

            protected abstract int getHeight();
        }

        public Sail getSail() {
            return Sail.TALL;
        }
    }

    public static void main(String[] args) {
        Bottle bottle = new Bottle();
        Bottle.Ship ship = new Bottle.Ship();
        System.out.println(ship.getSail());
    }
}
