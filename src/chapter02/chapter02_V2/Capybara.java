package chapter02.chapter02_V2;

class Rodent {
}

public class Capybara extends Rodent {


    public static void main(String[] args) {
        Rodent rodent = new Rodent();
        if (rodent instanceof Capybara) {
            Capybara capybara = (Capybara) rodent;  // classCast
        }


        Capybara capybara1 = new Capybara();
        Rodent rodent1 = capybara1;
        Capybara capybara2 = (Capybara) rodent1;
    }
}
