package chapter02.chapter02_V2;
 interface Edible{
    void eat();
}
public class ApplePicking {
    public static void main(String[] args) {
        Edible apple = new Edible() {
            @Override
            public void eat() {
                System.out.println("Jumm");
            }
        };
        apple.eat();

//        Edible apple = () -> System.out.println("Jumm");

    }
}
