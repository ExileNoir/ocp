package chapter02.chapter02_V2;

import static java.lang.System.*;

public abstract class Food {
    private int quantity;

    public Food(final int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public abstract void consumed();
}

class Hay extends Food {
    public Hay(final int quantity) {
        super(quantity);
    }

    @Override
    public void consumed() {
        out.println("Hay eaten: " + getQuantity());
    }
}

class Pellets extends Food {
    public Pellets(final int quantity) {
        super(quantity);
    }

    @Override
    public void consumed() {
        out.println("Pellets eaten: " + getQuantity());
    }
}

class Fish extends Food {
    public Fish(final int quantity) {
        super(quantity);
    }

    @Override
    public void consumed() {
        out.println("Fish eaten: " + getQuantity());
    }
}
