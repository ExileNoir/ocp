package chapter02.chapter02_V2;

public class Ready {
    private static int first = 2;
    private final short DEFAULT_VALUE = 10;

    private static class GetSet {
        int first = Ready.first;
        static int second = new Ready().DEFAULT_VALUE;
//        int second = Ready.this.DEFAULT_VALUE;        // if GetSet != Static class

    }

    private GetSet go = new GetSet();

    public static void main(String[] args) {
        Ready r = new Ready();
        System.out.println(r.go.first);
        System.out.println(", " + r.go.second);
    }
}
