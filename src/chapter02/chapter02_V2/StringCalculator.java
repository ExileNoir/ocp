package chapter02.chapter02_V2;
@FunctionalInterface
public interface StringCalculator {
    public abstract int calcString(final String s,final int n);

}
