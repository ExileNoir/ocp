package chapter02.chapter02_V2;

import static java.lang.System.*;

public class LlamaTrainer {
    public boolean feedLlamas(final int numberOfLlamas) {
        final int amountNeeded = 5 * numberOfLlamas;
        final HayStorage hayStorage = HayStorage.getInstance();
        if (hayStorage.getHayQuantity() < amountNeeded) {
            hayStorage.addHay(amountNeeded + 10);
        }
        final boolean fed = hayStorage.removeHay(amountNeeded);
        if (fed) {
            out.println("llamas have been fed");
        }
        return fed;
    }

    public static void main(String[] args) {
        LlamaTrainer ll = new LlamaTrainer();
        final boolean fed = ll.feedLlamas(10);
        out.println("fed? "+fed) ;
    }

}
