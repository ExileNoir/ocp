package chapter02.chapter02_V2;

public class FindMatchingAnimals {

    private static void print(final Animal animal, final CheckTrait trait) {
        if (trait.kiss(animal)) System.out.println(animal);
    }

    private static int calc(final String s, final int n, final StringCalculator calc) {
        return calc.calcString(s, n);
    }

    public static void main(String[] args) {
        print(new Animal("fish", false, true), a -> a.canSwim());
        print(new Animal("kangaroo", true, false), Animal::canHop);

        System.out.println(calc("DS", 0, (s, n) -> s.length() + n));
        System.out.println(calc("DS", 1, (String sLength, int n) -> {
            return sLength.length() + n;
        }));
    }
}
