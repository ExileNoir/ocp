package chapter02.chapter02_V2;

import static java.lang.System.*;

class Primate {
    boolean hasHair() {
        return true;
    }
}

@FunctionalInterface
interface HasTail {
    public boolean isTailStriped();
}

public class Lemur extends Primate implements HasTail {
    int age = 10;

    public boolean isTailStriped() {
        return false;
    }


    public static void main(String[] args) {

        Lemur lemur = new Lemur();
        out.println(lemur.age);

        HasTail hasTail = lemur;
        out.println(hasTail.isTailStriped());
        out.println(((Lemur) hasTail).age);
        out.println(((Primate)hasTail).hasHair());

        Primate primate = lemur;
        out.println(primate.hasHair());
        out.println(((Lemur)primate).age);
        out.println(((HasTail)(Lemur)primate).isTailStriped());

        Lemur l = (Lemur)primate;
    }
}
