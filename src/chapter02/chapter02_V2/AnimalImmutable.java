package chapter02.chapter02_V2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;

public final class AnimalImmutable {
    private final String species;
    private final int age;
    private final List<String> favoriteFoods;

    public AnimalImmutable(final String species, final int age, final List<String> favoriteFoods) {
        this.species = species;
        this.age = age;
        if (favoriteFoods == null) {
            throw new RuntimeException("FavoriteFoods is required");
        }
        this.favoriteFoods = new ArrayList<>(favoriteFoods);
    }

    public String getSpecies() {
        return species;
    }

    public int getAge() {
        return age;
    }

    public int getFavoriteFoodsCount() {
        return favoriteFoods.size();
    }

    public String getFavoriteFood(final int index) {
        return favoriteFoods.get(index);
    }

    public List<String> getFavoriteFoods() {
        return new ArrayList<>(favoriteFoods);
    }

    public static void main(String[] args) {
        final AnimalImmutable lion = new AnimalImmutable("lion", 5, Arrays.asList("meat", "more Meat", "fish?"));
        out.println(lion.favoriteFoods.add("turkey"));
        for (final String favoriteFood : lion.favoriteFoods) {
            out.println(favoriteFood);
        }

    }
}
