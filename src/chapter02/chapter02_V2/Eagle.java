package chapter02.chapter02_V2;

public class Eagle implements Fly {

    @Override
    public int getWingSpan() throws Exception {
        return 15;
    }

    @Override
    public void land() {
        Fly.super.land();
        System.out.println("Eagle is diving real fast");
    }

    public static double calculateSpeed(final float distance, final double time) {
        return distance / time + 1;
    }

    public static void main(String[] args) {
        final Fly eagle = new Eagle();
        System.out.println(MAX_SPEED);
        System.out.println(Fly.calculateSpeed(15.0f, 1.0));
        System.out.println(calculateSpeed(15.0f, 1.0));
    }
}
