package chapter02.chapter02_V2;

public class Animal {
    private String species;
    private boolean canHop;
    private boolean canSwim;

    public Animal(final String species, final boolean canHop, final boolean canSwim) {
        this.species = species;
        this.canHop = canHop;
        this.canSwim = canSwim;
    }

    public boolean canHop() {
        return canHop;
    }

    public boolean canSwim() {
        return canSwim;
    }

    public String toString() {
        return species;
    }
}
