package chapter02.chapter02_V2;

public interface Fly {
    public static final int MAX_SPEED = 100;

    public abstract int getWingSpan() throws Exception;

    public default void land() {
        System.out.println("Animal is landing");
    }

    public static double calculateSpeed(final float distance, final double time) {
        return distance / time;
    }
}
