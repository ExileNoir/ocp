package chapter02.chapter02_V2;

/* Singleton Lazy */
public class HayStorage {

    private int quantity = 0;
    private static volatile HayStorage instance;

    private HayStorage() {
    }

    public static /*synchronized*/ HayStorage getInstance() {
        if (instance == null) {
            synchronized (HayStorage.class) {
                if (instance == null) {
                    instance = new HayStorage();
                }
            }
        }
        return instance;
    }

    public synchronized void addHay(final int amount) {
        quantity += amount;
    }

    public synchronized boolean removeHay(final int amount) {
        if (quantity < amount) {
            return false;
        }
        quantity -= amount;
        return true;
    }

    public synchronized int getHayQuantity() {
        return quantity;
    }
}
