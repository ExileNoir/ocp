package chapter02.chapter02_V2;

import java.util.Arrays;
import java.util.List;

/* Builder Pattern */
public class AnimalBuilder {
    private String species;
    private int age;
    private List<String> favoriteFoods;

    public AnimalBuilder setAge(final int age) {
        this.age = age;
        return this;
    }

    public AnimalBuilder setSpecies(final String species) {
        this.species = species;
        return this;
    }

    public AnimalBuilder setFavoriteFoods(final List<String> favoriteFoods) {
        this.favoriteFoods = favoriteFoods;
        return this;
    }

    public AnimalImmutable build() {
        return new AnimalImmutable(species, age, favoriteFoods);
    }

    public static void main(String[] args) {
        final AnimalBuilder duckBuilder = new AnimalBuilder();
        duckBuilder
                .setAge(14)
                .setSpecies("duck")
                .setFavoriteFoods(Arrays.asList("grass", "fish"));
        final AnimalImmutable duck = duckBuilder.build();

        final AnimalImmutable flamingo = new AnimalBuilder()
                .setAge(10)
                .setFavoriteFoods(Arrays.asList("Algea", "insects"))
                .setSpecies("flamingo")
                .build();
    }
}
