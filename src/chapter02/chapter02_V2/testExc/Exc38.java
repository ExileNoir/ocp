package chapter02.chapter02_V2.testExc;

import java.util.function.BiPredicate;
import java.util.function.IntUnaryOperator;
import java.util.function.LongFunction;
import java.util.function.UnaryOperator;

interface Toy {
}

public class Exc38 {
    public static void main(String[] args) {
        abstract class Robot {
        }
        class Transformer extends Robot implements Toy {
            String name = "GiantRobot";

            public String play() {
                return "DinoRobot";
            }
        }
        Transformer prime = new Transformer() {
            public String play() {
                return name;
            }
        };
        System.out.println(prime.play() + " " + prime.name);
    }
}
