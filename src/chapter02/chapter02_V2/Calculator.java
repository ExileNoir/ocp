package chapter02.chapter02_V2;
@FunctionalInterface
interface AddNumbers{
    int add(int x, int y);
    static int subtract(int x , int y){return x-y;}
    default int multiply(int x, int y){return x*y;}
}
public class Calculator {
    private void calculate(AddNumbers add,int x, int y){
        System.out.println(add.add(x,y));
    }

    public static void main(String[] args) {
        Calculator ti = new Calculator();
        ti.calculate((k,p)->k+p+1,5,7);
    }
}
