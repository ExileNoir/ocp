package chapter02.chapter02_V2;
enum DaysOff{
    ThanksGiving,PresidentsDay,ValentinesDay
}
public class Vacation {
    public static void main(String[] args) {
        final DaysOff input = DaysOff.PresidentsDay;
//        final DaysOff input = PresidentsDay;
        switch (input){
            default:
//            case DaysOff.ValentinesDay:
            case ValentinesDay:
                System.out.println("1");
            case PresidentsDay:
                System.out.println("2");
        }
    }
}
