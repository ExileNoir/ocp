package chapter02.chapter02_V2;

@FunctionalInterface
public interface CheckTrait {
    public boolean kiss(final Animal a);
}
