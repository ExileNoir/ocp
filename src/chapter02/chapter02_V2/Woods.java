package chapter02.chapter02_V2;

public class Woods {
    static class Tree {
    }

    public static void main(String[] args) {
        int water = 10 + 5;
        final class Oak extends Tree {
            public int getWater() {
                return water;
            }
        }
        System.out.println(new Oak().getWater());
    }
}
