package chapter02.chapter02_V2;

/* Singleton using static block instantiation EAGER */
public class StaffRegister {
    private static final StaffRegister INSTANCE;

    private StaffRegister() {
    }

    static {
        INSTANCE = new StaffRegister();
    }

    public static StaffRegister getInstance() {
        return INSTANCE;
    }
}
