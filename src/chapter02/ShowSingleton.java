package chapter02;

import java.util.HashSet;
import java.util.Set;

public class ShowSingleton {

    /* Eager initialisation */
//    private static final ShowSingleton SHOW_INSTANCE = new ShowSingleton();

    /* Lazy initialisation */
    private static ShowSingleton SHOW_INSTANCE;
    private Set<String> availableSeats;

    public static ShowSingleton getInstance() {
        /* Eager initialisation */
//        return SHOW_INSTANCE;

        /* Lazy initialisation */
        if (SHOW_INSTANCE == null) {
            SHOW_INSTANCE = new ShowSingleton();
        }
        return SHOW_INSTANCE;
    }

    private ShowSingleton() {
        availableSeats = new HashSet<>();
        availableSeats.add("1A");
        availableSeats.add("1B");
        availableSeats.add("1C");
    }

    public boolean bookSeat(String seat) {
        return availableSeats.remove(seat);
    }

    private static void ticketAgentBooks(String seat) {
        final ShowSingleton show = getInstance();
        System.out.println(show.bookSeat(seat));
    }

    public static void main(String[] args) {
        ticketAgentBooks("1A");
        ticketAgentBooks("1A");
        ticketAgentBooks("1C");
    }

}
