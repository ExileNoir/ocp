package chapter02;

import java.util.HashSet;
import java.util.Set;

public enum  ShowEnumSingleton {

    INSTANCE;

    private Set<String> availableSeats;

    private ShowEnumSingleton(){
        availableSeats = new HashSet<>();
        availableSeats.add("1A");
        availableSeats.add("1B");
    }

    public boolean bookSeat(String seat){
        return availableSeats.remove(seat);
    }

    private static void ticketAgentBooks(String seat){
        ShowEnumSingleton show = ShowEnumSingleton.INSTANCE;
        assert show.bookSeat(seat);
        System.out.println(show.bookSeat(seat));

//        System.out.println(ShowEnumSingleton.INSTANCE.bookSeat(seat));
    }

    public static void main(String[] args) {
        ticketAgentBooks("1A");
        ticketAgentBooks("1A");
    }
}
