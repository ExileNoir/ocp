package chapter02;

class Alpha{
    static String s = " ";
    protected Alpha() {s += "Alpha ";  }
}
class SubAlpha extends Alpha{
    private SubAlpha() { s+= "Sub ";  }
}
public class SubSubAlpha extends Alpha {
    private SubSubAlpha(){s += "SubSub ";}

    public static void main(String[] args) {
        new SubSubAlpha();
        System.out.println(s);
    }
}
