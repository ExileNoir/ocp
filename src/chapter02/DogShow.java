package chapter02;

class Dog{
    public void bark(){
        System.out.println(" WOOF ");
    }
}

class Hound extends Dog{
    public void sniff(){
        System.out.println(" SNIFF ");
    }

    public void bark() {
        System.out.println(" HOWL ");
    }
}


public class DogShow {
    public static void main(String[] args) {
        new DogShow().go();
    }

    void go() {
    new Hound().bark();
        ((Dog)new Hound()).bark();
//        ((Dog)new Hound()).sniff();
//    new Hound().sniff();
//        ((Hound)new Dog()).sniff();
//        ((Hound)new Dog()).bark();
    }

}
