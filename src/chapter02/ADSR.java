package chapter02;

final public class ADSR {

    private final StringBuilder name;
    private final int attack;
    private final int decay;

    public ADSR(StringBuilder n, int attack, int decay) {
        name = new StringBuilder(n);
        this.attack = attack;
        this.decay = decay;
    }

    // no setters for final class

    public StringBuilder getName() {
         StringBuilder nameCopy = new StringBuilder(name);
        if (nameCopy != name)
            System.out.println("different object");
        return nameCopy;
    }

    public ADSR getADSR() {
        return this;
    }

    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder("a1 ");
        ADSR a1 = new ADSR(sb, 5, 7);
        ADSR a2 = a1.getADSR();
        System.out.println(a1.getName());
        sb.append("alter the name");
        System.out.println(a2.getName());
    }


}
