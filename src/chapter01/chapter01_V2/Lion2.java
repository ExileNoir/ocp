package chapter01.chapter01_V2;

public class Lion2 {
    public int idNumber;
    private int age;
    private String name;

    Lion2(int idNumber, int age, String name) {
        this.idNumber = idNumber;
        this.age = age;
        this.name = name;
    }


    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof Lion2)) return false;
        final Lion2 otherLion = (Lion2) o;
        return this.idNumber == otherLion.idNumber && this.name.equals(otherLion.name);
    }

    @Override
    public int hashCode() {
        return idNumber;
    }

    public static void main(String[] args) {
        final Lion2 l = new Lion2(22, 25, "salome");
        final Lion2 l1 = new Lion2(23, 25, "salome");
        final Lion2 l2 = null;
        System.out.println(l.equals(l1));
        System.out.println(l.equals(l2));
    }
}
