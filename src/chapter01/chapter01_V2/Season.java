package chapter01.chapter01_V2;

import static java.lang.System.out;

public enum Season {
    WINTER("low") {
        public void printHours() {
            out.println("9am-3pm");
        }
    },
    SPRING("medium") {
        public void printHours() {
            out.println("9am-5pm");
        }
    },
    SUMMER("high") {
        public void printHours() {
            out.println("9am-7pm");
        }
    },
    FALL("medium") {
        @Override
        public void printHours() {
            out.println("9am-5pm");
        }
    };


    private final String expectedVisitors;

    private Season(final String s) {
        this.expectedVisitors = s;
    }

    public String getExpectedVisitors() {
        return expectedVisitors;
    }

    public abstract void printHours();

    public static void main(String[] args) {
        for (Season season : Season.values())
            out.println(season.name() + " " + season.ordinal() + " " + season.getExpectedVisitors());


        Season.SUMMER.printHours();

        final Season summer = Season.SUMMER;
        switch (summer.ordinal()) {
            case 0:
                out.println("Get out the sled");
                break;
            case 2:
                out.println("In the SummerTime lalalalal");
                break;
            default:
                out.println("Where is my Sunshine");
                break;
        }
    }
}
