package chapter01.chapter01_V2;

public class Card {
    private String rank;
    private String suit;

    public Card(final String r, final String s) {
        if (r == null || s == null) throw new IllegalArgumentException();
        this.rank = r;
        this.suit = s;
    }

    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof Card)) return false;
        final Card c = (Card) o;
        return this.rank.equals(c.rank) && this.suit.equals(c.suit);
    }

    @Override
    public int hashCode() {
        return this.rank.hashCode();
    }
}
