package chapter01.chapter01_V2;

public abstract class Cat {
    static final String name = "The Cat";

    static void clean() {
    }
}

class Lion extends Cat {
     void clean(String s) {
    }
}
