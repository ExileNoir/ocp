package chapter01.chapter01_V2;

class House extends Building {
}

public class Building {

    public static void main(String[] args) {
        Building b = new Building();
        House h = new House();
        Building bh = new House();

        Building p = (House) b;  // classCastE      // Building IS NOT a House
//        House q = (Building) h;                  //      (Building) cannotBe stored into House
        Building r = (Building) bh;
        House s = (House) bh;
    }

}
