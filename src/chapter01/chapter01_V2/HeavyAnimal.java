package chapter01.chapter01_V2;

class Hippo extends HeavyAnimal {
}

class Elephant extends HeavyAnimal {
}

interface Mother{}


public class HeavyAnimal {
    public static void main(String[] args) {
        HeavyAnimal h = new Hippo();
        System.out.println(h instanceof Elephant);

        Hippo nullH = null;
        System.out.println(nullH instanceof Object);

        Hippo interfH = new Hippo();
        System.out.println(interfH instanceof Mother);
    }
}


