package chapter01.chapter01_V2;

public class Outer {
    private String greeting = "Hi";

    protected class Inner {
        public void go() {
            System.out.println(greeting + " From Inner");
            System.out.println(Outer.this.greeting);
        }
    }

    public void outerCallInner() {
        final Inner in = new Inner();
        in.go();
    }

    public static void main(String[] args) {
        final Outer.Inner in = new Outer().new Inner();
        in.go();
    }
}
