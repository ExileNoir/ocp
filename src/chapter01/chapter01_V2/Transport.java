package chapter01.chapter01_V2;

public class Transport {
    static interface Vehicule {
    }

    static class Bus implements Vehicule {
    }

    static class Van extends Bus {
    }

    public static void main(String[] args) {
        Bus bus = new Bus();
        Van van = new Van();
        Van[] vans = new Van[10];
        A a = new A();

        System.out.println(bus instanceof Vehicule);
        System.out.println(van instanceof Vehicule);
        System.out.println(vans instanceof Vehicule[]);
        System.out.println(null instanceof Vehicule);
        System.out.println(a instanceof Vehicule);
    }
}
