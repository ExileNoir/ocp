package chapter08.functionalInterfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.function.IntPredicate;
import java.util.function.Predicate;

import static java.lang.System.out;

public class WorkingPredicates {
    public static void main(String[] args) {

        final Dog boi = new Dog("Boi", "55", "100");
        final Dog clover = new Dog("Clover", "120", "2500");

        final Predicate<Dog> p = d -> Integer.parseInt(d.getAge()) > 9;
        out.println("is Boi older then 9? " + p.test(boi));
        out.println("is Clover older then 9? " + p.test(clover));


        final List<Dog> dogs = new ArrayList<>();
        dogs.add(boi);
        dogs.add(clover);

        final Predicate<Dog> findBs = dog -> dog.getName().startsWith("B");
        dogs.removeIf(findBs);
        dogs.forEach(dog -> out.println(dog));

        final IntPredicate universeAnswer = i -> i == 69;
        out.println("\nis the answer 69? " + universeAnswer.test(69));
    }
}

/*
 * PREDICATES:
 * * test things
 * * functional method: test()
 * * takes a value, does a logical test, returns boolean
 * */