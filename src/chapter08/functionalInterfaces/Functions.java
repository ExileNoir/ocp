package chapter08.functionalInterfaces;

import java.util.Map;
import java.util.TreeMap;

import static java.lang.System.out;

 class Functions {
    public static void main(String[] args) {

        final Map<String, String> aprilWinner = new TreeMap<>();
        aprilWinner.put("April 2017", "Bob");
        aprilWinner.put("April 2016", "Annette");
        aprilWinner.put("April 2015", "Lamar");

        out.println("--- List, before checking April 2014 ---");
        aprilWinner.forEach((k, v) -> out.println(k + " : " + v));

        // no key for April 2014, so John Doe gets added to the map
        aprilWinner.computeIfAbsent("April 2014", k -> "John Doe");

        // Key April 2014 now has a value, so Jane won't be added
        aprilWinner.computeIfAbsent("April 2014", (k) -> "Jane Doe");

        out.println("\n--- List, After checking April 2014 ---");
        aprilWinner.forEach((k, v) -> out.println(k + " : " + v));

        // use a BiFunction to replace all values in the map with UpperCases
        aprilWinner.replaceAll((key, oldValue) -> oldValue.toUpperCase());
        out.println("\n--- List, After replacing values with uppercase ---");
        aprilWinner.forEach((k, v) -> out.println(k + " : " + v));
    }
}
