package chapter08.functionalInterfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.DoubleSupplier;
import java.util.function.IntConsumer;

public class ConsumerInt {
    public static void main(String[] args) {

        int input = 200;
        final IntConsumer intConsumer = (d) -> System.out.println("input was " + d);
        intConsumer.accept(input);


        final DoubleSupplier doubleSupplier = () -> 200D;
        final double output = doubleSupplier.getAsDouble();
        System.out.println("Double is: " + output);


        final Map<String, String> map = new HashMap<>();
        map.put("Constance", "Daddy Loves his little girl");
        map.put("ExileNoir", "Locked up during Covid19");
        map.forEach((k, v) -> {
            if (k.equalsIgnoreCase("constance")) {
                System.out.println(v);
            }
        });


        final List<String> list = new ArrayList<>();
        list.add("Constance");
        list.forEach(e -> {
            if (e.equalsIgnoreCase("constance"))
                System.out.println("SuperGirlPower");
        });

    }
}
