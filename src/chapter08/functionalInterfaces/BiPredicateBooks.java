package chapter08.functionalInterfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiPredicate;

class Book {
    private String name;
    private double price;

    public Book(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}

public class BiPredicateBooks {
    public static void main(String[] args) {

        final Book book1 = new Book("Your Brain is Better with Java", 58.99);
        final Book book2 = new Book("OCP8 Java Certification Study Guide", 53.39);
        final Book book3 = new Book("Is Java Coffee or Programming", 39.86);
        final Book book4 = new Book("While you were out Java Happened", 12.99);

        final List<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);
        books.add(book3);
        books.add(book4);

        final BiPredicate<String, Double> javaBuy = (name, price) -> name.contains("Java");
        final BiPredicate<String, Double> priceBuy = (name, price) -> price < 55.00;
        final BiPredicate<String, Double> definitelyBuy = javaBuy.and(priceBuy);

        books.forEach(book -> {
            if (definitelyBuy.test(book.getName(), book.getPrice()))
                System.out.println("You should definitely buy " + book.getName() + "(" + book.getPrice() + ")");
        });

    }
}
