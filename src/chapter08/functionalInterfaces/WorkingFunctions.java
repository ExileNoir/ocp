package chapter08.functionalInterfaces;

import java.util.function.*;

import static java.lang.System.out;

public class WorkingFunctions {
    public static void main(String[] args) {

        final Function<Integer, String> answer = a -> {
            if (a == 42)
                return "Forty-two";
            else
                return "No answer for you";
        };

        out.println(answer.apply(42));
        out.println(answer.apply(69));


        final IntFunction<String> intFunction = i -> {
            if (i == 69) {
                return "I LOVE";
            } else {
                return "Booo";
            }
        };
        final String output = intFunction.apply(69);
        out.println("do I get 69? " + output);


        final IntToDoubleFunction intToDoubleFunction = d -> d * 5.0;
        final double compute = intToDoubleFunction.applyAsDouble(2);
        out.println("compute = " + compute);


        // BiFunction: Takes 2 Strings and Returns 1 String
        final BiFunction<String, String, String> firstLast = (first, last) -> first + " " + last;
        out.println("First and Last name: " + firstLast.apply("Steven", "Deseure"));


        final BiFunction<Integer, String, String> concat = (i, s) -> i + 64 + " " + s;
        out.println("concat is: " + concat.apply(5, "iLove"));


        final UnaryOperator<Double> log2 = v -> Math.log(v) / Math.log(v);
        out.println(log2.apply(0.8));
    }
}

/*
 * FUNCTIONS:
 * * most generic (most abstract) of the functional Interfaces
 * * functional Method: apply()
 * * purpose of apply(): is to take one value and turn it into another
 * * are variations of suppliers, consumers, predicates and functions
 * */


