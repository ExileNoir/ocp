package chapter08.functionalInterfaces;

import java.util.Map;
import java.util.Random;
import java.util.function.IntSupplier;
import java.util.function.Supplier;

import static java.lang.System.out;

public class WorkingSuppliers {
    public static void main(String[] args) {

        final Supplier<Integer> answerSupplier = () -> 42;
        out.println("Answer to everything: " + answerSupplier.get());


        final Supplier<Integer> answerSupplierInnerClass = new Supplier<Integer>() {
            @Override
            public Integer get() {
                return 69;
            }
        };
        out.println("And the Greatest number is: " + answerSupplierInnerClass.get());


        final Supplier<String> userSupplier = () -> {
            final Map<String, String> env = System.getenv();    // get the System environment map
            return env.get("USERNAME");                        // get the value with the key <<USER>> from the map and return it
        };
        out.println("User is: " + userSupplier.get());



        /* Use of IntSupplier */
        final Random random = new Random();
        final IntSupplier randomIntSupplier = () -> random.nextInt(50);
        final int myRandom = randomIntSupplier.getAsInt();
        out.println("Random number: " + myRandom);

    }
}

/*
 * SUPPLIERS:
 * * Supply Results
 * * functional Method: get()
 * * Never takes an argument, ALWAYS returns something
 * */