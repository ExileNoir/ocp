package chapter08.functionalInterfaces;

import java.util.ArrayList;
import java.util.function.Predicate;

import static java.lang.System.out;

class Cat {
    private String name;
    private int weight;
    private int age;

    public Cat(String name, int weight, int age) {
        this.name = name;
        this.weight = weight;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return this.name + " is " + this.getAge() + " years old and weighs " + this.weight + "pounds";
    }

    public void miauw() {
        out.println("Miauw!");
    }
}

public class PredicatesCats {
    public static void main(String[] args) {
        final Cat boi = new Cat("Boi", 30, 6);
        final Cat clover = new Cat("Clover", 35, 12);
        final Cat aiko = new Cat("Aiko", 50, 10);
        final Cat zooey = new Cat("Zooey", 45, 8);
        final Cat charis = new Cat("Charis", 120, 7);

        final ArrayList<Cat> cats = new ArrayList<>();
        cats.add(boi);
        cats.add(clover);
        cats.add(aiko);
        cats.add(zooey);
        cats.add(charis);

        out.println("-- All Cats --");
        cats.forEach(out::println);

        out.println("\n-- Cats younger then 9 --");
        cats.forEach(cat -> {
            if (cat.getAge() < 9) out.println(cat);
        });
        out.println();

        printCatsIf(cats, cat -> cat.getAge() < 9);

        out.println("\n-- Cats older then 9 --");
        printCatsIf(cats, cat -> cat.getAge() > 9);

        // removeIf()
        final Predicate<Cat> findCs = cat -> cat.getName().startsWith("C");
        cats.removeIf(findCs);
        out.println("\n-- After removing cats whose names begin with C --");
        cats.forEach(out::println);
//        cats.forEach(cat -> out.println(cat));

        /* Predicate's Default && Static methods: and(), or(), negate() && isEqual()  */
        final Predicate<Cat> age = cat -> cat.getAge() == 6;
        out.println("\nIs Boi NOT 6? " + age.negate().test(boi));

        final Predicate<Cat> name = cat -> cat.getName().equalsIgnoreCase("boi");
        final Predicate<Cat> nameAndAge = cat -> name.and(age).test(cat);
        final Predicate<Cat> nameAndAgeShort = name.and(age);
        final Predicate<Cat> nameOrAge = name.or(age);

        out.println("\n-- Test name AND age of boi --");
        out.println("is Boi named 'boi' and age 6? " + nameAndAge.test(boi));
        boi.setAge(7);
        out.println("is Boi named 'boi' and age 6? " + nameAndAge.test(boi));
        out.println("is Boi named 'boi' or has age 6? " + nameOrAge.test(boi));

        final Predicate<Cat> equalCat = Predicate.isEqual(zooey);
        out.println("\nis aiko the same object as zooey? " + equalCat.test(aiko));
        out.println("is zooey the same object as zooey? " + equalCat.test(zooey));
    }


    static void printCatsIf(final ArrayList<Cat> cats, final Predicate<Cat> predicate) {
        for (final Cat cat : cats) {
            if (predicate.test(cat)) {
                out.println(cat);
            }
        }
    }
}
