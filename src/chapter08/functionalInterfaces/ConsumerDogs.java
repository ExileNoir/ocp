package chapter08.functionalInterfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static java.lang.System.out;

class Dog {
    private String name;
    private String age;
    private String weight;

    public Dog(String name, String age, String weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public String getAge() {
        return age;
    }

    public String getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public void bark() {
        out.println("Woof!");
    }

}

public class ConsumerDogs {
    public static void main(String[] args) {
        final Dog boi = new Dog("Boi", "30", "6");
        final Dog clover = new Dog("Clover", "55", "55");
        final Dog ceasar = new Dog("Ceasar", "20", "10");

        final List<Dog> dogs = new ArrayList<>();
        dogs.add(boi);
        dogs.add(clover);
        dogs.add(ceasar);

        final Consumer<Dog> displayNames = d -> out.print(d + " ");
//        dogs.forEach(displayNames);

        /* andThe() :  use to chain consumers together */
        dogs.forEach(displayNames.andThen(dog -> dog.bark()));

        final Consumer<Dog> doBark = Dog::bark;
        dogs.forEach(displayNames.andThen(doBark));
    }
}
