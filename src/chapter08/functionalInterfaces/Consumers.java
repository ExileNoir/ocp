package chapter08.functionalInterfaces;

import java.util.Map;
import java.util.function.BiConsumer;

import static java.lang.System.getenv;
import static java.lang.System.out;

public class Consumers {
    public static void main(String[] args) {

        final Map<String, String> env = getenv();
        final User user = new User();

        final BiConsumer<String, String> findUserName = (key, value) -> {
            if (key.equalsIgnoreCase("username"))
                user.setUsername(value);
        };

        env.forEach(findUserName);
        out.println("Username form env: " + user.getUsername());

    }
}

class User {
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

/*
 * We can't modify a variable from within a lambda expression, we CAN modify a field of an object
 * */