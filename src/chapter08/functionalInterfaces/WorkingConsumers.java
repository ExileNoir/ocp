package chapter08.functionalInterfaces;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import static java.lang.System.out;

public class WorkingConsumers {
    public static void main(String[] args) {

        final Consumer<String> redOrBlue = pill -> {
            if (pill.equalsIgnoreCase("red"))
                out.println("Down the Rabbit Hole");
            if (pill.equalsIgnoreCase("blue"))
                out.println("Stay in LaLa Land");
        };
        redOrBlue.accept("red");


        /* BiConsumer */
        final Map<String, String> env = System.getenv();
        final BiConsumer<String, String> printEnv = (key, value) -> {
            out.println("key: " + key + " Value: " + value);
        };
        printEnv.accept("username", env.get("USERNAME"));
//        env.forEach(printEnv);


        /* ForEach use with Consumer */
        final List<String> dogNames = Arrays.asList("Boi", "Clover", "Ceasar");
        final Consumer<String> printName = name -> out.println(name);
        dogNames.forEach(printName);

        dogNames.forEach(name -> out.println(name));
        dogNames.forEach(out::println);

    }
}

/*
 * CONSUMERS:
 * * consume values
 * * functional method: accept()
 * * ALWAYS takes an argument, NEVER returns anything
 * */
