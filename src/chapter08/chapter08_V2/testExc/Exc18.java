package chapter08.chapter08_V2.testExc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

class Guitar {
    void readMusic(File f) {
        try (BufferedReader reader = new BufferedReader(new FileReader(f))) {
            String music = null;
            try {
                while ((music = reader.readLine()) != null) {
                    System.out.println(music);
                }
            } catch (IOException e) {
                // catch IOE
            }
        } catch (/*FileNotFoundException |*/ IOException e) {  // cannot be siblings
            throw new RuntimeException(e);
        }
    }
}

public class Exc18 {

}
