package chapter08.chapter08_V2.testExc;

import java.io.*;

public class Exc29 {

    public void CopyPidgin(File s, File t) throws Exception {
        try (InputStream is = new FileInputStream(s);
             OutputStream os = new FileOutputStream(t)) {

            byte[] data = new byte[123];
            int chirps;
            while ((chirps = is.read(data)) > 0) {
//              os.write(data);                      // copies contents of some files
                os.write(data, 0, chirps);      // copies the contents of all files
            }
        }
    }
}
