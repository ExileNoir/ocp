package chapter08.chapter08_V2.testExc;

import java.io.*;

import static java.lang.System.*;

class Cereal implements Serializable {
    private String name = "CocoaCookies";
    private transient int sugar;

    public Cereal() {
        super();
        this.sugar = 100;
        this.name = "CaptainPebbles";
    }

    {
        name = "SugarPops";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSugar() {
        return sugar;
    }

    public void setSugar(int sugar) {
        this.sugar = sugar;
    }

    public String toString() {
        return getName() + " "+getSugar();
    }
}

public class Exc14 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        final Cereal c = new Cereal();
        c.setName("CornLoops");

        try (final ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("cereal.data"));
             final ObjectInputStream ois = new ObjectInputStream(new FileInputStream("cereal.data"))) {
            oos.writeObject(c);

            final Object o = ois.readObject();
            if (o instanceof Cereal) {
                final Cereal cerealCopy = (Cereal) o;

                out.println(cerealCopy);
            }

        }
    }
}
