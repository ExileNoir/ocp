package chapter08.chapter08_V2.testExc;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;

class Itinerary {
    private List<String> activities = new ArrayList<>();

    private static Itinerary getItinerary(String name) {
        return null;
    }

    static void printItinerary() throws Exception {
//      Console c = new Console();      // Constructor for Console == private
        Console c = System.console();

        final String name = c.readLine("What is your name?");
        final Itinerary stuff = getItinerary(name);
        stuff.activities.forEach(s -> c.printf(s));
    }
}

public class Exc12 {
    public static void main(String[] args) throws Exception {
        Itinerary.printItinerary();
    }
}
