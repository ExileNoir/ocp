package chapter08.chapter08_V2.testExc;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

public class Exc23 {
    public static void main(String[] args) {

        File file = new File("/Earth");
        out.println(file.getParent());                         // R String
//      out.println(file.getParent().getParent());
        out.println(file.getParentFile().getParent());

        Path path = Paths.get("/Earth");
        out.println(path.getParent());                       // R Path
        out.println(path.getParent().getParent());
//      out.println(path.getParent().getParent().getParent());      // NullPointerException
    }
}
