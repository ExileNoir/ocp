package chapter08.chapter08_V2.testExc;

import java.io.File;
import java.util.stream.Stream;

public class Exc25 {
    static void deleteTree(File f) {
        if (!f.isDirectory()) {
            f.delete();
        } else {
            Stream.of(f.list())               // returns Stream of String[]
                    .forEach(s -> deleteTree(new File(s)));
        }
    }

    static void deleteTree1(File f) {
        if (!f.isDirectory()) {
            f.delete();
        } else {
            Stream.of(f.listFiles())        // returns Stream of File[]
                    .forEach(s -> deleteTree1(s));
        }
    }
}
