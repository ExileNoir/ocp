package chapter08.chapter08_V2.testExc;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static java.lang.System.out;

public class Exc09 {

    private static final String LINK = "C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01.txt";

    public static void main(String[] args) throws IOException {

        try (/*final InputStream is = new BufferedInputStream(new FileInputStream(LINK));*/
                final BufferedReader is = new BufferedReader(new FileReader(LINK))) {
            is.skip(1);
            is.read();
            is.skip(1);
            is.read();
            is.mark(4);
            is.skip(1);
            is.reset();
            out.println((char) is.read());
        }
    }
}
/*
 * FileInputStream does not support marks, leads to IOException at runtime when reset() is called
 * markSupported()
 * */