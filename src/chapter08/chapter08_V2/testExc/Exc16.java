package chapter08.chapter08_V2.testExc;

import java.io.File;

import static java.lang.System.out;

public class Exc16 {

    private static final String LINK = "C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OcpTxtCreateIO";

    private static void resetWorkingDirectory() throws Exception {
        File f1 = new File(LINK, "templates/proofs");
        boolean one = f1.mkdirs();
        out.println("Dir 1: " + one);

        File f2 = new File(LINK, "templates");
        boolean two = f2.mkdir();
        out.println("Dir 2: " + two);

        boolean three = new File(f2, "draft.doc").createNewFile();
        out.println("File: " + three);

        boolean four = f1.delete();
        out.println("Delete 1: " + four);

        boolean five = f2.delete();
        out.println("Delete 2: " + five);
    }

    public static void main(String[] args) throws Exception {
        resetWorkingDirectory();
    }
}
