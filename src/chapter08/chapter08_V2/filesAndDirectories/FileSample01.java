package chapter08.chapter08_V2.filesAndDirectories;

import java.io.File;
import java.nio.file.Files;

import static java.lang.System.out;

public class FileSample01 {
    public static void main(String[] args) {

        out.println(System.getProperty("file.separator"));
        out.println(File.separator);
        final char sep = new File(new String()).separatorChar;


        final File file = new File("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01.txt");
        final File file1 = new File("C:/Users/Steven/Desktop/PROJECTS/Java/OCP/OCPprep/dvdInfo01.txt");

        out.println(file.exists());
        out.println(file1.exists());

        final File parent = new File("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java");
        final File child = new File(parent, "OCP\\OCPprep\\dvdInfo01.txt");

        out.println(child.exists());                 // boolean
        out.println(child.getName());                // String
        out.println(child.getAbsolutePath());        // String
        out.println(child.isDirectory());            // boolean
        out.println(parent.isDirectory());
        out.println(child.isFile());                 // boolean
        out.println(parent.isFile());                // boolean
        out.println(child.getParent());              // String
        out.println(parent.getParent());
//      out.println(parent.delete());              // RETURNS A BOOLEAN
    }
}
