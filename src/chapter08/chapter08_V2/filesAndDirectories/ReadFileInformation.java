package chapter08.chapter08_V2.filesAndDirectories;

import java.io.File;

import static java.lang.System.out;

public class ReadFileInformation {
    public static void main(String[] args) {

//      final File file = new File("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01.txt");
        final File file = new File("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep");

        out.println("File Exists: " + file.exists());

        final boolean fileExists = file.exists();
        if (fileExists) {
            out.println("Absolute Path: " + file.getAbsolutePath());
            out.println("Is Directory: " + file.isDirectory());
            out.println("Parent Path: " + file.getParent());

            final boolean isFile = file.isFile();
            if (isFile) {
                out.println("File size: " + file.length());
                out.println("File last Modified: " + file.lastModified());
            } else {
                for (File subFile : file.listFiles()) {
                    out.println("\t" + subFile.getName());
                }
            }
        }
    }
}
