package chapter08.chapter08_V2.streams;

import java.io.*;

public class CopyFileSample01 {

    private static void copy(final File source, final File dest) throws IOException {
        try (final InputStream in = new FileInputStream(source);
             final OutputStream out = new FileOutputStream(dest)) {

            int b;
            while ((b = in.read()) != -1) {      // in reads one byte at a time
                out.write(b);                   // copies the value to the out as in reads
                out.flush();                   // not necessary, auto when sources are closed!
            }
        }
    }


    public static void main(String[] args) {
        final File source = new File("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\src\\chapter08\\functionalInterfaces\\Functions.java");
        final File dest = new File("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\src\\chapter08\\chapter08_V2\\streams\\FunctionsCopy.java");
        try {
            copy(source, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
