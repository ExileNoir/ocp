package chapter08.chapter08_V2.streams;

import java.io.*;

import static java.lang.System.out;

public class SkippingData {
    public static void main(String[] args) {

        try (final InputStream is = new BufferedInputStream(
                new FileInputStream(
                        new File("tigers.txt")))) {

            is.mark(5);

            out.println((char) is.read());

            is.skip(2);
            is.read();

            out.println((char) is.read());
            out.println((char) is.read());

            is.reset();
            out.println((char) is.read());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
