package chapter08.chapter08_V2.streams;

import java.io.*;

public class CopyBufferFileSample02 {

    private static void copy(final File source, final File dest) throws IOException {
        try (final InputStream in = new BufferedInputStream(new FileInputStream(source));
             final OutputStream out = new BufferedOutputStream(new FileOutputStream(dest))) {

            final byte[] buffer = new byte[1240];       // buffer into which the data is read
            final int offSet = 0;
            int lengthRead;

            while ((lengthRead = in.read(buffer)) > 0) {
                out.write(buffer, offSet, lengthRead);
                out.flush();
            }
        }
    }

    public static void main(String[] args) {
        final File source = new File("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\src\\chapter08\\functionalInterfaces\\Functions.java");
        final File dest = new File("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\src\\chapter08\\chapter08_V2\\streams\\FunctionsCopy.java");
        try {
            copy(source, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
