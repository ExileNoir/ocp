package chapter08.chapter08_V2.streams;

import java.io.Console;
import java.io.IOException;
import java.util.Arrays;

public class PwdConsole {
    public static void main(String[] args) throws NumberFormatException, IOException {

        final Console console = System.console();

        if (console == null) {
            throw new RuntimeException("Console not available");
        } else {
            final char[] pwd = console.readPassword("Enter your password: ");       // returns a Character Array!!!

            console.format("Enter your password again: ");
            console.flush();

            final char[] verify = console.readPassword();
            final boolean match = Arrays.equals(pwd, verify);

            // Immediately clear pwd from memory
            Arrays.fill(pwd, 'X');

            for (int i = 0; i < verify.length; i++) {
                verify[i] = 'x';
            }
            console.format("Your Password was: " + (match ? "Correct" : "Incorrect"));
        }
    }
}
