package chapter08.chapter08_V2.streams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SystemInSampleOldWay {
    public static void main(String[] args) throws IOException {
        System.out.println("input text");

        final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        final String userInput = reader.readLine();
        System.out.println("You entered the following: " + userInput);
    }
}
