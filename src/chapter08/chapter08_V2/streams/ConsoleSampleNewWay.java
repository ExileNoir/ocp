package chapter08.chapter08_V2.streams;

import java.io.Console;

public class ConsoleSampleNewWay {
    public static void main(String[] args) {
        final Console console = System.console();

        if (console != null) {
            final String userInput = console.readLine("Give Us Something...");
            console.writer().println("You entered the following: " + userInput);
        } else System.out.println("fuck");
    }
}
