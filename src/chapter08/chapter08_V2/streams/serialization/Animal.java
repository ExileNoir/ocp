package chapter08.chapter08_V2.streams.serialization;

import java.io.Serializable;

import static java.lang.System.out;

class WarAnimal/* implements Serializable*/ {

    public WarAnimal() {
        out.println("War Animals of Lord Karas");       // when not serialized this class, then no-arg Constructor is called when child is deserialized
    }

    protected String m = "War Animal";

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }
}

public class Animal extends WarAnimal implements Serializable {
    private static final long serialVersionIUD = 1L;
    private transient String name;                    // value won't be included in serialization due to 'transient'
    private int age;
    private static char type;                       // last Object input will be used here for all instances
    private /*transient*/ Tail tail;               // NotSerializableException (RunTimeExc) when writing || give var transient if Serializable is NOT impl in class signature

    public Animal(String name, int age, char type) {
        this.name = name;
        this.age = age;
        this.type = type;
    }

    public Animal(String name, int age, char type, Tail tail) {
        this.name = name;
        this.age = age;
        this.type = type;
        this.tail = tail;
    }


    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public char getType() {
        return type;
    }

    public Tail getTail() {
        return tail;
    }

    public String toString() {
        return "Animal [name= " + name + ", age= " + age + ", type= " + type + " " + getTail() + " " + m + "]";
    }
}


class Tail implements Serializable {
    private String tail;

    public Tail(String tail) {
        this.tail = tail;
    }

    public Tail() {
        tail = "Tail exists";
    }

    public String getTail() {
        return tail;
    }

    public String toString() {
        return "has a " + getTail();
    }
}
