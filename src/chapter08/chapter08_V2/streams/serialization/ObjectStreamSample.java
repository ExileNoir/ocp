package chapter08.chapter08_V2.streams.serialization;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

public class ObjectStreamSample {
    private static List<Animal> getAnimals(final File dataFile) throws IOException, ClassNotFoundException {
        final List<Animal> animals = new ArrayList<>();

        try (final ObjectInputStream in = new ObjectInputStream(    // deserialize object   == read
                new BufferedInputStream(
                        new FileInputStream(dataFile)))) {        // were to get data from

            while (true) {
                final Object o = in.readObject();               // read to content to Object
                if (o instanceof Animal) {                     // to avoid classCastExc
                    animals.add((Animal) o);
                }
            }
        } catch (EOFException e) {                         // mainly used by data input streams to signal end of stream
            // File end reached
        }
        return animals;
    }


    private static void createAnimalsFile(final List<Animal> animals, final File dataFile) throws IOException {
        try (final ObjectOutputStream out = new ObjectOutputStream(         // serialize object    == write
                new BufferedOutputStream(
                        new FileOutputStream(dataFile)))) {     // were to safe
            for (final Animal animal : animals) {
                out.writeObject(animal);                      // writing the content the list argument
            }
        }
    }


    public static void main(String[] args) {
        final List<Animal> animals = new ArrayList<>();
        animals.add(new Animal("Tommy Tiger", 5, 'T'));
        animals.add(new Animal("Peter Pinguin", 8, 'P'));

        animals.add(new Animal("Lynch", 100, 'L', new Tail("Striped War Tail")));

        final Animal bear = new Animal("Bear", 20, 'B', new Tail("Small Piked Tail"));
        bear.setM("Shogun WarBear");
        animals.add(bear);

        final File dataFile = new File("animal.data");
        try {
            createAnimalsFile(animals, dataFile);

            final List<Animal> animals1 = getAnimals(dataFile);
            for (final Animal animal : animals1) {
                out.println(animal);
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
/*
 * When you Deserialize (read) an object, The constructor of the serialized class is NOT called!
 * FACT: Java calls the first no-arg Constructor for the first NON-serializable parent class,
 * Skipping the Constructors of any serialized class in between!
 * FURTHERMORE: any static variable || default initializations are ignored
 * */