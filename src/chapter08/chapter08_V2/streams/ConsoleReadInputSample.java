package chapter08.chapter08_V2.streams;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;

public class ConsoleReadInputSample {
    public static void main(String[] args) throws NumberFormatException, IOException {
        final Console console = System.console();

        if (console == null) {
            throw new RuntimeException("Console not available");
        } else {
            console.writer().print("How Excited are you about your trip today? ");
            console.flush();

            final String excitementAnswer = console.readLine();
            final String name = console.readLine("Please enter your name: ");

            Integer age = null;
            console.writer().print("What is your age? ");
            console.flush();

            final BufferedReader reader = new BufferedReader(console.reader());
            final String value = reader.readLine();
            age = Integer.valueOf(value);
            console.writer().println();

            console.format("Your name is: " + name);
            console.writer().println();
            console.format("Your age is: " + age);
            console.printf("\nYour excitement level is: " + excitementAnswer);
        }
    }
}
