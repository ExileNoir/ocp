package chapter08.chapter08_V2.streams;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static java.lang.System.out;

public class MarkReset {
    public static void main(String[] args) {

        try (final InputStream is = new BufferedInputStream(
                new FileInputStream("markReset.txt"))) {

            out.println((char) is.read());

            final boolean markSupported = is.markSupported();
            if (markSupported) {
                is.mark(100);            // IF you call reset() after you passed your mark() read limit an exception
                out.println((char) is.read());   // MAY be thrown (buffer could be used to allow extra data to be read)
                out.println((char) is.read());
                is.reset();
            }
            out.println((char) is.read());
            out.println((char) is.read());
            out.println((char) is.read());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
