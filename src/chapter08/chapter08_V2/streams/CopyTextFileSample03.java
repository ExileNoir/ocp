package chapter08.chapter08_V2.streams;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

public class CopyTextFileSample03 {
    private static List<String> readFile(final File source) throws IOException {
        final List<String> data = new ArrayList<>();

        try (final BufferedReader reader = new BufferedReader(new FileReader(source))) {

            String s;
            while ((s = reader.readLine()) != null) {       // we check NULL value!!  --   With FIS && FR we check -1 value
                data.add(s);
            }
        }
        return data;
    }

    private static void writeFile(final List<String> data, final File dest) throws IOException {
        try (final BufferedWriter writer = new BufferedWriter(new FileWriter(dest))) {
            for (final String s : data) {
                writer.write(s);
                writer.newLine();
            }
        }
    }


    public static void main(String[] args) {
        final File source = new File("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01.txt");
        final File dest = new File("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01COPY.txt");

        try {
            final List<String> data = readFile(source);

            for (final String s : data) {
                out.println(s);
            }

            writeFile(data, dest);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
