package chapter08.lambdaExpressionSyntax;

import static java.lang.System.out;

class Dog {
    private String name;
    private int weight;
    private int age;

    public Dog(String name, int weight, int age) {
        this.name = name;
        this.weight = weight;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public void bark() {
        out.println("Woof!");
    }
}

/* Functional Interface */
@FunctionalInterface
interface DogQuerier {
    public abstract boolean test(final Dog dog);
}

/* Passing Lambda Expressions to Methods */
class DogsPlay {
    private DogQuerier dogQuerier;

    public DogsPlay(final DogQuerier dq) {
        this.dogQuerier = dq;
    }

    public boolean doQuery(final Dog dog) {
        return dogQuerier.test(dog);
    }
}

public class TestDogs {
    public static void main(String[] args) {

        final Dog boi = new Dog("boi", 30, 6);
        final Dog clover = new Dog("Clover", 35, 12);

        // This inner Class we can replace with lambda
        final DogQuerier dq = new DogQuerier() {
            @Override
            public boolean test(final Dog dog) {
                return dog.getAge() > 9;
            }
        };

        // replaces the Inner Class
        final DogQuerier dq1 = dog -> dog.getAge() > 9;
        final DogQuerier dq2 = (Dog dog) -> dog.getAge() > 9;
        final DogQuerier dq3 = (dog) -> dog.getAge() > 9;
        final DogQuerier dq4 = dog -> {
            return dog.getAge() > 9;
        };

        out.println("Is Boi older then 9? " + dq1.test(boi));
        out.println("Is Clover older then 9? " + dq1.test(clover));

        // Passing Lambda Expression to Methods
        final DogsPlay dp = new DogsPlay(dq2);
        out.println("Is Clover older then 9? " + dp.doQuery(clover));

        final DogsPlay dp1 = new DogsPlay(dog -> dog.getAge() > 9);
        out.println("Is Boi older then 9? " + dp1.doQuery(boi));

        // Accessing Variables from Lambda Expressions
        int numCats = 3;
        final DogQuerier dqWithCats = d -> {
            int numBalls = 1;        // Completely new variable local to lambda
            numBalls++;             // we can modify
//            numCats++;           // won't compile!! MUST be final || effectively final
            out.println("Number of balls: " + numBalls);
            out.println("Number of Cats: " + numCats);    // we can access numCats
            return d.getAge() > 9;
        };
        out.println("Is catClover older then 9? " + dqWithCats.test(clover));

        out.println("--- use DogsPlay ---");
        final DogsPlay dp2 = new DogsPlay(dqWithCats);
        out.println("Is Clover older then 9? " + dp2.doQuery(clover));

    }
}
