package chapter08.methodReferences;

import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;

public class MethodRefs {
    public static void main(String[] args) {

        final List<String> trees = Arrays.asList("fir", "cedar", "pine");

        trees.forEach(t -> out.println(t));        // print with lambda
        trees.forEach(out::println);              // print with Method Reference

        trees.forEach(MethodRefs::printTreeStatic);     // print with own static method reference

        trees.forEach(new MethodRefs()::printThree);
    }

    static void printTreeStatic(final String t) {
        out.println("Tree name: " + t);
    }

    void printThree(final String t) {
        out.println("non static tree name: " + t);
    }
}
