package chapter08.methodReferences;

import static java.lang.System.out;

@FunctionalInterface
interface TriPredicate<T, U, V> {
    public abstract boolean test(final T t, final U u, final V v);
}

public class TestTriPredicate {
    public static void main(String[] args) {

        final TriPredicate<String, Integer, Integer> theTest = (s, n, w) -> {
            return s.equalsIgnoreCase("There is no spoon") && n > 2 && w < n;
        };

        out.println("Pass the test? " + theTest.test("Follow the white rabbit", 2, 3));
        out.println("Pass the test? " + theTest.test("There is no spoon", 101, 3));
    }
}