package chapter05.fileNavigationAndIO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static java.lang.System.out;

/* FileOutputStream && FileInputStream for byte data
 * -> read & write; Binary Data && Text Data
 *  */
public class Writer03 {
    public static void main(String[] args) throws IOException {

        final byte[] in = new byte[50];     // bytes, not chars!
        int size;

        final File file = new File("fileWrite03.txt");

        try (final FileOutputStream fos = new FileOutputStream(file);      // create a FileOutPutStream
             final FileInputStream fis = new FileInputStream(file)) {     // create a FileInputSteam
            final String s = "All Hail Lord\nKaras\nThe God of War";

            fos.write(s.getBytes(StandardCharsets.UTF_8));     // write chars (bytes) to the file
            fos.flush();                  // flush before closing
            // we don't need to close << fos >> due to try with Resources, autoClose

            size = fis.read(in);       // read the file into in
            out.println(size + " ");
            for (final byte b : in) {
                out.print((char) b);
            }
            // we don't need to close << fis >> due to try with Resources, autoClose
        }
    }
}
