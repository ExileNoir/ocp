package chapter05.fileNavigationAndIO;

import java.io.Console;

public class newConsole {
    public static void main(String[] args) {

        String name = "";
        final Console console = System.console();       // 1#: get a Console

        char[] pw;
        pw = console.readPassword("%s", "pw: ");     // #2: return a char[]

        for (final char c : pw) {
            console.format("%console ", c);        // #3: format output
        }
        console.format("\n");

        final MyUtility mu = new MyUtility();
        while (true) {
            name = console.readLine("%s", "intput?: ");  // #4: return a String

            console.format("output: %s \n", mu.doStuff(name));
        }
    }
}

class MyUtility {            // #5: Class to test
    String doStuff(final String arg1) {
        // stub code
        return "result is " + arg1;
    }
}