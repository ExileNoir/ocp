package chapter05.fileNavigationAndIO;

import java.io.*;

import static java.lang.System.out;

public class Writer05 {
    public static void main(String[] args) {

        final File file = new File("fileWrite05.txt");
        final String[] content = new String[50];

        // before java 5
        try (final PrintWriter pw = new PrintWriter(new FileWriter(file), true);
             final BufferedReader br = new BufferedReader(new FileReader(file))) {
            pw.println("A little Angel was born");
            pw.println("Constance");
            pw.println("Deseure Stevenin");

            String el;
            int counter = 0;
            while ((el = br.readLine()) != null) {
                out.println(el);
                content[counter++] = el;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        out.println("\nArray content: ");
        for (final String s : content) {
            if (s != null)
                out.println(s);
        }
    }
}
