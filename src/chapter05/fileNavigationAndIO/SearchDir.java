package chapter05.fileNavigationAndIO;

import java.io.File;

import static java.lang.System.*;

public class SearchDir {
    public static void main(String[] args) {

        final File file = new File("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep");

        final String[] files = file.list();

        for (final String s : files) {
            out.println("Found: " + s);
        }
    }
}
