package chapter05.fileNavigationAndIO;

import java.io.Console;

import static java.lang.System.console;
import static java.lang.System.out;

public class ConsoleDemo {
    public static void main(String args[]) {
        String str;

        //Obtaining a reference to the console.
        final Console con = console();

        // Checking If there is no console available, then exit.
        if (con == null) {
            out.print("No console available");
            return;
        }

        // Read a string and then display it.
        str = con.readLine("Enter your name: ");
        con.printf("Here is your name: %s\n", str);

        //to read password and then display it
        out.println("Enter the password: ");
        char[] ch = con.readPassword();

        //converting char array into string
        final String pass = String.valueOf(ch);
        out.println("Password is: " + pass);
    }
}

