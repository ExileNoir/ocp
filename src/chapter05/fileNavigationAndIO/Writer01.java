package chapter05.fileNavigationAndIO;

import java.io.Console;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static java.lang.System.out;

public class

Writer01 {
    public static void main(String[] args) throws IOException {

        boolean newFile;
        final File file = new File("fileWrite01.txt");  // it's only an Object

        out.println(file.exists());         // look for a real file

        newFile = file.createNewFile();  // maybe create a file!! && returns boolean
        out.println(newFile);

        out.println(file.exists());

        final String d = File.separator + "d";
        out.println(d);

        /* First Run
         * false
         * true
         * true
         * */

        /* Second Run
         * true
         * false
         * true
         * */

    }
}
