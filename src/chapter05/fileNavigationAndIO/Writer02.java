package chapter05.fileNavigationAndIO;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import static java.lang.System.out;

/* FileWriter && FileReader for character Data */
public class Writer02 {
    public static void main(String[] args) throws IOException {

        final char[] in = new char[50];     // to store input
        int size;

        final File file = new File("FileWrite02.txt");    // just an Object

        try (final FileWriter fw = new FileWriter(file);          // Create an actual file && a FileWriter Object
             final FileReader fr = new FileReader(file)) {       // Create a FileReader Object
            fw.write("All Hail\nLord ExileNoir!");          // Write Chars to the file

            fw.flush();                // flush before closing

            // we don't need to close << fw >> due to try with Resources, autoClose

            size = fr.read(in);      // read the whole file into char Array
            out.println(size + " ");
            for (final char c : in) {
                out.print(c);
            }
            // we don't need to close << fr >> due to try with Resources, autoClose
        }
    }
}
