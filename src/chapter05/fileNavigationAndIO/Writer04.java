package chapter05.fileNavigationAndIO;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

public class Writer04 {
    public static void main(String[] args) throws IOException {

        final Path path = Paths.get("fileWrite04.txt");     // it's only an Object
        out.println(Files.exists(path));      // look for a real file

        Files.createFile(path);     // create a file
        out.println(Files.exists(path));

        /* If file already exists == FileAlreadyExistsException */

        /* file.createNewFile throws false if false already exists != Files.createFile */

    }
}
