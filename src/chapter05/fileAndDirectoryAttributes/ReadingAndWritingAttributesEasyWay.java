package chapter05.fileAndDirectoryAttributes;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.time.*;

import static java.lang.System.out;

public class ReadingAndWritingAttributesEasyWay {
    public static void main(String[] args) throws IOException {

        final String filePath = "C:/Users/Steven/Desktop/PROJECTS/Java/OCP/OcpTxtCreateIO/";

        final ZonedDateTime janFirstDateTime = ZonedDateTime.of(
                LocalDate.of(2017, 1, 1),
                LocalTime.of(10, 0),
                ZoneId.of("US/Pacific"));
        final Instant januaryFirst = janFirstDateTime.toInstant();

        // OLD WAY
        final File file = new File(filePath, "file.txt");
        file.createNewFile();

        file.setLastModified(januaryFirst.getEpochSecond() * 1000);       // set Time
        out.println(file.lastModified());                                // get Time

        file.canRead();
        file.canWrite();
        file.canExecute();
        file.delete();


        // NEW WAY
        final Path path = Paths.get(filePath, "file2.txt");
        Files.createFile(path);                            // create another File

        final FileTime fileTime = FileTime.fromMillis(januaryFirst.getEpochSecond() * 1000);      // FileTime Object
        Files.setLastModifiedTime(path, fileTime);       // set Time
        out.println(Files.getLastModifiedTime(path));   // get Time

        Files.isReadable(path);
        Files.isWritable(path);
        Files.isExecutable(path);
        Files.delete(path);
    }
}
