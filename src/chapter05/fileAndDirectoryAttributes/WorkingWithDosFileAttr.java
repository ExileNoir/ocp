package chapter05.fileAndDirectoryAttributes;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.DosFileAttributes;

import static java.lang.System.out;

public class WorkingWithDosFileAttr {
    public static void main(String[] args) throws IOException {

        final String filePath = "C:/Users/Steven/Desktop/PROJECTS/Java/OCP/OcpTxtCreateIO/";

        final Path path = Paths.get(filePath + "test.txt");
        Files.createFile(path);

        Files.setAttribute(path, "dos:hidden", true);     // set Attribute
        Files.setAttribute(path, "dos:readonly", true);  // if: readonly ==> IllegalArgumentEx

        final DosFileAttributes dos = Files.readAttributes(path, DosFileAttributes.class);
        out.println("Hidden " + dos.isHidden());
        out.println("ReadOnly " + dos.isReadOnly());

        Files.setAttribute(path, "dos:hidden", false);     // set Attribute again
        Files.setAttribute(path, "dos:readonly", false);

        final DosFileAttributes dos1 = Files.readAttributes(path, DosFileAttributes.class);
        out.println("Hidden " + dos1.isHidden());
        out.println("ReadOnly " + dos1.isReadOnly());

    }
}
