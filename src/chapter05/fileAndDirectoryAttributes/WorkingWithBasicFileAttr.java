package chapter05.fileAndDirectoryAttributes;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.*;

import static java.lang.System.out;

public class WorkingWithBasicFileAttr {
    public static void main(String[] args) throws IOException {

        final String filePath = "C:/Users/Steven/Desktop/PROJECTS/Java/OCP/OcpTxtCreateIO/";
        final Path path = Paths.get(filePath);

        final BasicFileAttributes basic = Files.readAttributes(path, DosFileAttributes.class);

        out.println("create " + basic.creationTime());
        out.println("access " + basic.lastAccessTime());
        out.println("modify " + basic.lastModifiedTime());
        out.println("directory " + basic.isDirectory());

        final FileTime lastUpdated = basic.lastModifiedTime();  // get current
        final FileTime created = basic.creationTime();         //  values
        final FileTime now = FileTime.fromMillis(System.currentTimeMillis());


        final BasicFileAttributeView basicView = Files.getFileAttributeView(path, DosFileAttributeView.class); // << view >> this time
        basicView.setTimes(lastUpdated, now, created);    // set all three

        out.println(basicView.readAttributes().creationTime());
    }
}
