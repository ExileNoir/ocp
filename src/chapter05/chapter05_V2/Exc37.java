package chapter05.chapter05_V2;

import java.time.LocalDate;
import java.time.Month;

import static java.lang.System.out;

public class Exc37 {
    public static void main(String[] args) {

        final LocalDate date1 = LocalDate.of(2017, Month.MARCH, 3);
        final LocalDate date2 = date1.plusDays(2).minusDays(1).minusDays(1);
        out.println(date1.equals(date2));
    }
}
