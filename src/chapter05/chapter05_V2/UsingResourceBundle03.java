package chapter05.chapter05_V2;

import java.util.Locale;
import java.util.ResourceBundle;

public class UsingResourceBundle03 {
    public static void main(String[] args) {

        Locale locale = new Locale("en","CA");
        ResourceBundle rb =  ResourceBundle.getBundle("Zoo",locale);
        System.out.println(rb.getString("hello"));
        System.out.println(rb.getString("name"));
        System.out.println(rb.getString("open"));
        System.out.println(rb.getString("visitor"));
    }
}
