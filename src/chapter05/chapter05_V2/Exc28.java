package chapter05.chapter05_V2;

import java.time.*;
import java.time.temporal.ChronoUnit;

public class Exc28 {
    public static void main(String[] args) {

        final LocalDate trainDay = LocalDate.of(2017, 5, 13);
        final LocalTime time = LocalTime.of(10, 0);
        final ZoneId zone = ZoneId.of("America/Los_Angeles");
        final ZonedDateTime zdt = ZonedDateTime.of(trainDay, time, zone);
        Instant instant = zdt.toInstant();

        instant = instant.plus(1, ChronoUnit.DAYS);
        System.out.println(instant);
    }
}
