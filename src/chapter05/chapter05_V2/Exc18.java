package chapter05.chapter05_V2;

import java.time.LocalTime;

import static java.lang.System.out;

public class Exc18 {
    public static void main(String[] args) {

        final LocalTime time = LocalTime.of(1, 2, 3, 4);
        out.println(time);
    }
}
