package chapter05.chapter05_V2;

import java.time.LocalDate;

import static java.lang.System.*;

public class DateTime02 {
    public static void main(String[] args) {

        LocalDate d1 = LocalDate.of(2019,8,25);
        out.println(d1);

        d1 = d1.plusDays(2);
        out.println(d1);

        d1 = d1.plusWeeks(1);
        out.println(d1);

        d1 = d1.plusMonths(1);
        out.println(d1);

        d1 = d1.plusYears(1);
        out.println(d1);


    }
}
