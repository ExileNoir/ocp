package chapter05.chapter05_V2;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;

import static java.lang.System.out;

public class Exc07 {
    public static void main(String[] args) {

        final LocalDate hatDay = LocalDate.of(2017, Month.JANUARY, 15);
        final DateTimeFormatter f = DateTimeFormatter.ISO_DATE;
        out.println(hatDay.format(f));
        out.println(f.format(hatDay));

    }
}
