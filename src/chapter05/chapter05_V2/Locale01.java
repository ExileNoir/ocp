package chapter05.chapter05_V2;

import java.util.Locale;

import static java.lang.System.*;

public class Locale01 {
    public static void main(String[] args) {

        Locale locale = Locale.getDefault();
        out.println(locale);

        Locale locale1 = new Locale.Builder()
                .setRegion("BE")
                .setLanguage("nl")
                .build();
        out.println(locale1);
    }
}
