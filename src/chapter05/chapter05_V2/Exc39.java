package chapter05.chapter05_V2;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static java.lang.System.*;

public class Exc39 {
    public static void main(String[] args) {

        final LocalDateTime pi = LocalDateTime.of(2017,3,14,1,59);
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M.ddhhmm");
        out.println(formatter.format(pi));
    }
}
