package chapter05.chapter05_V2;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static java.lang.System.*;

public class Exc09 {
    public static void main(String[] args) {

        final LocalDate date = LocalDate.of(2017, 3, 12);
        final LocalTime time = LocalTime.of(3, 0);
        final ZoneId zone = ZoneId.of("America/New_York");
        final ZonedDateTime z = ZonedDateTime.of(date, time, zone);
        out.println(z);

    }
}
