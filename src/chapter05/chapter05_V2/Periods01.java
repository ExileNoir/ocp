package chapter05.chapter05_V2;

import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;

import static java.lang.System.out;

public class Periods01 {
    public static void main(String[] args) {

        LocalDate start = LocalDate.of(2019, 8, 25);
        LocalDate end = LocalDate.of(2019, 10, 26);

//        performAnimalEnrichment(start, end);

        Period period = Period.ofMonths(1);
        performAnimalEnrichment(start, end, period);


        Period bad = Period.ofYears(1).ofWeeks(1);
        out.println(bad);
        // equivalent of
        Period bad1 = Period.ofYears(1);
        bad1 = Period.ofWeeks(1);
        out.println(bad1);

        out.println(Period.ofWeeks(50));        // weeds converted in D(ays)
    }

    private static void performAnimalEnrichment(LocalDate start, LocalDate end) {
        LocalDate upTo = start;
        while (upTo.isBefore(end)) {
            out.println("Give new Toy: " + upTo);
            upTo = upTo.plusMonths(1);
        }
    }

    private static void performAnimalEnrichment(LocalDate start, LocalDate end, Period period) {
        LocalDate upTo = start;
        while (upTo.isBefore(end)) {
            out.println("Give new Toy: " + upTo);
            upTo = upTo.plus(period);
        }
    }
}
