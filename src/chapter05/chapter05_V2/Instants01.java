package chapter05.chapter05_V2;

import java.time.*;

public class Instants01 {
    public static void main(String[] args) {

        LocalDate date = LocalDate.of(2019,8,25);
        LocalTime time = LocalTime.of(12,49);
        ZoneId zone = ZoneId.of("Europe/Paris");
        ZonedDateTime zonedDateTime = ZonedDateTime.of(date,time,zone);
        System.out.println(zonedDateTime);

        Instant instant = zonedDateTime.toInstant();        // Instant gets rid of TimeZone and converts  to time GMT
        System.out.println(instant);                       // Paris is GMT+2 so we -2FromParis to get GMT


    }
}
