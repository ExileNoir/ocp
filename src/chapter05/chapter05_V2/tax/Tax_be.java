package chapter05.chapter05_V2.tax;

import java.util.ListResourceBundle;
import java.util.Locale;
import java.util.ResourceBundle;

public class Tax_be extends ListResourceBundle {

    @Override
    protected Object[][] getContents() {
        return new Object[][]{
                {"tax", new StringBuilder("Hallo Lieve Constance vanuit Java")}
        };
    }

    public static void main(String[] args) {
        ResourceBundle rb = ResourceBundle.getBundle("chapter05.chapter05_V2.tax.Tax",new Locale("be"));
        System.out.println(rb.getObject("tax"));
    }
}