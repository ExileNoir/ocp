package chapter05.chapter05_V2;

import java.time.*;

import static java.lang.System.out;

public class Exc11 {
    public static void main(String[] args) {

        final LocalDate montyPythonDay = LocalDate.of(2017, Month.MAY, 10);
        final LocalDate aprilFools = LocalDate.of(2018, Month.APRIL, 1);
        final Duration duration = Duration.ofDays(1);       // UnsupportedTemporalTypeException

        final LocalDate result = montyPythonDay.minus(duration);
        out.println(result + " " + aprilFools.isBefore(result));
    }
}
