package chapter05.chapter05_V2;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.stream.Stream;

public class UsingResourceBundle04 {
    public static void main(String[] args) {

        System.out.println(Locale.getDefault());

        Locale fr = new Locale("fr");
        ResourceBundle rb = ResourceBundle.getBundle("Dolphins",fr);
        System.out.println(rb.getString("name"));
        System.out.println(rb.getString("age"));

        List<String> l = new ArrayList(8);
        
    }

}
