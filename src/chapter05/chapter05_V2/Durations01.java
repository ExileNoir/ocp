package chapter05.chapter05_V2;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static java.lang.System.out;

public class Durations01 {
    public static void main(String[] args) {

        out.println(Duration.ofDays(2));
        out.println(Duration.ofMinutes(55));

        LocalDate date = LocalDate.of(2019, 8, 25);
        LocalDate now = LocalDate.now();
        out.println(ChronoUnit.MONTHS.between(date, now));
        out.println(ChronoUnit.DAYS.between(date, now));

        LocalDateTime c = LocalDateTime.of(2019, 8, 25, 12, 49);
        LocalDateTime nowDT = LocalDateTime.now();
        out.println(ChronoUnit.HOURS.between(c, nowDT));
    }
}
