package chapter05.chapter05_V2;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class FormattingNums {
    public static void main(String[] args) {

        int attendeesPetYear = 3_200_000;
        int attendeesPerMonth = attendeesPetYear / 12;

        NumberFormat us = NumberFormat.getInstance(Locale.US);
        System.out.println(us.format(attendeesPerMonth));

        NumberFormat de = NumberFormat.getInstance(Locale.GERMANY);
        System.out.println(de.format(attendeesPerMonth));

        NumberFormat ca = NumberFormat.getInstance(Locale.CANADA_FRENCH);
        System.out.println(ca.format(attendeesPerMonth));

        System.out.println(NumberFormat.getInstance(Locale.FRANCE).format(attendeesPerMonth));

        double price = 48;
        System.out.println(NumberFormat.getCurrencyInstance(Locale.FRANCE).format(price));

        System.out.println(NumberFormat.getCurrencyInstance(Locale.US).format(price));


        String s = "40,45";
        try {
            System.out.println(us.parse(s));
            System.out.println(us.parse("-2.5165x10"));
            System.out.println(de.parse(s));
//            System.out.println(de.parse("x85.3"));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        String amt = "92807,99 €";
        try {
            NumberFormat cf = NumberFormat.getCurrencyInstance(Locale.FRANCE);
            double value = (Double) cf.parse(amt);
            System.out.println(value);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
