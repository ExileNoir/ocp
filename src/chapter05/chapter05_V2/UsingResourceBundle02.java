package chapter05.chapter05_V2;

import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

public class UsingResourceBundle02 {
    public static void main(String[] args) {

        Locale us = new Locale("en");
        ResourceBundle rb = ResourceBundle.getBundle("Zoo", us);

        final Set<String> keys = rb.keySet();
        keys.stream()
                .map(key -> key + " " + rb.getString(key))
                .forEach(System.out::println);

        Properties properties = new Properties();
        rb.keySet().stream()
                .forEach(k -> System.out.println(rb.getString(k)));


    }
}
