package chapter05.chapter05_V2;

import java.time.*;

import static java.lang.System.out;

public class Exc29 {
    public static void main(String[] args) {

        final LocalDate date = LocalDate.of(2017, Month.JULY, 17);
        LocalTime time = LocalTime.of(10, 0);
        final ZoneId zone = ZoneId.of("America/New_York");
        final ZonedDateTime iceCreamDay = ZonedDateTime.of(date, time, zone);

        time = time.plusHours(5);
        out.println(iceCreamDay);
    }
}
