package chapter05.chapter05_V2.zoo;

import java.util.ListResourceBundle;

public class Zoo_nl extends ListResourceBundle {

    @Override
    protected Object[][] getContents() {
        return new Object[][]{
                {"hello", "Hallo"},
                {"open", "De Zoo is open"}
        };
    }
}
