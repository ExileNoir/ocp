package chapter05.chapter05_V2;

import java.time.Period;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import static java.lang.System.*;

public class UsingResourceBundle01 {
    public static void main(String[] args) {

        Locale us = new Locale("en","US");
        Locale france = new Locale("fr","FR");

        printProperties(us);
        printProperties(france);

    }

    private static void printProperties(final Locale locale){
        final ResourceBundle rb = ResourceBundle.getBundle("Zoo",locale);
        out.println(rb.getString("hello"));
        out.println(rb.getString("open"));
    }
}
