package chapter05.chapter05_V2;

import java.time.*;

import static java.lang.System.*;

public class DateTime01 {
    public static void main(String[] args) {

        out.println(LocalDate.now());
        out.println(LocalTime.now());
        out.println(LocalDateTime.now());
        out.println(ZonedDateTime.now());

        LocalDate d1 = LocalDate.of(2019,Month.AUGUST,25);
        LocalDate d2 = LocalDate.of(2019,8,25);
        out.println(d1);

        LocalTime t1 = LocalTime.of(12,49);
        out.println(t1);

        ZoneId zone = ZoneId.of("Europe/Paris");
        ZonedDateTime zoned1 = ZonedDateTime.of(2019,8,25,12,49,00,000,zone);
        ZonedDateTime zoned2 = ZonedDateTime.of(d1,t1,zone);
        out.println(zoned1);
        out.println(zoned1);

        out.println(ZoneId.systemDefault());


    }
}
