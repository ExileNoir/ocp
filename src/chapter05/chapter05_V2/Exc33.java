package chapter05.chapter05_V2;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import static java.lang.System.out;

public class Exc33 {
    public static void main(String[] args) {

        final LocalDate xmas = LocalDate.of(2017, 12, 25);
        final LocalDate blackFriday = LocalDate.of(2017, 11, 24);

        final long shoppingDaysLeft = blackFriday.until(xmas, ChronoUnit.DAYS);
        out.println(shoppingDaysLeft);

        final long shoppingDaysLeft1 = ChronoUnit.DAYS.between(blackFriday, xmas);
        out.println(shoppingDaysLeft1);
    }
}
