package chapter05.chapter05_V2;

import java.time.Duration;
import java.time.Period;

import static java.lang.System.out;

public class Exc17 {
    public static void main(String[] args) {

        final Duration duration = Duration.ofDays(1);
        final Period period = Period.ofDays(1);
        out.println(duration.toString());
        out.println(period.toString());
    }
}
