package chapter05.howInheritanceAffectSerialization;

import java.io.*;

import static java.lang.System.out;

public class SuperNotSerial {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Dog dog = new Dog(35, "Fido");
        out.println("before: " + dog.name + " " + dog.weight);

        try (final FileOutputStream fos = new FileOutputStream("testSer02");
             final ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(dog);
        }

        try (final FileInputStream fis = new FileInputStream("testSer02");
             final ObjectInputStream ois = new ObjectInputStream(fis)) {
            dog = (Dog) ois.readObject();
        }
        out.println("After: " + dog.name + " " + dog.weight);
    }
}

class Animal  {
    int weight = 42;
}

class Dog extends Animal implements Serializable {
    String name;

    Dog(int w, String n) {
        this.weight = w;
        this.name = n;
    }

//    /* These method will be called automatically during serialization/deserialization process */
//
//    // manually added the state of the Collar to the Dog's serialized representation, even though Collar itself != saved
//    private void writeObject(final ObjectOutputStream os) throws IOException, ClassNotFoundException {
//        os.defaultWriteObject();
//        os.writeInt(weight);
//    }
//
//    // read the extra "int" I saved to the Dog Stream, and use it to create a new Coller Object and then assign to the Dog that's being de-serialized
//    private void readObject(final ObjectInputStream is) throws IOException, ClassNotFoundException {
//        is.defaultReadObject();
//        weight = is.readInt();
//    }
}
