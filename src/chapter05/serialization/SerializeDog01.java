package chapter05.serialization;

import java.io.*;

import static java.lang.System.out;

class Dog01 implements Serializable {

    private transient Collar01 theCollar;       // add transient. We can't Serialize this (class != serialized)
    private int dogSize;

    public Dog01(Collar01 collar, int size) {
        this.theCollar = collar;
        this.dogSize = size;
    }

    public Collar01 getCollar() {
        return theCollar;
    }

    public int getDogSize() {
        return dogSize;
    }

    /* These method will be called automatically during serialization/deserialization process */

    // manually added the state of the Collar to the Dog's serialized representation, even though Collar itself != saved
    private void writeObject(final ObjectOutputStream os) throws IOException, ClassNotFoundException {
        os.defaultWriteObject();
        os.writeInt(theCollar.getCollarSize());
    }

    // read the extra "int" I saved to the Dog Stream, and use it to create a new Collar Object and then assign to the Dog that's being de-serialized
    private void readObject(final ObjectInputStream is) throws IOException, ClassNotFoundException {
        is.defaultReadObject();
        this.theCollar = new Collar01(is.readInt());
    }
}

class Collar01 {       // no longer Serializable
    private int collarSize;

    public Collar01(int size) {
        this.collarSize = size;
    }

    public int getCollarSize() {
        return collarSize;
    }
}

public class SerializeDog01 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        final Collar01 collar = new Collar01(20);
        Dog01 dog = new Dog01(collar, 8);
        out.println("before: collar size is " + dog.getCollar().getCollarSize());
        out.println("before: dogSize is: " + dog.getDogSize());


        // write the object
        try (final FileOutputStream fos = new FileOutputStream("testSer01.ser");
             final ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(dog);

            // << oos.close() >> not needed due to close with resources
        }

        // read the object
        try (final FileInputStream fis = new FileInputStream("testSer01.ser");
             final ObjectInputStream ois = new ObjectInputStream(fis)) {
            dog = (Dog01) ois.readObject();

            // << ois.close() >> not needed due to close with resources
        }
        out.println("after: collar size is " + dog.getCollar().getCollarSize());
        out.println("after: dogSize is: " + dog.getDogSize());

    }
}
