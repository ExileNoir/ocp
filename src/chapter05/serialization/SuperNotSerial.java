package chapter05.serialization;

import java.io.*;

import static java.lang.System.out;

public class SuperNotSerial {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Dog03 d = new Dog03(35, "fido", 200);
        out.println("before: " + d.name + " " + d.weight + " " + d.lenght);

        try (final FileOutputStream fos = new FileOutputStream("testSer03");
             final ObjectOutputStream oos = new ObjectOutputStream(fos);
             final FileInputStream fis = new FileInputStream("testSer03");
             final ObjectInputStream ois = new ObjectInputStream(fis)) {
            oos.writeObject(d);

            d = (Dog03) ois.readObject();
        }
        out.println("After: " + d.name + " " + d.weight + " " + d.lenght);
    }
}

class Dog03 extends Animal03 implements Serializable {
    String name;
    transient int lenght = 500;        // transient here will always go back to zero default

    Dog03(int w, String n, int l) {
        weight = w;
        name = n;
        lenght = l;
    }


//    private void writeObject(final ObjectOutputStream os) throws IOException, ClassNotFoundException {
//        os.defaultWriteObject();
//        os.writeInt(weight);
//    }
//
//    // read the extra "int" I saved to the Dog Stream, and use it to create a new Coller Object and then assign to the Dog that's being de-serialized
//    private void readObject(final ObjectInputStream is) throws IOException, ClassNotFoundException {
//        is.defaultReadObject();
//        this.weight = is.readInt();
//    }
}

class Animal03 {        // no implementation of serialization
    /*transient*/ int weight = 42;  // always restored to default value, unless write/readObject in serializable class (same output with/without transient)
}
