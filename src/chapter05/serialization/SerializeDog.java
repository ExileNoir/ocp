package chapter05.serialization;

import java.io.*;

import static java.lang.System.out;

class Dog implements Serializable {
    private Collar theCollar;
    private int dogSize;

    public Dog(Collar collar, int size) {
        this.theCollar = collar;
        this.dogSize = size;
    }

    public Collar getCollar() {
        return theCollar;
    }

    public int getDogSize() {
        return dogSize;
    }
}

class Collar implements Serializable {
    private int collarSize;

    public Collar(int size) {
        this.collarSize = size;
    }

    public int getCollarSize() {
        return collarSize;
    }
}

public class SerializeDog {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        final Collar collar = new Collar(5);
        Dog dog = new Dog(collar, 8);
        out.println("before: collar size is " + dog.getCollar().getCollarSize());
        out.println("before: dogSize is: " + dog.getDogSize());

        // write the object
        try (final FileOutputStream fos = new FileOutputStream("testSer.ser");
             final ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(dog);

            // << oos.close() >> not needed due to close with resources
        }

        // read the object
        try (final FileInputStream fis = new FileInputStream("testSer.ser");
             final ObjectInputStream ois = new ObjectInputStream(fis)) {
            dog = (Dog) ois.readObject();

            // << ois.close() >> not needed due to close with resources
        }
        out.println("after: collar size is " + dog.getCollar().getCollarSize());
        out.println("after: dogSize is: " + dog.getDogSize());
    }
}
