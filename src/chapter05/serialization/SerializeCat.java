package chapter05.serialization;

import java.io.*;

class Cat implements Serializable {     // marker interface
}

public class SerializeCat {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        final Cat cat = new Cat();

        try (final FileOutputStream fos = new FileOutputStream("testSer.ser");         // to write the object to
             final ObjectOutputStream oos = new ObjectOutputStream(fos);                    // has serialization method
             final FileInputStream fis = new FileInputStream("testSer.ser");        // to read the object
             final ObjectInputStream ois = new ObjectInputStream(fis)) {                 // has de-serialize method

            oos.writeObject(cat);

            // << oos.close() >> not needed due to close with resources

            final Cat cat01 = (Cat) ois.readObject();

            // << ois.close() >> not needed due to close with resources
        }
    }
}
