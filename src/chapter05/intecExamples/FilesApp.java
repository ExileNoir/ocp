package chapter05.intecExamples;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.DosFileAttributes;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

public class FilesApp {
    public static void main(String[] args) throws IOException {

        final Path path = Paths.get("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OcpTxtCreateIO\\IntecExamples");
        final Path file = path.resolve("FileZero.txt");
        // create parentDirs

        Files.createDirectories(file.getParent());
        if (Files.notExists(file)) {
            Files.createFile(file);
            out.println("File Created");
        } else {
            out.println("File already exists");
        }

        // write lines of text to file
        final List<String> lines = new ArrayList<>();
        lines.add("Hello");
        lines.add("My Beautiful");
        lines.add("CONSTANCE <3");
        Files.write(file, lines, StandardCharsets.UTF_8, StandardOpenOption.APPEND);

        // setAttr
        Files.setAttribute(file, "dos:readonly", true);
        Files.setAttribute(file, "dos:hidden", false);

        // Retrieve attr of file
        final DosFileAttributes attrs = Files.readAttributes(file, DosFileAttributes.class);
        out.println("size: " + attrs.size());
        out.println("creationTime: " + attrs.creationTime());
        out.println("lastAccessTime: " + attrs.lastAccessTime());
        out.println("lastModifiedTime: " + attrs.lastModifiedTime());
        out.println("isArchive: " + attrs.isArchive());
        out.println("isHidden: " + attrs.isHidden());
        out.println("isReadOnly: " + attrs.isReadOnly());

        // Read lines of text from file
        Files.lines(file).forEach(out::println);

        // Copy file
        final Path path2 = Paths.get("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OcpTxtCreateIO\\IntecExamples");
        Files.copy(file, path2, StandardCopyOption.REPLACE_EXISTING);

        // Delete Files
//        Files.deleteIfExists(path);
    }
}
