package chapter05.intecExamples;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import static java.lang.System.out;

public class ClassFileSystemExample {
    public static void main(String[] args) {

        try (final FileSystem fs = FileSystems.getDefault()) {
            out.println(fs.getSeparator());

            for (final Path path : fs.getRootDirectories()) {
                out.println(path);
            }

            out.println();
            int spaces = 1;
            for (final Path path1 : fs.getPath("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OcpTxtCreateIO")) {
                out.format("%" + spaces + "s%s%n", "", path1);
                spaces += 2;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
