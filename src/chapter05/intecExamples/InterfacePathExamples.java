package chapter05.intecExamples;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

public class InterfacePathExamples {
    public static void main(String[] args) throws IOException {

        final Path p1 = Paths.get("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OcpTxtCreateIO");
        final Path p2 = p1.resolve("folder1");
        final Path p3 = p2.resolve("file1.txt");
        out.println(p3);

        out.println();

        final Path p4 = Paths.get("file2.txt");
        out.println(p4.toAbsolutePath());
//      out.println(p4.toRealPath());   // gives real path, if file not exists == NoSuchFileException

        out.println();

        final Path p5 = Paths.get("C:/data/subfolder1/file1.txt");
        final Path p6 = Paths.get("C:/data/subfolder2/file3.txt");
        final Path p7 = p5.relativize(p6);
        out.println(p7);
    }
}
