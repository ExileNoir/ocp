package chapter05.directoryStream;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

import static java.lang.System.out;

public class MyPathMatcher extends SimpleFileVisitor<Path> {

    private final PathMatcher matcher = FileSystems
            .getDefault()
            .getPathMatcher(
                    "glob:**/OcpTxtCreateIO/**.txt");     // << ** >> means any subdirectory

    @Override
    public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
        if (matcher.matches(file)) {
            out.println(file);
        }
        return FileVisitResult.CONTINUE;
    }

    public static void main(String[] args) throws IOException {
        final MyPathMatcher dirs = new MyPathMatcher();
        Files.walkFileTree(
                Paths.get("C:", "Users", "Steven", "Desktop", "PROJECTS"),
                dirs);
    }
}
