package chapter05.directoryStream;

import java.io.IOException;
import java.nio.file.*;

import static java.lang.System.out;
import static java.nio.file.StandardWatchEventKinds.*;

public class UsingWatchService {
    public static void main(String[] args) throws IOException {

        final String pathDir = "C:/Users/Steven/Desktop/PROJECTS/Java/OCP/OcpTxtCreateIO/dir";
        final Path dir = Paths.get(pathDir);    // get dir containing file/directory we care about

        final WatchService watcher = FileSystems
                .getDefault()
                .newWatchService();                  // file system-specific code && create empty WatchService
        dir.register(watcher, ENTRY_DELETE,ENTRY_CREATE,ENTRY_MODIFY);        // needs a static import! start watching for deletions

        while (true) {
            WatchKey key;
            try {
                key = watcher.take();
            } catch (InterruptedException e) {
                out.println("shit...");
                return;
            }
            // IMPORTANT when run delete the << dir: directoryToDelete >> then app continues

            for (final WatchEvent<?> event : key.pollEvents()) {
                final WatchEvent.Kind<?> kind = event.kind();
                out.println("kind name: " + kind.name());               // create/delete/modify
                out.println("kind type: " + kind.type());              // always a Path for us
                out.println("event context: " + event.context());     // name of the file
                final String name = event.context().toString();

                if (name.equals("directoryToDelete")) {       // only delete right directory
                    out.format("Directory deleted, now we can proceed");
                    return;         // end program, we found what we are looking for
                }
            }
            key.reset();            // keep looking for events
        }
    }
}
