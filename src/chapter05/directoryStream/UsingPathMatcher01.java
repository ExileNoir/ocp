package chapter05.directoryStream;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;

import static java.lang.System.out;

public class UsingPathMatcher01 {

    private static void matches(final Path path, final String glob) {
        final PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:" + glob);
        out.println(matcher.matches(path));
    }

    public static void main(String[] args) {
        final Path path = Paths.get("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OcpTxtCreateIO\\One.java");

        matches(path, "*.java");
        matches(path, "**/*.java");
        matches(path, "*");
        matches(path, "**");

        out.println();

        final Path path01 = Paths.get("One.java");

        matches(path01, "*.java");
        matches(path01, "**/*.java");
        matches(path01, "*");
        matches(path01, "**");

        out.println();

        final Path path03 = Paths.get("Bert-book");
        final Path path04 = Paths.get("Kathy-horse");

        matches(path03, "{Bert*,Kathy*}");
        matches(path04, "{Bert,Kathy}*");
        matches(path03, "{Bert,Kathy}");

        out.println();
        final Path path05 = Paths.get("0b/test/1");
        final Path path06 = Paths.get("9\\b/test/1");
        final Path path07 = Paths.get("01b/test/1");
        final Path path08 = Paths.get("0b/1");
        final String glob = "[0-9]\\*{A*,b}/**/1";

        matches(path05, glob);
        matches(path06, glob);
        matches(path07, glob);
        matches(path08, glob);

        out.println();
        final Path path09 = Paths.get("One.java");
        final Path path10 = Paths.get("One.ja^a");

        matches(path09, "*.????");
        matches(path09, "*.???");
        matches(path10, "*.????");
        matches(path10, "*.???");
    }
}
