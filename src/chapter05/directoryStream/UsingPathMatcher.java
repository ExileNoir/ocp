package chapter05.directoryStream;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;

import static java.lang.System.out;

public class UsingPathMatcher {
    public static void main(String[] args) {

        final Path path01 = Paths.get("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OcpTxtCreateIO\\One.txt");
        final Path path02 = Paths.get("One.txt");

        final PathMatcher matcher = FileSystems
                .getDefault()                                        // get the PathMatcher
                .getPathMatcher(                                    // for the right file system
                        "glob:**/*.txt");              // use glob

        out.println(matcher.matches(path01));
        out.println(matcher.matches(path02));

    }
}
