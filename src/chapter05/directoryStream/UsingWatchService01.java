package chapter05.directoryStream;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

public class UsingWatchService01 {
    public static void main(String[] args) throws IOException {

        final Path path = Paths.get("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OcpTxtCreateIO\\dir\\parent");
        final WatchService watcher = FileSystems
                .getDefault()
                .newWatchService();

        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) throws IOException {
                dir.register(watcher, ENTRY_MODIFY);        // watch each dir
                return FileVisitResult.CONTINUE;
            }
        });
    }

}
