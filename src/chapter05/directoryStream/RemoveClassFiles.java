package chapter05.directoryStream;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class RemoveClassFiles extends SimpleFileVisitor<Path> {

    @Override       // == called automatically
    public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
        if (file.getFileName().toString().endsWith(".class")) {
            Files.delete(file);                    // delete te file
        }
        return FileVisitResult.CONTINUE;        // go on to next file
    }


    public static void main(String[] args) throws IOException {

        final Path dir = Paths.get("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OcpTxtCreateIO\\src");

        final RemoveClassFiles dirs = new RemoveClassFiles();
        Files.walkFileTree(
                dir,        // starting point
                dirs);      // the visitor
    }
}
