package chapter05.directoryStream;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

public class UsingDirectoryStream02 {
    public static void main(String[] args) throws IOException {

        final String directoryPath = "C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OcpTxtCreateIO";
        final Path dir = Paths.get(directoryPath);

        try(final DirectoryStream<Path> stream = Files.newDirectoryStream(dir, "[asc]*")){
            for (final Path path : stream) {
                out.println(path.getFileName());
            }
        }
    }
}
