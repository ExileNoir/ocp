package chapter05.filesPathPaths;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CreateDirectoryAndFile01 {
    public static void main(String[] args) throws IOException {

        final Path pathDir = Paths.get("C:", "Users", "Steven", "Desktop", "PROJECTS", "Java", "OCP", "OcpTxtCreateIO");
        final Path pathFile = Paths.get("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OcpTxtCreateIO\\ProgramCreateDF.txt");

        Files.createDirectory(pathDir);
        Files.createFile(pathFile);
    }
}
/*
 * If pathDir || pathFile exists -> FileAlreadyExistsException
 * */