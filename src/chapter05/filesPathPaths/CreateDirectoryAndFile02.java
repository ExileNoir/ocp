package chapter05.filesPathPaths;

import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

public class CreateDirectoryAndFile02 {
    public static void main(String[] args) {

        final Path path = Paths.get("C:", "Users", "Steven", "Desktop", "PROJECTS", "Java", "OCP", "OcpTxtCreateIO");

        out.println("getFileName: " + path.getFileName());
        out.println("getName(1): " + path.getName(1));
        out.println("getNameCount: " + path.getNameCount());
        out.println("getRoot: " + path.getRoot());
        out.println("subPath(0,2): " + path.subpath(0, 2));
        out.println("getParent: "+path.getParent());
        out.println("toString: " + path.toString());

        int spaces = 1;


        for (final Path p : path) {
            out.format("%" + spaces + "s%s%n", "", p);
            spaces += 2;
        }
    }
}
