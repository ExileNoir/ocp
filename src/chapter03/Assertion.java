package chapter03;

public class Assertion {
    public static void main(String[] args) {
        Assertion assertion = new Assertion();
        assertion.setup();
    }

    void setup() {
        Object conn = getConnection();
        assert conn != null : "Connection is null STUPIDO";


        // continue with other setup ...
    }

    // Simulate failure to get a connection; using Object
    // to avoid dependencies on JDBC or some other heavy
    // 3rd party library
    Object getConnection() {
        return null;
    }
}
