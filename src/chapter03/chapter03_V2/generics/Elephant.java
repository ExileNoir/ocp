package chapter03.chapter03_V2.generics;

import static java.lang.System.*;

public class Elephant {
    private String name;

    public Elephant(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString(){
        return getName();
    }


    public static void main(String[] args) {
        final Elephant dumbo = new Elephant("Dumbo");

        final Crate<Elephant> crateForElephant = new Crate<>();
        crateForElephant.packCrate(dumbo);

        final Elephant inNewHome = crateForElephant.emptyCrate();
        out.println(inNewHome);
    }
}
