package chapter03.chapter03_V2.generics;

import static java.lang.System.out;

public class ShippableGenCrate<U> implements Shippable<U> {
    @Override
    public void ship(U u) {
        System.out.println("Shipped via Generic shipping " + u.toString());
    }

    public static void main(String[] args) {
        final Elephant dumbo = new Elephant("Dumbo");

        final Crate<Elephant> oldHouse = new Crate<>();
        oldHouse.packCrate(dumbo);

        final ShippableGenCrate<Elephant> ship = new ShippableGenCrate<>();
        ship.ship(dumbo);

        final Elephant newHouse = oldHouse.emptyCrate();
        out.println("in his new House: " + newHouse);
    }
}
