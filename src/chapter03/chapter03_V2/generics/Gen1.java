package chapter03.chapter03_V2.generics;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

public class Gen1 {
    static void printNames(final List<?> list) {
        for (int i = 0; i < list.size(); i++) {
            String name = list.get(i).toString();
            out.println(name);
        }
    }

    public static void main(String[] args) {
        final List names = new ArrayList();
        names.add(new StringBuilder("Constance"));
        names.add("Hellow my love");
        names.add(69);
        printNames(names);

        final ArrayList<String> list = new ArrayList();
        list.add("Constance");
        printNames(list);
    }
}
