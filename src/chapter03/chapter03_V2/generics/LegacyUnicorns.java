package chapter03.chapter03_V2.generics;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

class Dragon {
}

class Unicorn {
}

public class LegacyUnicorns {
    public static void main(String[] args) {
        List<Unicorn> unicorns = new ArrayList<>();
        addUnicorn(unicorns);
        Unicorn unicorn = unicorns.get(0);      // ClassCast

        List unicorns2 = new ArrayList();
        unicorns2.add(new Unicorn());
        printDragons(unicorns2);

    }

    private static void addUnicorn(List unicorns) {
        unicorns.add(new Dragon());
    }

    private static void printDragons(List<Dragon> dragons) {
        dragons.forEach(out::println);      // ClassCast
//        for (Object dragon : dragons) {
//            out.println(dragon);
//        }
    }
}
