package chapter03.chapter03_V2.generics;

public class SizeLimitedCrate<T,U> {
    private T contents;
    private U sizeLimit;

    public SizeLimitedCrate(T contents, U sizeLimit) {
        this.contents = contents;
        this.sizeLimit = sizeLimit;
    }

    public static void main(String[] args) {
        final Elephant dumbo = new Elephant("Dumbo");
        final Integer numPounds = 15_000;
        final SizeLimitedCrate<Elephant,Integer> crate = new SizeLimitedCrate<>(dumbo,numPounds);
    }
}
