package chapter03.chapter03_V2.generics;

import static java.lang.System.out;

public class ShippableElephantCrate implements Shippable<Elephant> {

    @Override
    public void ship(final Elephant elephant) {
        out.println("Being Shipped: " + elephant.toString());
    }


    public static void main(String[] args) {
        final Elephant dumbo = new Elephant("Dumbo");

        final Crate<Elephant> oldHouse = new Crate<>();
        oldHouse.packCrate(dumbo);

        final ShippableElephantCrate ship = new ShippableElephantCrate();
        ship.ship(dumbo);

        final Elephant newHouse = oldHouse.emptyCrate();
        out.println("in his new House: " + newHouse);
    }
}
