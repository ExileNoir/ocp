package chapter03.chapter03_V2.generics;

import java.util.ArrayList;
import java.util.List;

public class LegacyAutoBoxing {
    public static void main(String[] args) {
        List numbers = new ArrayList();
        numbers.add(69);
//        int result = numbers.get(0);      // without Gen, it contains Object does not know about Integer
//        Integer result = numbers.get(0);
        Object result = numbers.get(0);
        System.out.println(result);
    }
}
