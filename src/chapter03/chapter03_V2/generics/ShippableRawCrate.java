package chapter03.chapter03_V2.generics;

public class ShippableRawCrate implements Shippable {

    @Override
    public void ship(Object o) {
        System.out.println("Shipped via Raw way: " + o.toString());
    }
}
