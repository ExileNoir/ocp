package chapter03.chapter03_V2.generics;

public interface Shippable<T> {
    public abstract void ship(T t);
}
