package chapter03.chapter03_V2.generics;

public class Crate<T> {
    private T contents;

    public void packCrate(T contents) {
        this.contents = contents;
    }

    public T emptyCrate() {
        return contents;
    }
}
