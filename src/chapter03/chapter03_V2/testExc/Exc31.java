package chapter03.chapter03_V2.testExc;

import java.util.ArrayDeque;
import java.util.function.Consumer;

public class Exc31 {
    public static void main(String[] args) {

        ArrayDeque<Integer> dice =new ArrayDeque<>();
        dice.offer(3);
        dice.offer(2);
        dice.offer(4);

        System.out.println(dice.stream().filter(n->n != 4));

    }
}
