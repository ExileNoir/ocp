package chapter03.chapter03_V2.testExc;

class Wash<T>{
    T item;
    public void clean(T item){
        System.out.println("Clean: "+item);
    }
}
public class Exc22 {
    public static void main(String[] args) {
        Wash wash = new Wash();
        wash.clean("Socks");

        Wash wash1 = new Wash<String>();
        wash1.clean("underwear");

        Wash<String> wash3 = new Wash<>();
        wash3.clean("fuck");
    }
}
