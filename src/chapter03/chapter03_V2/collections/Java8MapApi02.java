package chapter03.chapter03_V2.collections;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

public class Java8MapApi02 {
    public static void main(String[] args) {

        Map<String, Integer> counts = new HashMap<>();
        counts.put("Jenny", 1);

        BiFunction<String, Integer, Integer> mapper = (k, v) -> v + 1;

        Integer jenny = counts.computeIfPresent("Jenny", mapper);
        Integer sam = counts.computeIfPresent("Sam", mapper);
        System.out.println(counts);
        System.out.println(jenny);
        System.out.println(sam);

        Function<String, Integer> f = (k) -> 1;
        counts.put("Tom",null);
        jenny = counts.computeIfAbsent("Jenny", f);
        sam = counts.computeIfAbsent("Sam", f);
        Integer tom = counts.computeIfAbsent("Tom", f);
        System.out.println(counts);
        System.out.println(jenny);
        System.out.println(sam);
        System.out.println(tom);

    }
}
