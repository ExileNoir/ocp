package chapter03.chapter03_V2.collections;

import java.util.*;

public class Duck implements Comparable<Duck> {
    private String name;
    private int weight;

    public Duck(String name) {
        this.name = name;
    }

    public Duck(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public int compareTo(Duck d) {
        return this.name.compareTo(d.name);
    }

    @Override
    public String toString() {
        return this.name;
    }


    public static void main(String[] args) {
        List<Duck> ducks = new ArrayList<Duck>();
        ducks.add(new Duck("Goofy"));
        ducks.add(new Duck("kwak"));
        Collections.sort(ducks);
        System.out.println(ducks);

        List<Duck> ducksOne = new ArrayList<>();
        ducksOne.add(new Duck("Goofy", 15));
        ducksOne.add(new Duck("Goofy", 1));
        Comparator<Duck> compare = Comparator.comparing(Duck::getName).thenComparing(Duck::getWeight);
        Collections.sort(ducks, compare);
        System.out.println(ducksOne);



    }
}

class CompareDucks implements Comparator<Duck> {
    @Override
    public int compare(Duck o1, Duck o2) {
        int result = o1.getName().compareTo(o2.getName());
        if (result != 0) {
            return result;
        }
        return o1.getWeight() - o2.getWeight();
    }
}
