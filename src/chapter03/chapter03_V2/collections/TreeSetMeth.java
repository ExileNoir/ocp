package chapter03.chapter03_V2.collections;

import java.util.TreeSet;

public class TreeSetMeth {
    public static void main(String[] args) {
        TreeSet<String> tree=new TreeSet<>();
        tree.add("one");
        tree.add("One");
        tree.add("ONE");
        tree.add("On");
        System.out.println(tree.ceiling("On"));
        System.out.println(tree);
    }
}
