package chapter03.chapter03_V2.collections;

import java.util.HashMap;
import java.util.Map;

public class Java8MapApi {
    public static void main(String[] args) {
        Map<String,String> favorites = new HashMap<>();
        favorites.put("Jenny","Bus Tour");
        String s = favorites.put("Jenny","Tram");   // replaces old and gives old back
        System.out.println(favorites);
        System.out.println(s);

        Map<String, String> map = new HashMap<>();
        map.put("Jenny","Bus tour");
        map.put("Tom",null);
        map.putIfAbsent("Jenny","DickheadTour");
        map.putIfAbsent("Sam","Tram");
        map.putIfAbsent("Tom","Tram");
        System.out.println(map);
    }
}
