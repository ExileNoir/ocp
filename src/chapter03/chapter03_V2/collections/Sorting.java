package chapter03.chapter03_V2.collections;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Sorting {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Fluffy","Hoppy");
        Comparator<String> c = Comparator.reverseOrder();
        int result = Collections.binarySearch(names,"Hoppy",c);
        System.out.println(result);
        Collections.sort(names,c);
        System.out.println(names);



        Comparator<Integer> comp = (o1, o2) -> o2-o1;
        List<Integer> list=Arrays.asList(5,4,7,1);
        Collections.sort(list,comp);
        System.out.println(Collections.binarySearch(list,1,comp));      // if not comp in argument then search does default order ascending and not descending
    }
}
