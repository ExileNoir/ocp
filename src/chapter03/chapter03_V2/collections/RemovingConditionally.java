package chapter03.chapter03_V2.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RemovingConditionally {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>(Arrays.asList("Magician", "Assistant"));
        System.out.println(list);
        String s = "Assistant";
        list.removeIf(s::startsWith);
        System.out.println(list);
        list.removeIf(l -> l.startsWith("Ma"));
        System.out.println(list);
    }
}
