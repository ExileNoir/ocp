package chapter03.chapter03_V2.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OCARev {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Fluffy");
        list.add("Webby");
        System.out.println(list);

        String[] array = new String[list.size()];
        System.out.println(array.length + " " + array[0]);
        array[0] = list.get(1);
        array[1] = list.get(0);
        for (String s : array) {
            System.out.println(s);
        }
        System.out.println();
        String[] array1 = {"gerbil", "mouse"};
        List<String>list1 = Arrays.asList(array1);
        list1.set(1,"test");
       array1[0]="new";
        for (String s : array1) {
            System.out.println(s);
        }
        list1.forEach(System.out::println);
    }
}
