package chapter03;

import java.io.IOException;

public class SuppressedV2 {
    public static void main(String[] args) {
        try (Bad b1 = new Bad("Bad 1 "); Bad b2 = new Bad("Bad 2 ")) {
            // do some shit
        } catch (Exception e) {
            System.err.println("Real exception: " + e.getMessage());
            for (Throwable t : e.getSuppressed()) {
                System.err.println("Supressed exception: " + t);
            }
        }
    }
}

class Bad implements AutoCloseable {
    private String name;

    Bad(String n) {
        name = n;
    }

    @Override
    public void close() throws Exception {
        throw new IOException("Closing: " + name);
    }
}