package chapter04.chapter04_V2.functionalInterfaces;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

import static java.lang.System.out;

public class PredicateOne {
    public static void main(String[] args) {

        final Predicate<String> p1 = String::isEmpty;
        final Predicate<String> p2 = x -> x.isEmpty();
        final Predicate<Integer> p3 = i -> i < 10;

        out.println(p1.test(""));
        out.println(p2.test("Constance"));
        out.println(p3.test(5));

        final Predicate<String> eggs = s -> s.contains("Eggs");
        final Predicate<String> brownEggs = eggs.and(s -> s.contains("Brown")).negate();
        out.println(brownEggs.test("BruwnEggs"));


        final BiPredicate<String, String> b1 = String::startsWith;
        final BiPredicate<String, String> b2 = (s, i) -> s.startsWith(i);

        out.println(b1.test("chicken", "chick"));
        out.println(b2.test("hello", "Noir"));

    }
}
