package chapter04.chapter04_V2.functionalInterfaces;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.function.Supplier;

import static java.lang.System.out;

public class SupplierOne {
    public static void main(String[] args) {
        final Supplier<LocalDate> s1 = LocalDate::now;
        final Supplier<LocalDate> s2 = () -> LocalDate.now();

        final LocalDate d1 = s1.get();
        final LocalDate d2 = s2.get();
        out.println(d1);
        out.println(d2);
        out.println(s1);

        final Supplier<String> ss = () -> "Hellow Constance";
        final String constance = ss.get();
        out.println(constance);

        final Supplier<ArrayList<String>> s3 = ArrayList<String>::new;
        final ArrayList<String> a1 = s3.get();
        a1.add("doooh");
        out.println(a1);
        out.println(s3);

        final Supplier tryIt = LocalDate::now;
        final LocalDate dTryIt = (LocalDate) tryIt.get();
        out.println(dTryIt);
    }
}
