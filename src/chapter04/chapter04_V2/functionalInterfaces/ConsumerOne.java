package chapter04.chapter04_V2.functionalInterfaces;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import static java.lang.System.out;

public class ConsumerOne {
    public static void main(String[] args) {

        final Consumer<String> c1 = out::println;
        final Consumer<String> c2 = s -> out.println(s);

        c1.accept("Constance");
        c2.accept("ExileNoir");

        final Map<String, Integer> map = new HashMap<>();
        final BiConsumer<String, Integer> b1 = map::put;
        final BiConsumer<String, Integer> b2 = (k, v) -> map.put(k, v);

        b1.accept("Constance", 7);
        b2.accept("ExileNoir", 36);

        out.println(map);
    }
}
