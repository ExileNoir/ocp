package chapter04.chapter04_V2.functionalInterfaces;

import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

import static java.lang.System.out;

public class UnaryBinaryOperators {
    public static void main(String[] args) {

        final UnaryOperator<String> u1 = String::toUpperCase;
        final UnaryOperator<String> u2 = s -> s.toUpperCase();

        out.println(u1.apply("Constance"));
        out.println(u2.andThen(s -> s.concat(" My Love")).apply("Constance"));

        final BinaryOperator<String> b1 = String::concat;
        final BinaryOperator<String> b2 = (a, b) -> a.concat(b);

        out.println(b1.apply("Hello", "World"));
        out.println(b2.andThen(s -> s.concat("Constance")).apply("My World", " true love "));
    }
}
