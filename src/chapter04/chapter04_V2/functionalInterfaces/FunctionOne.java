package chapter04.chapter04_V2.functionalInterfaces;

import java.util.function.BiFunction;
import java.util.function.Function;

import static java.lang.System.out;

public class FunctionOne {
    public static void main(String[] args) {

        final Function<String, Integer> f1 = String::length;
        final Function<String, Integer> f2 = x -> x.length();

        out.println(f1.apply("Constance"));
        out.println(f2.apply("ExileNoir"));

        final BiFunction<String, String, String> b1 = String::concat;
        final BiFunction<String, String, String> b2 = (a, b) -> a.concat(b);

        out.println(b1.apply("Constance", "DS"));
        out.println(b2.apply("Hello", "ExileNoir"));


    }
}
