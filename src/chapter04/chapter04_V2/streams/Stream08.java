package chapter04.chapter04_V2.streams;

import java.util.stream.Stream;

import static java.lang.System.out;

public class Stream08 {
    public static void main(String[] args) {

        final Stream<String> stream = Stream.of("w", "o", "l", "f");
        final StringBuilder word = stream.collect(
                StringBuilder::new, StringBuilder::append, StringBuilder::append);
        System.out.println(word);


        final StringBuilder wordOne = stream.collect(
                StringBuilder::new, (x, y) -> x.append(y.toUpperCase()), (x, y) -> y.reverse());
        out.println(wordOne);
    }
}
