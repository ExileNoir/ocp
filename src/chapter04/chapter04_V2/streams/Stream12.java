package chapter04.chapter04_V2.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.lang.System.out;

public class Stream12 {
    public static void main(String[] args) {

        final List<String> list = Arrays.asList("lions", "tigers", "bears");

        final Map<Boolean, List<String>> map = list.stream()
                .collect(Collectors.partitioningBy(
                        s -> s.length() <= 5));
        out.println(map);

        final Map<Boolean, Set<String>> map1 = list.stream()
                .collect(Collectors.partitioningBy(
                        s -> s.length() <= 5,
                        Collectors.toSet()));
        out.println(map1);


    }
}
