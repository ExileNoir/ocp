package chapter04.chapter04_V2.streams;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.out;

public class Exc11 {
    public static void main(String[] args) {

        out.println(Stream.iterate(1, x -> ++x)
                .limit(5)
                .map(x -> "" + x)
                .collect(Collectors.joining())
        );
    }
}
