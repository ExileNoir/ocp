package chapter04.chapter04_V2.streams;

import java.util.stream.Stream;

import static java.lang.System.out;

public class Exc01 {
    public static void main(String[] args) {

        final Stream<String> stream = Stream.iterate("", s -> s + "1")
                .limit(2)
                .map(x -> x + "2");     // ""2""12
        stream.forEach(out::print);

        final Stream<String> stream1 = Stream.iterate("", s -> s + "1");
        out.println(stream1.limit(2).map(x -> x + "2"));
    }
}
