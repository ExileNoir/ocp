package chapter04.chapter04_V2.streams;

import java.util.Map;
import java.util.stream.Stream;

import static java.lang.System.out;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

public class Exc33 {
    public static void main(String[] args) {
        final Stream<Ballot> ballots = Stream.of(
                new Exc33().new Ballot("Mario", 1, 10),
                new Exc33().new Ballot("Christina", 1, 8),
                new Exc33().new Ballot("Mario", 2, 9),
                new Exc33().new Ballot("Christina", 2, 8)
        );
        final Map<String, Integer> scores = ballots.collect(
                groupingBy(Ballot::getName, summingInt(Ballot::getScore)));
        out.println(scores.get("Mario"));


    }

    class Ballot {
        private String name;
        private int judgeNumber;
        private int score;

        public Ballot(String name, int judgeNumber, int score) {
            this.name = name;
            this.judgeNumber = judgeNumber;
            this.score = score;
        }

        public String getName() {
            return name;
        }

        public int getJudgeNumber() {
            return judgeNumber;
        }

        public int getScore() {
            return score;
        }
    }
}
