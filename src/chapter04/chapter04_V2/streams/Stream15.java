package chapter04.chapter04_V2.streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import static java.lang.System.*;

public class Stream15 {
    public static void main(String[] args) {
        final List<String> list = Arrays.asList("Tom Cruise", "Tom Hart", "Tom hanks", "Tom Brady");
        final Predicate<String> p = str -> {
            out.println("Looking...");
            return str.indexOf("Tom") > -1;
        };

        final boolean flag = list.stream()
                .filter(str -> str.length() > 8)
                .allMatch(p);

        out.println(flag);
    }
}
