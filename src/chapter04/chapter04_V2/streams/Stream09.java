package chapter04.chapter04_V2.streams;

import java.util.IntSummaryStatistics;
import java.util.stream.IntStream;

import static java.lang.System.out;

public class Stream09 {
    public static void main(String[] args) {

        final IntStream intStream = IntStream.of(1, 2, 3, 5);
        final int range = range(intStream);
        out.println(range);
    }

    private static int range(final IntStream ints) {
        final IntSummaryStatistics stats = ints.summaryStatistics();
        if (stats.getCount() == 0) {
            throw new RuntimeException();
        }
        out.println(stats.getCount());
        return stats.getMax() - stats.getMin();
    }
}
