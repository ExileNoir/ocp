package chapter04.chapter04_V2.streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Stream14 {
    public static void main(String[] args) {
        final List<Integer> primes = Arrays.asList(2, 3, 5, 7, 11, 13, 17);
        final Stream<Integer> primeStream = primes.stream();

        final Predicate<Integer> test1 = k -> k < 10;

        primeStream.collect(Collectors.partitioningBy(
                test1,
                Collectors.counting()))          // counts the number of input elements
                .values()                       // Returns a Collection view of the values contained in this map.
                .forEach(System.out::print);   // The collection is backed by the map, so changes to the map are reflected in the collection, and vice-versa.
    }
}
