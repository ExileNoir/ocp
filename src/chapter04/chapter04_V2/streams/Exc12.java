package chapter04.chapter04_V2.streams;

import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;

import static java.lang.System.out;
import static java.util.stream.Collectors.toMap;

public class Exc12 {
    public static void main(String[] args) {

        final Stream<String> s = Stream.of("speak", "bark", "meow", "growl");
        final BinaryOperator<String> merge = (a, b) -> a;
        final Map<Integer, String> map = s.collect(toMap(String::length, k -> k, merge));
        out.println(map.size() + " " + map.get(4));
        out.println(map);
    }
}
