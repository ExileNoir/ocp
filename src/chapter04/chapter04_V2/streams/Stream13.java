package chapter04.chapter04_V2.streams;

import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;

public class Stream13 {
    public static void main(String[] args) {

        final List<String> strings = Arrays.asList("Hello", "Constance");
        final List<Double> doubles = Arrays.asList(23.0, 25.5);
        final List<StringBuilder> stringBuilders = Arrays.asList(new StringBuilder("Exile"), new StringBuilder("Noir"));

        strings.stream()
                .forEach(s -> s.concat("Hellooooo"));       // variables are passed by value, so copy is made
        out.println(strings);

        doubles.stream()
                .forEach(d -> d += 10);
        out.println(doubles);

        stringBuilders.stream()                                // Stream of mutable objects &&
                .forEach(sb -> sb.append(" TheLord"));        // if you append something to the elements, that would change the original objects contained in the list.
        out.println(stringBuilders);
    }
}
