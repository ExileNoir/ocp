package chapter04.chapter04_V2.streams;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.out;

public class Stream10 {
    public static void main(String[] args) {

        final List<String> list = Arrays.asList("lions", "tigers", "bears");

        final String result = Stream.of(list)
                .flatMap(Collection::stream)
                .collect(Collectors.joining(", "));
        out.println(result);

        final Double result1 = Stream.of(list)
                .flatMap(Collection::stream)
                .collect(Collectors.averagingInt(String::length));
        out.println(result1);


        final Map<String, Integer> map = list.stream()
                .collect(Collectors.toMap(
                        s -> s,                  // 1st function tells collector how to create the key
                        String::length));       // 2nd function tells the collector how to create the value
        out.println(map);

        final Map<Integer, String> map1 = list.stream()
                .collect(Collectors.toMap(
                        String::length,
                        k -> k.toUpperCase(),
                        (s1, s2) -> s2 + " | " + s1));
        out.println(map1);

        final Map<Integer, String> map2 = list.stream()
                .collect(Collectors.toMap(
                        String::length,                    // 1st function tells collector how to create the key
                        String::toUpperCase,              // 2nd function tells the collector how to create the value
                        (s1, s2) -> s2 + " & " + s1,     // 3th binaryOp to merge values
                        TreeMap::new));                 // mapSupplier
        out.println(map2);
    }
}
