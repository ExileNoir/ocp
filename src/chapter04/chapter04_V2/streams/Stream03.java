package chapter04.chapter04_V2.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.System.out;

public class Stream03 {
    public static void main(String[] args) {


        final List<String> list = Arrays.asList("Toby", "Anna", "Leroy", "Alex");

        final List<String> filtered = list.stream()
                .sorted()
                .filter(s -> s.length() == 4)
                .limit(2)
                .collect(Collectors.toCollection(ArrayList::new));
        out.println(filtered);
    }
}
