package chapter04.chapter04_V2.streams;

import java.util.stream.Stream;

public class Stream07 {
    public static void main(String[] args) {

        final Stream<Integer> oddNumbers = Stream.iterate(1, n -> n + 2);
        oddNumbers
                .limit(20)
                .forEach(System.out::println);

        final Stream<String> concat = Stream.iterate("Constance", c -> c + " my Love");
        concat
                .limit(20)
                .forEach(System.out::println);
    }
}
