package chapter04.chapter04_V2.streams;

import java.util.stream.Stream;

import static java.lang.System.out;

public class Stream05 {
    public static void main(String[] args) {

        final Stream<String> stream = Stream.iterate("i", s -> s + "a");
        stream.peek(out::println)
                .limit(2)
                .peek(out::println)
                .map(x -> x + "2")
                .forEach(out::println);
//                        .forEach(d->System.out.print(d));

    }
}
