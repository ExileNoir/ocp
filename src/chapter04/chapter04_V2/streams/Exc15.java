package chapter04.chapter04_V2.streams;

import java.util.stream.Stream;

public class Exc15 {
    public static void main(String[] args) {

        Stream<Character> stream = Stream.of('c','b','a');
        stream.sorted().findAny().ifPresent(System.out::println);
    }
}
