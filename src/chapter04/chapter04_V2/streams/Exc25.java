package chapter04.chapter04_V2.streams;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Exc25 {
    public static void main(String[] args) {

        final IntStream ints = IntStream.empty();
        final IntStream moreInts = IntStream.of(66, 77, 88);
        Stream.of(ints, moreInts)
                .flatMapToInt(x -> x)
                .forEach(System.out::print);
    }
}
