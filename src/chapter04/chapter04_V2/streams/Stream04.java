package chapter04.chapter04_V2.streams;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;
import java.util.OptionalInt;

import static java.lang.System.out;

public class Stream04 {
    public static void main(String[] args) {

        final List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6);

        final int sum = list.stream()
                .mapToInt(i -> i)
                .sum();
        out.println(sum);


        final OptionalDouble avg = list.stream()
                .mapToInt(i -> i)
                .average();
        avg.ifPresent(out::println);

        final OptionalInt max = list.stream()
                .mapToInt(i -> i)
                .max();
        max.ifPresent(out::println);

        final OptionalInt min = list.stream()
                .mapToInt(Integer::intValue)
                .min();
        min.ifPresent(out::println);


        final long count = list.stream()
                .mapToInt(Integer::intValue)
                .count();
        out.println(count);

    }
}
