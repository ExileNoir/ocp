package chapter04.chapter04_V2.streams;

import java.util.stream.Stream;

import static java.lang.System.*;

public class Exc34 {
    public static void main(String[] args) {

        final Stream<Boolean> hide = Stream.of(true, false, true);
        final boolean found = hide.filter(b -> b).anyMatch(Boolean::booleanValue);
        out.println(found);
    }
}
