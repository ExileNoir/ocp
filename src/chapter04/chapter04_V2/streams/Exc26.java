package chapter04.chapter04_V2.streams;

import java.util.LongSummaryStatistics;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class Exc26 {
    public static void main(String[] args) {

        final LongStream stream = LongStream.of(6,10);
        final LongSummaryStatistics stats = stream.summaryStatistics();
        System.out.println(stats.getAverage());
    }
}
