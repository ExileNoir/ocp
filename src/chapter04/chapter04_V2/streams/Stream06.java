package chapter04.chapter04_V2.streams;

import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Stream06 {
    public static void main(String[] args) {

        final Stream<Integer> s = Stream.of(1);
        final IntStream is = s.mapToInt(x -> x);
        final DoubleStream ds = is.mapToDouble(x -> x);
//        IntStream is2 = ds.mapToInt(x->x);
    }
}
