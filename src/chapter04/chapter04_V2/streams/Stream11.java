package chapter04.chapter04_V2.streams;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.System.out;

public class Stream11 {
    public static void main(String[] args) {

        final List<String> list = Arrays.asList("lions", "tigers", "bears");

        final Map<Integer, List<String>> map = list.stream()
                .collect(Collectors.groupingBy(
                        String::length));
        out.println(map);

        final Map<Integer, Set<String>> map1 = list.stream()
                .collect(Collectors.groupingBy(
                        String::length,
                        Collectors.toSet()));
        out.println(map1);

        final TreeMap<Integer, Set<String>> map2 = list.stream()
                .collect(Collectors.groupingBy(
                        String::length,
                        TreeMap::new,
                        Collectors.toCollection(TreeSet::new)));
        out.println(map2);
    }
}
