package chapter04.chapter04_V2.streams;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.lang.System.out;

public class Stream01 {
    public static void main(String[] args) {

        final Stream<String> s = Stream.of("Monkey", "Gorilla", "Bonobo");
        long count = s.count();
        out.println(count);

        Stream.of("Monkey", "Gorilla", "Bonobo")
                .sorted(String::compareTo)
                .forEach(out::println);

        final Stream<String> s2 = Stream.of("Monkey", "Gorilla", "Bonobo");
//        final Optional<String> min = s2.min((a, b) -> a.length() - b.length());
        final Optional<String> min2 = s2.max(Comparator.comparing(String::length));
//        min.ifPresent(System.out::println);
        min2.ifPresent(out::println);


        out.println(Stream
                .of("W", "O", "L", "F")
                .reduce("1", String::concat));

        final BinaryOperator<Integer> op = Integer::sum;
        final Stream<Integer> stream = Stream.of(3, 5, 6);
        out.println(stream
                .reduce(1, op, op));        // the last signature is for parallel. allows for intermediate reductions and combines them at the end

        final Stream<String> stringStream = Stream.of("C", "O", "N", "S", "T", "A", "N", "C", "E");
//        TreeSet<String> treeSet = stringStream.collect(Collectors.toCollection(TreeSet::new));
        final TreeSet<String> treeSet = stringStream.collect(TreeSet::new, TreeSet::add, TreeSet::addAll);
        out.println(treeSet);

        final IntStream stream1 = IntStream.rangeClosed(1, 10);
        final OptionalDouble optionalDouble = stream1.average();
        out.println(optionalDouble.orElse(2.2));

        final Stream<String> ohMy = Stream.of("Lions", "tigers", "bears");
        final TreeMap<Integer, String> map = ohMy.collect(
                Collectors.toMap(
                        String::length,
                        v -> v,
                        (s1, s3) -> s1 + ", " + s3,
                        TreeMap::new)
        );
        out.println(map);


    }

}
