package chapter04.chapter04_V2.streams;

import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.out;

public class Stream02 {
    public static void main(String[] args) {

        final String[] wolf = {"w", "o", "l", "f"};

        final TreeSet<String> set = Stream.of(wolf)
                .collect(TreeSet::new, TreeSet::add, TreeSet::addAll);      // Supplier, Accumulator, Combiner
        out.println(set);


        final TreeSet<String> set1 = Stream.of(wolf)
                .collect(Collectors.toCollection(TreeSet::new));
        out.println(set1);


    }
}
