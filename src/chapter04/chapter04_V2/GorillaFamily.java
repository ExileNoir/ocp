package chapter04.chapter04_V2;

@FunctionalInterface
interface Gorilla {
    String move();
}

public class GorillaFamily {
    private String walk = "Walk";

    void everyonePlay(boolean play) {
        String approach = "amble";
//        approach = "fun";     // else approach != effectively final

        play(() -> walk);
        play(() -> play ? "Hitch" : "Run");
        play(() -> approach);
    }

    void play(final Gorilla g) {
        System.out.println(g.move());
    }

    public static void main(String[] args) {
        new GorillaFamily().everyonePlay(true);
    }
}
