package chapter04.chapter04_V2.testExc;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

class Boss {
    private String name;

    public Boss(String name) {
        this.name = name;
    }

    public String getName() {
        return name.toUpperCase();
    }

    public String toString() {
        return getName();
    }

}

public class Exc23 {
    public static void main(String[] args) {
        final List<Boss> bosses = new ArrayList<>(8);
        bosses.add(new Boss("Jenny"));
        bosses.add(new Boss("Ted"));
        bosses.add(new Boss("Grace"));

        bosses.removeIf(s -> s.getName().equalsIgnoreCase("ted"));
        out.println(bosses);
    }
}
