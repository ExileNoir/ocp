package chapter04.chapter04_V2.testExc;

import java.util.function.DoubleToIntFunction;
import java.util.function.IntFunction;

import static java.lang.System.out;

class Bank {
    int savingsInCents;

    static class ConvertToCents {
        static DoubleToIntFunction f = p -> (int) p * 100;      // explicit cast to "int" needed, if not return == double so compiler error
    }
}

public class Exc30 {
    public static void main(String[] args) {
        final Bank creditUnion = new Bank();

        creditUnion.savingsInCents = 100;
        final double deposit = 1.5;

        creditUnion.savingsInCents += Bank.ConvertToCents.f.applyAsInt(deposit);
        out.println(creditUnion.savingsInCents);

    }
}
