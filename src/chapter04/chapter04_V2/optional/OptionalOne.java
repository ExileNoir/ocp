package chapter04.chapter04_V2.optional;

import java.util.Optional;

import static java.lang.System.*;

public class OptionalOne {
    private static Optional<Double> average(int... scores) {
        if (scores.length == 0) return Optional.empty();
        int sum = 0;
        for (final int score : scores) {
            sum += score;
        }
        return Optional.of((double) sum / scores.length);
    }

    public static void main(String[] args) {
        final Optional<Double> opt = average(90, 100);

        if (opt.isPresent()) out.println(opt.get());
        if (opt.isPresent()) out.println(opt);

        opt.ifPresent(out::println);


    }
}
