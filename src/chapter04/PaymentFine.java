package chapter04;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class PaymentFine {
    public static void main(String[] args) {
        LocalDate beginDate = LocalDate.of(2019,9,24);
        LocalDate paymentFinal = beginDate.plusDays(45);
        System.out.println(paymentFinal.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));

        System.out.println(paymentFinal.minus(Period.ofDays(2)).format(DateTimeFormatter.ofPattern("dd/MM/yy")));
        System.out.println(paymentFinal.getDayOfWeek());
    }
}
