package chapter04.chapter04_V3.generics;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.*;

public class UtilitiesTest {
    private static <T> void fill(List<T> list, T val) {
        for (int i = 0; i < list.size(); i++) {
            list.set(i, val);
        }
    }

    public static void main(String[] args) {
        final List<Integer> integers = new ArrayList<>();
        integers.add(10);
        integers.add(20);
        out.println("Original list is: " + integers);
        fill(integers, 69);
        out.println("Updated list is: " + integers);
    }
}
