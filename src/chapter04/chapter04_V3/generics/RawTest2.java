package chapter04.chapter04_V3.generics;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class RawTest2 {
    public static void main(String[] args) {
        List<String> strList = new LinkedList<>();
        strList.add("One");
        strList.add("Two");

        List list = strList;
        list.add(3);
        for (Iterator<Object> iterator = list.iterator(); iterator.hasNext(); ) {
            System.out.println("Item: " + iterator.next());
        }

        for (Iterator<String> iterator = list.iterator(); iterator.hasNext(); ) {
            System.out.println("Item: " + iterator.next());
        }
    }

}
