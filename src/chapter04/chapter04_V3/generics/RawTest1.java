package chapter04.chapter04_V3.generics;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class RawTest1 {
    public static void main(String[] args) {
        List list = new LinkedList();
        list.add("One");
        list.add("Two");
        list.add(3);

        List<String> strList = list;
        for (Iterator<String> iterator = strList.iterator();iterator.hasNext();){
            System.out.println("Item: "+iterator.next());
        }
    }
}
