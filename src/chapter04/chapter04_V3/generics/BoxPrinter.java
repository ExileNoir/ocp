package chapter04.chapter04_V3.generics;

import static java.lang.System.*;

public class BoxPrinter<T> {
    private T val;

    public BoxPrinter(T arg){
        val = arg;
    }

    public String toString(){
        return "["+val+"]";
    }

    public static void main(String[] args) {
        final BoxPrinter<Integer> value1 = new BoxPrinter<>(new Integer(10));
        out.println(value1);

        final BoxPrinter<String> value2 = new BoxPrinter<>("Hello World");
        out.println(value2);
    }
}
