package chapter04.chapter04_V3.generics;

import static java.lang.System.out;

public class Pair<T, U> {
    private T objectOne;
    private U objectTwo;

    public Pair(T t, U u) {
        this.objectOne = t;
        this.objectTwo = u;
    }

    public T getObjectOne() {
        return objectOne;
    }

    public U getObjectTwo() {
        return objectTwo;
    }


    public static void main(String[] args) {
        final Pair<Integer, String> worldCup = new Pair<>(2018, "Belgium");
        out.println("World Cup " + worldCup.getObjectOne() + " in " + worldCup.getObjectTwo());

        final Pair<? extends Number, String> worldCup2 = new Pair<Integer, String>(2019, "Mexico");
        out.println("World Cup " + worldCup2.getObjectOne() + " in " + worldCup2.getObjectTwo());
    }
}
