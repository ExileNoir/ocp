package chapter04.resourceBundles.labels;

import java.util.ListResourceBundle;

public class Labels_fr extends ListResourceBundle {

    @Override
    protected Object[][] getContents() {
        return new Object[][]{
                {"hello", new StringBuilder("Bonjour ma doudou Constance, de Java")}
        };
    }
}
