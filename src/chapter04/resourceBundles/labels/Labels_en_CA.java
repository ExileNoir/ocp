package chapter04.resourceBundles.labels;

import java.util.ListResourceBundle;

public class Labels_en_CA extends ListResourceBundle {

    @Override
    protected Object[][] getContents() {
        return new Object[][]{
                {"hello", new StringBuilder("Hello Constance! From Java File")}
        };
    }
}

/*
 * When we need to move beyond simple property file KEY to VALUE mappings,
 * we can use Resource Bundles as Java classes
 * EXTENDS ListResourceBundle
 * class name == similar to one of property file!
 * */