package chapter04.resourceBundles.labels;

import java.util.ListResourceBundle;

public class Labels_be extends ListResourceBundle {

    @Override
    protected Object[][] getContents() {
        return new Object[][]{
                {"hello", new StringBuilder("Hallo Lieve Constance vanuit Java")}
        };
    }
}
