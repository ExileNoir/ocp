package chapter04.resourceBundles;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

import static java.lang.System.out;

public class WhichLanguage1 {
    public static void main(String[] args) {

        final Scanner kbd = new Scanner(System.in);

        out.println("Give Language");
        final Locale locale = new Locale(kbd.nextLine(),kbd.nextLine());

        // Use static factory method to call the Resource Bundle
        // Need to pass the full bundle name: package/directory, with Locale as parameter!!!
        final ResourceBundle rb = ResourceBundle.getBundle("chapter04.resourceBundles.labels.Labels", locale);

        out.println(rb.getObject("hello"));
    }
}
