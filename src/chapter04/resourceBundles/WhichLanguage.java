package chapter04.resourceBundles;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

import static java.lang.System.out;

public class WhichLanguage {
    public static void main(String[] args) {

        final Scanner kbd = new Scanner(System.in);

        out.println("Give Language");
        final Locale locale = new Locale(kbd.nextLine());

        final ResourceBundle rb = ResourceBundle.getBundle("Labels", locale);

//      out.println(rb.getString("hello"));
        out.println(rb.getObject("hello"));
    }
}
/*
 * 3 good reasons to write resource bundles:
 *
 * * Easily Localized, or translated int different languages,
 * * Handle multiple locales at once,
 * * Easily modified later to support even more locales.
 * */