package chapter04.resourceBundles;

import java.util.Locale;
import java.util.ResourceBundle;

import static java.lang.System.out;
public class WhichLanguage2 {
    public static void main(String[] args) {

        final Locale locale = new Locale("en","UK");

        final ResourceBundle rb = ResourceBundle.getBundle("RB",locale);

        out.println(rb.getObject("ride.in") +" "+ rb.getObject("elevator"));

    }


}
