package chapter04.dateTimes;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;

import static java.lang.System.out;

public class ConstanceBday {
    public static void main(String[] args) {
        String constanceBday = "25/08/2019 12:49";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        LocalDateTime bDayConstance = LocalDateTime.parse(constanceBday, formatter);
        out.println("My little troubleMaker was born on " + bDayConstance.format(DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm")));

        LocalDateTime today = LocalDateTime.now();
        Duration birthAndNow = Duration.between(bDayConstance, today);
        out.println("\nConstance age = " + birthAndNow.toDays() + " Days Old");

        out.println("Constance age in period: "+Period.between(bDayConstance.toLocalDate(),today.toLocalDate()));
    }
}
