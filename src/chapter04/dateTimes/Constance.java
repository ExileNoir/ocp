package chapter04.dateTimes;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Set;

import static java.lang.System.out;

public class Constance {
    public static void main(String[] args) {

        final LocalDate nowDate = LocalDate.now();
        final LocalTime nowTime = LocalTime.now();
        final LocalDateTime nowDateTime = LocalDateTime.of(nowDate, nowTime);
        final LocalDateTime nowDateTime1 = LocalDateTime.now();
        out.println("It's Currently : " + nowDateTime + " where I am (" + nowDateTime1 + ")");  // "Date << T >>  Time" LocalDateTime converted to String
        out.println("It's Currently (Formatted): " + nowDateTime.format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm")));

        // Constance bdayDate
        final LocalDate birthConstance = LocalDate.of(2019, Month.AUGUST, 25);
        final LocalDate birthConstance1 = LocalDate.of(2019, 8, 25);
        final LocalDate birthConstance2 = LocalDate.parse("2019-08-25");         // ISO-8601 calender == YYYY-MM-DD pattern
        out.println("\nConstance's Birth date : " + birthConstance + "  (" + birthConstance1 + " ; " + birthConstance2 + ")");

        // Constance bdayTime
        final LocalTime birthTimeConstance = LocalTime.of(12, 49);
        final LocalTime birthTimeConstance1 = LocalTime.parse("12:49");
        out.println("\nConstance was born at " + birthTimeConstance + "  (" + birthTimeConstance1 + ")");

        // Constance bdayDateTime
        final String birthDateTimeConstance = "2019-08-25 12:49";
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"); // pattern == YYYY-MM-DD (else DateTimeParseException)
        final LocalDateTime birthDayConstance = LocalDateTime.parse(birthDateTimeConstance, formatter); // use of formatter
        out.println("\nConstance Bday : " + birthDayConstance);
        out.println("Constance Bday formatted : " + birthDayConstance.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        out.println("Constance Bday formatted1 : " + birthDayConstance.format(DateTimeFormatter.ofPattern("dd, MM, yy hh, mm")));

        out.println("\nDay of week of Constance's bDay : " + birthDayConstance.getDayOfWeek());
        out.println("Day of year of Constance's bDay : " + birthDayConstance.getDayOfYear());

        // Zoned Dates && Times
        final ZonedDateTime zonedDateTimeBdayConstance = ZonedDateTime.of(birthDayConstance, ZoneId.of("Europe/Paris"));
        out.println("\nDate && Time Birth Constance with TimeZone " + zonedDateTimeBdayConstance);

        final Set<String> zoneIds = ZoneId.getAvailableZoneIds();
        zoneIds.forEach(s -> {
            if (s.contains("Paris") || s.contains("Brussels")) out.println(s);
        });

        // ZoneRules == class captures current rules about daylight savings in various pars of world
        final ZoneId brussels = ZoneId.of("Europe/Brussels");
        out.println("\nIs DayLight savings in effect at time of Birth Constance : " + brussels.getRules().isDaylightSavings(zonedDateTimeBdayConstance.toInstant()));

        // Date && Time Adjustments (dateTime == immutable)
        final LocalDateTime followingThuDateTime = birthDayConstance.with(TemporalAdjusters.next(DayOfWeek.SUNDAY));
        out.println("Sunday following her bday : " + followingThuDateTime.format(DateTimeFormatter.ofPattern("dd MM yy -- hh:mm")));

        // Periods && Durations && Instants
        // Period  -- Period of days/months/years
        final Period period = Period.ofMonths(1);
        out.println("\nPeriod is : " + period);
        final ZonedDateTime reminderConstanceBday = zonedDateTimeBdayConstance.minus(period);
        out.println("DateTime of one month reminder : " + reminderConstanceBday); // timeZone
        out.println("Reminder formatted " + reminderConstanceBday.format(DateTimeFormatter.ofPattern("dd/MM/yyyy - HH:mm a z")));
        out.println("Local DateTime (Paris) of reminder " + reminderConstanceBday.toLocalDateTime()); // No timeZone

        // Durations  -- short period of minutes or hours
        // ChronoUnit == a unit of time
        // Duration == specifying a period of time
        final LocalTime beginLabour = LocalTime.of(00, 15);
        final LocalTime birth = LocalTime.of(12, 49);
        out.println("\nBegin labour: " + beginLabour + " Birth " + birth);

        final long betweenMin = ChronoUnit.MINUTES.between(beginLabour, birth);
        final long betweenHours = ChronoUnit.HOURS.between(beginLabour, birth);
        out.println("Minutes between labour && birth " + betweenMin + " min");
        out.println("Hours between labour && birth " + betweenHours + " hours");

        final Duration betweenDurationM = Duration.ofMinutes(betweenMin);
        final Duration betweenDurationH = Duration.ofHours(betweenHours);
        final Duration betweenLabourAndBirth = Duration.between(beginLabour, birth);
        out.println("Duration between minutes " + betweenDurationM + " min");
        out.println("Duration between hours " + betweenDurationH + " hours");
        out.println("Duration between labour and birth " + betweenLabourAndBirth);

        final LocalTime birthBegan = beginLabour.plus(betweenDurationM);
        out.println("Birth began (computed) " + birthBegan);

        // Instant == represents an instant in time
        final Instant bdayConstanceInstant = zonedDateTimeBdayConstance.toInstant();
        out.println("\nConstance bday instant: " + bdayConstanceInstant);

        final Instant localDateTimeConstanceInstant = birthDayConstance.toInstant(ZoneOffset.of("Z"));
        out.println(localDateTimeConstanceInstant);


        final Instant nowInstant = Instant.now();
        final long hoursBetweenInstants = ChronoUnit.HOURS.between(zonedDateTimeBdayConstance.toInstant(), nowInstant);
        final long minBetweenInstants = ChronoUnit.MINUTES.between(bdayConstanceInstant, nowInstant);
        final long daysBetweenInstants = ChronoUnit.DAYS.between(zonedDateTimeBdayConstance.toInstant(), nowInstant);

        final Duration durationBetweenInstants = Duration.ofHours(hoursBetweenInstants);
        final Duration durationBetweenInstantsMinutes = Duration.ofMinutes(minBetweenInstants);
        final Duration durationBetweenInstantsDays = Duration.ofDays(daysBetweenInstants);

        out.println("\nHours between (long) " + hoursBetweenInstants + " is duration " + durationBetweenInstants);
        out.println("minutes between (long) " + minBetweenInstants + " is duration " + durationBetweenInstantsMinutes);
        out.println("days between (long) " + daysBetweenInstants + " is duration " + durationBetweenInstantsDays);



    }
}
