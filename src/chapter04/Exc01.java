package chapter04;

import java.time.ZonedDateTime;

import static java.lang.System.*;

public class Exc01 {
    public static void main(String[] args) {

        final ZonedDateTime zd = ZonedDateTime.parse("2020-05-04T08:05:00");    // DateTimeParseException
        out.println(zd.getMonth() + " " + zd.getDayOfMonth());
    }

}
