package chapter04;

import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

import static java.lang.System.out;

public class Exc02 {
    public static void main(String[] args) {

        final LocalTime t1 = LocalTime.of(9, 0);
        final LocalTime t2 = LocalTime.of(10, 5);
        out.println(t2);

        final LocalTime t3 = t1.plus(65, ChronoUnit.MINUTES);
        out.println(t3);

        final LocalTime t4 = t1.plusMinutes(65);
        out.println(t4);

        final LocalTime t5 = t1.plus(Duration.ofMinutes(65));
        out.println(t5);
    }
}

