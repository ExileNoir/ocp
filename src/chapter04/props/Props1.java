package chapter04.props;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Props1 {
    public static void main(String[] args) throws IOException {

        // SET, LIST, STORE

        final Properties p = new Properties();
        p.setProperty("K1", "Value 1");
        p.setProperty("K2", "Value 2");
        p.list(System.out);

        try (final FileOutputStream out = new FileOutputStream("myProps1.props")) {
            p.store(out, "test-comment"); // adds header comment
        }
    }
}
