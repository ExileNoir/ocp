package chapter04.props;

import java.util.Properties;

public class SysProps {
    public static void main(String[] args) {

        final Properties props = System.getProperties();  // open system Prop file
        props.setProperty("MyProp", "MyValue"); // add an entry
        props.list(System.out);
        props.remove("MyProp");

    }
}
