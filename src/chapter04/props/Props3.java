package chapter04.props;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Props3 {
    public static void main(String[] args) throws IOException {

        // LOAD, GET

        final Properties p3 = new Properties();

        try(final FileInputStream in = new FileInputStream("myProps2.props")){  // makes a FIS to file
            p3.load(in);    // loads the FIS

            final String value = p3.getProperty("newProp");     // getProp value

            System.out.println(value);
        }
    }
}
