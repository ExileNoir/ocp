package chapter04.props;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Props2 {
    public static void main(String[] args) throws IOException {

        // LOAD, LIST, SET, STORE

        final Properties p2 = new Properties();
        try (final FileInputStream in = new FileInputStream("myProps1.props")) {
            p2.load(in);    // loads the prop1File via FIS
            p2.list(System.out);

            p2.setProperty("newProp", "new Data");      // sets the new data
            p2.list(System.out);

            final FileOutputStream out = new FileOutputStream("myProps2.props");
            p2.store(out, "myUpdate");  // writes the new data via FOS in file

        }
    }
}
