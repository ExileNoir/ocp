package chapter10.synchronizingAndProblems;

import static java.lang.System.out;

class Account {
    private int balance = 50;

    public int getBalance() {
        return balance;
    }

    public void withdraw(final int amount) {
        balance -= amount;
    }
}

public class AccountDanger implements Runnable {

    private final Account acc = new Account();

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            makeWithdrawal(10);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if ((acc.getBalance() < 0)) {
                out.println("Account is overdrawn!");
            }
        }
    }

    /* 'synchronized' for atomic operations, protection */
    private /*synchronized*/ void makeWithdrawal(final int amount) {
        synchronized (acc) {
            if (acc.getBalance() >= amount) {
                out.println(Thread.currentThread().getName() + " is going to withdrawal");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                acc.withdraw(amount);
                out.println(Thread.currentThread().getName() + " completes the withdrawal. " + acc.getBalance() + " Current accountBalance");
            } else {
                out.println("Not enough in account for " + Thread.currentThread().getName() + " to withdraw " + acc.getBalance());
            }
        }
    }

    /* Synchronized:
     * Guarantees that one a thread (Steven || Salomé) starts the withdrawal process
     * by invoking makeWithdrawal(), the other thread cannot enter that
     * method until the first one completes the process by exiting the method.
     *  */


    public static void main(String[] args) {
        final AccountDanger account = new AccountDanger();
        final Thread one = new Thread(account, "Steven");
        final Thread two = new Thread(account, "Salomé");

        one.start();
        two.start();
    }
}
