package chapter10.synchronizingAndProblems;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.System.out;

public class AccountSync {
    private static Account account = new Account();

    public static void main(String[] args) {
        final ExecutorService executor = Executors.newCachedThreadPool();

        // Create and launch 100 threads
        for (int i = 0; i < 100; i++) {
            executor.execute(new AddAPennyTask());
        }

        executor.shutdown();

        // Wait until all tasks are finished
        while (!executor.isTerminated()) {
        }

        out.println("What is balance? " + account.getBalance());
    }

    // A thread for adding a penny to the account
    private static class AddAPennyTask implements Runnable {
        public void run() {
            synchronized (account) {
                account.deposit(1);
            }
        }
    }

    // An inner class for account
    private static class Account {
        private int balance = 0;

        public int getBalance() {
            return balance;
        }

        public /*synchronized*/ void deposit(int amount) {
//            synchronized (this) {
            int newBalance = balance + amount;

            // This delay is deliberately added to magnify the
            // data-corruption problem and make it easy to see.
            try {
                Thread.sleep(5);
            } catch (InterruptedException ex) {
            }

            balance = newBalance;
//            }
        }
    }
}
// 3 ways to avoid race Condition && make Class Thread-safe