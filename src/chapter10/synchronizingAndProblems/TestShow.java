package chapter10.synchronizingAndProblems;

import java.util.HashSet;
import java.util.Set;

class Show {

    /*
     * volatile:
     * Makes sure that the variable == atomic
     * A write to the variable happens all at once.
     * Nothing can interrupt this process:
     * it either happens completely, or it doesn't happen at all.
     * In getInstance(), we create a new instance of Show() AND assign it to INSTANCE,
     * that entire operation must complete before the Thread can be interrupted
     * */
    private static volatile Show INSTANCE;
    private Set<String> availableSeats;

    private Show() {
        availableSeats = new HashSet<>();
        availableSeats.add("1A");
        availableSeats.add("1B");
    }

    /*
     * Synchronized:
     * Makes sure only one Thread at a time can access the getInstance() method
     * */
    public static synchronized Show getInstance() {       // Create a Singleton Instance
        if (INSTANCE == null) {
            INSTANCE = new Show();      // should be only Show!
        }
        return INSTANCE;
    }

    /*
     * Due to the Shared resource  && Set != ThreadSafe
     * Thread could be interrupted in the middle of removing seat "1A" from the set,
     * and another Thread could come along and access the Set.
     * */
    public synchronized boolean bookSeats(final String seat) {
        return availableSeats.remove(seat);
    }
}

public class TestShow {
    public static void main(String[] args) {
        final TestShow testThreads = new TestShow();
        testThreads.go();
    }

    public void go() {
        // create Thread 1, which will try to book seats 1A and 1B
        final Thread getSeats1 = new Thread(() -> {
            ticketAgentBooks("1A");
            ticketAgentBooks("1B");
        });

        // create Thread 2, which will try to book seats 1A and 1B
        final Thread getSeats2 = new Thread(() -> {
            ticketAgentBooks("1A");
            ticketAgentBooks("1B");
        });
        // start both Threads
        getSeats1.start();
        getSeats2.start();

    }

    private void ticketAgentBooks(final String seat) {
        // get the one instance of the Show Singleton
        final Show show = Show.getInstance();
        // book a seat and print
        System.out.println(Thread.currentThread().getName() + " : " + show.bookSeats(seat));
    }
}

