package chapter10.synchronizingAndProblems;

public class Exercise10_2 extends Thread {

    private StringBuffer letter;

    public Exercise10_2(final StringBuffer buffer) {
        this.letter = buffer;
    }



    @Override
    public void run() {
        synchronized (letter) {     // we Synchronize here and NOT the method signature to make it work
            for (int i = 0; i < 100; i++) {
                System.out.print(letter);
            }
            System.out.println();
            char temp = letter.charAt(0);
            ++temp;
            letter.setCharAt(0, temp);
        }
    }

    public static void main(String[] args) {
        final StringBuffer sb = new StringBuffer("A");
        new Exercise10_2(sb).start();
        new Exercise10_2(sb).start();
        new Exercise10_2(sb).start();
    }
}
