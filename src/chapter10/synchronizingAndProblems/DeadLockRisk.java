package chapter10.synchronizingAndProblems;

public class DeadLockRisk {
    private static class Resource {
        public int value;
    }

    private Resource resourceA = new Resource();
    private Resource resourceB = new Resource();

    public int read() {
        synchronized (resourceA) {   // may deadlock here
            synchronized (resourceB) {
                return resourceB.value + resourceA.value;
            }
        }
    }

    public void write(final int a, final int b) {
        synchronized (resourceB) {       // may deadlock here
            synchronized (resourceA) {
                resourceA.value = a;
                resourceB.value = b;
            }
        }
    }
}
/*
 * fix this situation by swap the order of locking for either the reader || writer
 * */

/*
* DeadLock:
* Occurs when 2 Threads have a circular dependency on a pair of synchronized objects.
* */