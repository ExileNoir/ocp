package chapter10.synchronizingAndProblems;

public class Counter {
    private int count = 0;

    public void increment() {
        synchronized (this) {
            count++;
        }
    }

    public void decrement() {
        synchronized (this) {
            count--;
        }
    }

    public int getCount() {
        return count;
    }

    public static void main(String[] args) throws InterruptedException {
        final Counter counter = new Counter();
        final Thread thread01 = new Thread(() -> increment(counter, 20));
        final Thread thread02 = new Thread(() -> increment(counter, 20));

        thread01.start();
        thread02.start();
        thread01.join();
        thread02.join();

        System.out.println(counter.getCount());
    }

    private static void increment(final Counter counter, final int number) {
        for (int i = 0; i < number; i++) {
            counter.increment();
        }
    }
}
