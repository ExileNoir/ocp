package chapter10.synchronizingAndProblems;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class NameList {
    private List<String> names = Collections.synchronizedList(new LinkedList<>());

    public void add(String name) {
        synchronized (this) {
            names.add(name);
        }
    }

    public String removeFirst() {
        synchronized (this) {
            if (!names.isEmpty()) {
                return (String) names.remove(0);
            } else {
                return null;
            }
        }
    }

    public static void main(String[] args) {
        final NameList nl = new NameList();
        nl.add("ExileNoir");

        class NameDropper extends Thread {
            public void run() {
                final String name = nl.removeFirst();
                System.out.println(name);
            }
        }

        final Thread t1 = new NameDropper();
        final Thread t2 = new NameDropper();
        t1.start();
        t2.start();
    }
}
