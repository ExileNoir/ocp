package chapter10.synchronizingAndProblems;

import static java.lang.System.out;

class A {
    synchronized void foo(final B b) {
        final String name = Thread.currentThread().getName();

        out.println(name + " entered A.foo");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException ie) {
            out.println("A interrupted");
        }
        out.println(name + " trying to call B.last");
        b.last();
    }

    synchronized void last() {
        out.println("Inside A.last");
    }
}

class B {
    synchronized void bar(final A a) {
        final String name = Thread.currentThread().getName();

        out.println(name + " entered B.bar");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException ie) {
            out.println("B interrupted");
        }
        out.println(name + " Trying to call A.last");
        a.last();
    }

    synchronized void last() {
        out.println("Inside B.last");
    }
}

public class DeadLock implements Runnable {
    final A a = new A();
    final B b = new B();

    DeadLock() {
        Thread.currentThread().setName("MainThread");
        Thread t = new Thread(this, "RacingThread");
        t.start();

        a.foo(b);   // get a lock on A this Thread
        out.println("Back in 'main' Thread");
    }

    @Override
    public void run() {
        b.bar(a);   // get a lock on B in other Thread
        out.println("Back in other Thread");
    }

    public static void main(String[] args) {
        new DeadLock();
    }

}
