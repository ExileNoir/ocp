package chapter10.testExc;

class Dudes {
    static long flag = 0;

    /*synchronized*/ void chat(long id) {
        if (flag == 0) flag = id;
        for (int i = 1; i < 3; i++) {
            if (flag == id) System.out.print("yo ");
            else System.out.print("dude ");
        }
    }
}

public class DudesChat implements Runnable {
    static Dudes d;

    void go() {
        d = new Dudes();
        new Thread(new DudesChat()).start();
        new Thread(new DudesChat()).start();
    }

    public void run() {
        d.chat(Thread.currentThread().getId());
    }

    public static void main(String[] args) {
        new DudesChat().go();
    }
}
