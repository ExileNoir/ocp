package chapter10.threadStatesAndTransitions.yield;

import static java.lang.System.out;

public class CoopMultitasking {
    public static void main(String[] args) {
        final Thread thread1 = new Thread(() -> print('*', 10));
        final Thread thread2 = new Thread(() -> print('@', 10));

        thread1.start();
        thread2.start();

    }

    private static void print(final char c, final int count) {
        for (int i = 0; i < count; i++) {
            out.print(c);
            Thread.yield();
        }
    }
}
