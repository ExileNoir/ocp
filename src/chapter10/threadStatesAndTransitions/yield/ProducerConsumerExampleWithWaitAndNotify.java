package chapter10.threadStatesAndTransitions.yield;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

class Producer implements Runnable {
    private final List<Integer> taskQueue;
    private final int maxCapacity;

    Producer(final List<Integer> sharedQueue, int size) {
        this.taskQueue = sharedQueue;
        this.maxCapacity = size;
    }

    @Override
    public void run() {
        int counter = 0;
        while (true) {
            try {
                produce(counter++);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

    private void produce(final int counter) throws InterruptedException {
        synchronized (taskQueue) {
            while (taskQueue.size() == maxCapacity) {
                out.println("Queue is full: " + Thread.currentThread().getName() + " is waiting, size: " + taskQueue.size());
                taskQueue.wait();
            }
            Thread.sleep(500);
            taskQueue.add(counter);
            out.println("Produced: " + counter);
            taskQueue.notifyAll();
        }
    }
}


class Consumer implements Runnable {
    private final List<Integer> taskQueue;

    Consumer(final List<Integer> sharedQueue) {
        this.taskQueue = sharedQueue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                consume();
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

    private void consume() throws InterruptedException {
        synchronized (taskQueue) {
            while (taskQueue.isEmpty()) {
                out.println("Queue is empty: " + Thread.currentThread().getName() + " is waiting, size: " + taskQueue.size());
                taskQueue.wait();
            }
            Thread.sleep(500);
            final int count = (Integer) taskQueue.remove(0);
            out.println("Consumed: " + count);
            taskQueue.notifyAll();
        }
    }
}


public class ProducerConsumerExampleWithWaitAndNotify {
    public static void main(String[] args) throws InterruptedException {
        final List<Integer> taskQueue = new ArrayList<>();
        int maxCapacity = 5;
        final Thread tProducer = new Thread(new Producer(taskQueue,maxCapacity),"Producer");
        final Thread tConsumer = new Thread(new Consumer(taskQueue),"Consumer");

        tProducer.start();
        tConsumer.start();
    }
}
