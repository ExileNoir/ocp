package chapter10.threadStatesAndTransitions.sleep;

class NameRunnable implements Runnable {

    @Override
    public void run() {
        for (int i = 1; i < 4; i++) {
            System.out.println("Run by " + Thread.currentThread().getName());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }
}

public class ManyNames {
    public static void main(String[] args) throws InterruptedException {

        final NameRunnable nr = new NameRunnable();

        final Thread one = new Thread(nr, "Steven");
        final Thread two = new Thread(nr, "Salome");
        final Thread three = new Thread(nr, "Constance");

        one.start();
        Thread.sleep(2000);
        two.start();
        three.start();
    }
}

/*
 * Just because a thread's sleep expires and it wakes up does not mean it will return running!
 * when a thread wakes up, it simply goes back to the 'runnable' state
 * sleep() time is NOT guarantee that the thread will start running again as soon as the time expires and the thread wakes
 *
 * sleep() == static method:
 * do not be fooled into thinking that one thread can put another to sleep!
 * */