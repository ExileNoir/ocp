package chapter10.threadStatesAndTransitions.sleep;

public class Exc10_1Threads {
    public static void main(String[] args) {

        final Runnable r = () -> {
            for (int i = 1; i <= 100; i++) {
                System.out.println("counter: " + i);
                if (i % 10 == 0) {
                    System.out.println("modulo ok " + i);
                }
                try {
                    Thread.sleep(1000);     // Thread is being called out of RUNNABLE (!active) && only active RUNNING thread can go into sleep
                } catch (InterruptedException e) {      // status: TIMED_WAITING
                    e.printStackTrace();
                }
            }
        };

        final Thread t = new Thread(r, "StevenTest");
        t.start();
    }
}
