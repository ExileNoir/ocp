package chapter10.threadStatesAndTransitions.join;

import static java.lang.System.*;

public class TestJoinMethod01 {
    public static void main(String[] args) throws Exception{

        final Runnable r = ()->{
            for (int i = 1; i < 10 ; i++) {
                try {
                    Thread.sleep(500);
                }catch (InterruptedException ie){
                    out.println("Something went wrong");
                }
                out.println(i+" : "+Thread.currentThread().getName());
            }
        };

        final Thread t1 = new Thread(r,"T1");
        final Thread t2 = new Thread(r,"T2");
        final Thread t3 = new Thread(r,"T3");

        t1.start();
        t1.join();

        t2.start();
//        t2.join();

        t3.start();
        t3.join();

        out.println( Thread.currentThread().getName());;

    }
}
