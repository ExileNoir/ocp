package chapter10.threadInteraction;

import static java.lang.System.out;

class Calculator implements Runnable {
    private int total;

    @Override
    public void run() {
        out.println("enter run of calc");
        synchronized (this) {
            for (int i = 0; i < 100; i++) {
                total += i;
            }
            notifyAll();
        }
    }

    public int getTotal() {
        return total;
    }
}

public class Reader extends Thread {
    Calculator calculator;

    public Reader(Calculator calc) {
        calculator = calc;
    }

    @Override
    public void run() {
        synchronized (calculator) {
            try {
                out.println("Waiting for calculations...");
                calculator.wait(1000);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
            out.println("Total is: " + calculator.getTotal());
        }
    }

    public static void main(String[] args) {
        final Calculator calculator = new Calculator();
        new Reader(calculator).start();
//        new Reader(calculator).start();
//        new Reader(calculator).start();
        new Thread(calculator).start();
    }
}
