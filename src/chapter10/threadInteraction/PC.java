package chapter10.threadInteraction;

import static java.lang.System.out;

class Queue {
    private int n;
    private boolean valueSet = false;

    synchronized int get() {
        while (!valueSet) {
            try {
                wait();     // This causes its execution to suspend until Producer notifies that some data is ready.
            } catch (InterruptedException ie) {
                out.println("Interrupted Ex caught");
            }
        }
        out.println("Get: " + n);
        valueSet = false;
        notify();   // After data has been obtained. This tells Producer that it's okay to put more data in the queue
        return n;
    }

    synchronized void put(final int n) {
        while (valueSet) {
            try {
                wait();     // Suspends execution until Consumer has removed the item from the queue
            } catch (InterruptedException ie) {
                out.println("Interrupted Ex caught");
            }
        }
        this.n = n;     // When execution resumes, next item of data is put in the queue
        valueSet = true;
        out.println("Put: " + n);
        notify();       // This tells the Consumer that it should now remove it from queue
    }
}

class Producer implements Runnable {
    private Queue q;

    Producer(final Queue q) {
        this.q = q;
        new Thread(this, "Producer").start();
        out.println("Producer Starts");
    }

    @Override
    public void run() {
        int i = 0;

        while (true) {
            q.put(i++);
        }
    }
}

class Consumer implements Runnable {
    private Queue q;

    Consumer(final Queue q) {
        this.q = q;
        new Thread(this, "Consumer").start();
        out.println("Consumer Starts");
    }

    @Override
    public void run() {
        while (true) {
            q.get();
        }
    }
}

public class PC {
    public static void main(String[] args) {
        final Queue q = new Queue();
        new Producer(q);
        new Consumer(q);

        out.println("Press Control-C to stop.");
    }
}
