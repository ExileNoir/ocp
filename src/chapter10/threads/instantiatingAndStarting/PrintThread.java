package chapter10.threads.instantiatingAndStarting;

public class PrintThread extends Thread {
    private char c;
    private int count;

    PrintThread(final char c, final int count) {
        this.c = c;
        this.count = count;
    }

    @Override
    public void run() {
        for (int i = 0; i < count; i++) {
            System.out.print(c + " ");
        }
    }

    public static void main(String[] args) {
        final PrintThread thread1 = new PrintThread('*', 20);
        final PrintThread thread2 = new PrintThread('#', 20);

        thread1.start();
        thread2.start();

    }
}
