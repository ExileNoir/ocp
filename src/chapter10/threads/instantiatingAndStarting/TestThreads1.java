package chapter10.threads.instantiatingAndStarting;

import static java.lang.System.*;

public class TestThreads1 {
    public static void main(String[] args) {

        final Runnable r = () -> {
            for (int x = 1; x < 6; x++) {
                out.println("Runnable running " + x);
            }
        };
        final Thread t = new Thread(r,"MyThreadRunnable");




        t.start();
        out.println(t.getName());
    }
}
