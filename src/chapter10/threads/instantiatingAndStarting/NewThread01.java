package chapter10.threads.instantiatingAndStarting;

import static java.lang.System.out;

public class NewThread01 extends Thread {
    // Create a second Thread

    NewThread01() {
        // Create a new, Second Thread
        super("Demo Thread");       // call to super()
        out.println("Child Thread: " + this);
        start();      // Start the Thread
    }

    // This is entry point for the second Thread
    @Override
    public void run() {
        try {
            for (int i = 5; i > 0; i--) {
                out.println("Child Thread: " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException ie) {
            out.println("Child Interrupted");
        }
        out.println("Exiting Child Thread.");
    }

    public static void main(String[] args) {
        new NewThread();    // create a new Thread

        try {
            for (int i = 5; i > 0; i--) {
                out.println("Main Thread: " + i);
                Thread.sleep(2000);
            }
        } catch (InterruptedException ie) {
            out.println("Main Thread interrupted");
        }
        out.println("Exiting Main Thread");
    }
}
