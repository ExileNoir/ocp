package chapter10.threads.instantiatingAndStarting;

import static java.lang.System.out;

public class PrintAppLambda {
    public static void main(String[] args) {
        final Runnable runnable = () -> print('*', 20);
        final Thread thread = new Thread(runnable, "First Shizzle");
        thread.start();

        final Thread thread1 = new Thread(
                () -> print('#', 20));
        thread1.start();
    }

    private static void print(final char c, final int count) {
        for (int j = 0; j < count; j++) {
            out.print(c + " ");
        }
    }
}
