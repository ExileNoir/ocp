package chapter10.threads.instantiatingAndStarting;

import static java.lang.System.out;

/* Create Multiple Threads */
class NewThread02 implements Runnable {
    private String name;
    private Thread thread;

    public NewThread02(final String threadName) {
        this.name = threadName;
        thread = new Thread(this, name);
        out.println("New Thread: " + thread.getName());
        thread.start();
    }

    // This is the entry point for the Thread
    @Override
    public void run() {
        try {
            for (int i = 5; i > 0; i--) {
                out.println(name + ": " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException ie) {
            out.println(name + " Interrupted");
        }
        out.println(name + " Exiting.");
    }

    public Thread getThread() {
        return thread;
    }
}

public class MultiThreadDemo {
    public static void main(String[] args) {
        final NewThread02 ob1 = new NewThread02("One");
        final NewThread02 ob2 = new NewThread02("Two");
        final NewThread02 ob3 = new NewThread02("Three");

        out.println("Thread One is alive: " + ob1.getThread().isAlive());
        out.println("Thread Two is alive: " + ob2.getThread().isAlive());
        out.println("Thread Three is alive: " + ob3.getThread().isAlive());

        // wait for threads to finish.
        try {
            out.println("Wait for Threads to finish");
            ob1.getThread().join();
            ob2.getThread().join();
            ob3.getThread().join();
        } catch (InterruptedException ie) {
            out.println("Main Thread Interrupted");
        }

        out.println("Thread One is alive: " + ob1.getThread().isAlive());
        out.println("Thread Two is alive: " + ob2.getThread().isAlive());
        out.println("Thread Three is alive: " + ob3.getThread().isAlive());

        out.println("Main Thread exiting");
    }
}
