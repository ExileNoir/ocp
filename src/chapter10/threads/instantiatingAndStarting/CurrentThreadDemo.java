package chapter10.threads.instantiatingAndStarting;

import static java.lang.System.out;

public class CurrentThreadDemo {
    public static void main(String[] args) {

        // Controlling the main Thread.
        final Thread t = Thread.currentThread();

        out.println("Current Thread: " + t);

        // change the name of the Thread
        t.setName("My ExileNoir Thread");
        out.println("After name change: " + t);
        out.println("After name change (getName()): " + t.getName());

        try {
            for (int i = 5; i > 0; i--) {
                out.println(i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException ie) {
            out.println("Main Thread interrupted");
        }
    }
}
