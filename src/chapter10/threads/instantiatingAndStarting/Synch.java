package chapter10.threads.instantiatingAndStarting;

import static java.lang.System.out;

class CallMe {
    // to avoid << race condition >> == add synchronized.
    // if not: nothing exists to stop all 3 Threads from calling same method, on the same object.
    /*synchronized*/ void call(final String msj) {
        out.print("[" + msj);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ie) {
            out.println("Interrupted");
        }
        out.println("]");
    }
    /*
     * Any time you have a method / group of methods, that manipulate the internal state of an object in a multithreaded situation,
     * you use the synchronize keyword to guard the state from race conditions.
     * */
}

class Caller implements Runnable {
    private String msg;
    private CallMe target;
    private Thread thread;

    public Caller(final CallMe target, final String s) {
        this.target = target;
        this.msg = s;
        this.thread = new Thread(this);
        this.thread.start();
    }

    @Override
    public void run() {
        /*
         * Synchronized block ensures that a call to a synchronized method that is a member of 'target' class occurs only
         * after the current thread has successfully entered 'target's' monitor.
         * */
        synchronized (target) {
            this.target.call(msg);
        }
    }

    public Thread getThread() {
        return thread;
    }
}

public class Synch {
    public static void main(String[] args) {
        final CallMe target = new CallMe();
        final Caller ob1 = new Caller(target, "Hello");
        final Caller ob2 = new Caller(target, "Synchronized");
        final Caller ob3 = new Caller(target, "World");

        // wait for Threads to end
        try {
            ob1.getThread().join();
            ob2.getThread().join();
            ob3.getThread().join();
        } catch (InterruptedException ie) {
            out.println("Interrupted");
        }
        out.println("Main Thread ends");
    }
}
