package chapter10.threads.instantiatingAndStarting.revision;

import static java.lang.System.out;

public class Thread01 {
    public static void main(String[] args) throws InterruptedException {

        final Runnable r = new Runnable() {
            public void run() {
                out.println("We are running Zero");
            }
        };

        final Runnable r1 = () -> out.println("We are running One");

        final Thread t1 = new Thread(r, "R0");
        final Thread t2 = new Thread(r1, "R1");
        final Thread t3 = new Thread(() -> out.println("We are running two"), "R2");
        final Thread t4 = new Thread(t3);

        out.println(Thread.currentThread().getName() + " Opening");
        t1.start();
        Thread.sleep(1000);
        t2.start();
        t3.start();
        t3.join();       // so main has to wait closing before this join ends
        t4.start();     // does not print when t3 is NOT commented
        out.println(Thread.currentThread().getName() + " Closing");

    }


}
