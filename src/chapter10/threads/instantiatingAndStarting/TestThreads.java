package chapter10.threads.instantiatingAndStarting;

import chapter10.threads.defining.MyRunnable;
import chapter10.threads.defining.MyThread;

import static java.lang.System.out;

public class TestThreads {
    public static void main(String[] args) {

        // if you extend the Thread class
        final MyThread t = new MyThread();


        // if you implement the Runnable interface
        final MyRunnable r = new MyRunnable();
        final Thread t1 = new Thread(r, "MyRunnable");   // pass your Runnable to the Thread


        // or get rid of MyRunnable and write:
        final Runnable r1 = () -> out.println("Important job running in a Runnable");
        final Thread foo = new Thread(r1);
        final Thread bar = new Thread(r1);
        final Thread bat = new Thread(r1);
        // you can pass a single Runnable instance to multiple Thread Objects
        // Means that several Threads of execution will be running the very same job


        // lambda expression, you can eliminate MyRunnable
        final Thread t2 = new Thread(
                () -> out.println("Important job in a Runnable via lambda")
        );


        /*
         * Thread class itself implements Runnable:
         * you could pass a Thread to another Thread's constructor
         * Silly BUT legal
         * */
        final Thread t3 = new Thread(new MyThread());


        /*
         * When Thread == instantiated but != started => Thread is in the 'new state' meaning not yet considered alive
         * When start() is called == Thread is considered alive
         * Thread is considered death, after the run() completes
         * */
        t.start();
        t1.start();
        t2.start();

        /*
         * What happens after you call start()?
         * * new thread of execution starts (with a new call stack)
         * * thread moves from the 'new state' to the 'runnable state'
         * * when the thread gets a change to execute, its target run() method will run
         *
         * You start a Thread NOT a Runnable!
         * you call start() on a Thread instance NOT on a Runnable instance
         * */
    }
}
/*
 * Thread == the worker
 * Runnable == job to be done
 * */