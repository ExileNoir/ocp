package chapter10.threads.instantiatingAndStarting;

public class ManyNames {
    public static void main(String[] args) throws InterruptedException {

        // Make 1 Runnable
        final Runnable r = () -> {
            for (int x = 1; x <= 20; x++) {
                System.out.println("Run by " + Thread.currentThread().getName() + " , x is " + x);
            }
        };

        final Thread one = new Thread(r);
        final Thread two = new Thread(r);
        final Thread three = new Thread(r);

        one.setName("Steven");
        two.setName("Salomé");
        three.setName("Constance");

        one.start();
//        one.join();

        two.start();
//        two.join();

        three.start();
        three.join();

//        one.start();        // -> IllegalThreadStateException !!
    }
}
/*
 * Just because a series of threads are started in a particular order doesn't mean they'll run in that order
 *
 * Within each thread, things will happen in a predictable order.
 * BUT the actions of different threads can mix in unpredictable ways.
 *
 * join() => Guaranteed to cause the Current Thread to stop executing UNTIL the thread it joins with completes || afters specified duration.
 *
 * Once a Thread has been Started, it can NEVER be started again
 * */
