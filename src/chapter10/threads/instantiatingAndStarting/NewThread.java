package chapter10.threads.instantiatingAndStarting;

import static java.lang.System.out;

/* Implementing Runnable */
public class NewThread implements Runnable {
    // Create a second Thread

    private Thread t;

    NewThread() {
        // Create a new, Second Thread
        t = new Thread(this, "Demo Thread");  // this == want the new Thread to call the run() on this NewThread object
        out.println("Child Thread: " + t);
        t.start();      // Start the Thread
    }

    // This is entry point for the second Thread
    @Override
    public void run() {
        try {
            for (int i = 5; i > 0; i--) {
                out.println("Child Thread: " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException ie) {
            out.println("Child Interrupted");
        }
        out.println("Exiting Child Thread.");
    }

    public static void main(String[] args) {
        new NewThread();    // create a new Thread

//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        try {
            for (int i = 5; i > 0; i--) {
                out.println("Main Thread: " + i);
                Thread.sleep(2000);
            }
        } catch (InterruptedException ie) {
            out.println("Main Thread interrupted");
        }
        out.println("Exiting Main Thread");
    }
}
