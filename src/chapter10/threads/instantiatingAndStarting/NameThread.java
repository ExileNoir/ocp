package chapter10.threads.instantiatingAndStarting;

import static java.lang.System.out;

class NameRunnable implements Runnable {

    @Override
    public void run() {
        out.println("NameRunnable running");
        out.println("Run by " + Thread.currentThread().getName());
    }
}

public class NameThread {
    public static void main(String[] args) {
        final NameRunnable nr = new NameRunnable();
        final Thread t = new Thread(nr);

        t.setName("Constance");
        t.start();
    }
}
