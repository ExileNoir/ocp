package chapter10.threads.defining;

import static java.lang.System.out;

public class MyRunnable implements Runnable {

    @Override
    public void run() {
        out.println("Important job running in MyRunnable");
    }

    public static void main(String[] args) {

        // Runnable == functional interface :
        final Runnable r = () -> out.println("Important jub running in a Runnable");
    }
}
/*
 * * Runnable Object:
 * Each task = an instance of the Runnable interface.
 * * Thread
 * An object that facilitates the execution of a task.
 *
 * * A task class must Implement the Runnable interface
 * A task must be run from a thread
 * */