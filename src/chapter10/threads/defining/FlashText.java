package chapter10.threads.defining;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class FlashText extends Application {
    private String text = "";

    // Override the start() in the application class
    @Override
    public void start(final Stage primaryStage) throws Exception {
        final StackPane pane = new StackPane();
        final Label lblText = new Label("Programming is fun");
        pane.getChildren().add(lblText);

        new Thread(() -> {
            try {
                while (true) {      // This text is set && unSet to simulate the a flashing effect
                    if (lblText.getText().trim().length() == 0) {
                        text = "Welcome ExileNoir";
                    } else {
                        text = "";
                    }

                    // to update the text in the label
                    Platform.runLater(() -> lblText.setText(text));     // tells the system to run this Runnable object in the application Thread
                    Thread.sleep(1000);         // to make them switch
                }
            } catch (InterruptedException ie) {
            }
        }).start();

        // Create a scene and place it in the stage
        final Scene scene = new Scene(pane, 200, 50);
        primaryStage.setTitle("FlashText");       // set the stage title
        primaryStage.setScene(scene);            // Place the scene in the stage
        primaryStage.show();                    // Display the stage
    }

    public static void main(String[] args) {
        launch(args);
    }
}
