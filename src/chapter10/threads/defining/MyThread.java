package chapter10.threads.defining;

import static java.lang.System.out;

public class MyThread extends Thread {

    @Override
    public void run() {
        out.println("Important job running in MyThread");
    }

    /* you're free to overload run() in your Thread subclass
     * But know this:
     * overloaded run(String s) will be ignored by the Thread class unless you
     * call it yourself
     */
    public void run(final String s) {
        out.println("String in run is " + s);
    }
}
