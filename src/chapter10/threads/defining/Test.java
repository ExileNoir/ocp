package chapter10.threads.defining;

public class Test implements Runnable {
    public static void main(String[] args) {
        new Test();
    }

    public Test() {
//        Test task = new Test();                   // StackOverFlowError
//        new Thread(task).start();

        Thread t = new Thread(this);        // IllegalThreadStateException
        t.start();
        t.start();
    }

    public void run() {
        System.out.println("test");
    }
}
