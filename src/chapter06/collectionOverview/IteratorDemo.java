package chapter06.collectionOverview;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import static java.lang.System.out;

public class IteratorDemo {
    public static void main(String[] args) {

        /* Create an ArrayList */
        final ArrayList<String> al = new ArrayList<>();

        /* Add elements to the ArrayList */
        al.add("C");
        al.add("A");
        al.add("E");
        al.add("B");
        al.add("D");
        al.add("F");

        out.println("Original Contents of al: " + al);

        /* Use Iterator to display contents of al */
        Iterator<String> itr = al.iterator();

        while (itr.hasNext()) {
            final String element = itr.next();
            out.print(element + " ");
        }
        out.println();

        /* Modify objects being iterated */
        final ListIterator<String> litr = al.listIterator();

        while (litr.hasNext()) {
            final String element = litr.next();
            litr.set(element + " + ");
        }
        out.println("Modified contents of al: " + al);

        itr = al.iterator();    // attention! List is modified so re-instantiate
        while (itr.hasNext()) {


            final String element = itr.next();
            out.print(element);
        }
        out.println();

        /* Now display the list backwards */
        out.print("Modified list backwards: ");
        while (litr.hasPrevious()) {
            final String element = litr.previous();
            out.print(element);
        }

    }
}
