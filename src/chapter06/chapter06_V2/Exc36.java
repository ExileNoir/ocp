package chapter06.chapter06_V2;

import java.io.FileNotFoundException;

class OutOfTuneException extends Exception {
    OutOfTuneException(String message) {
        super(message);
    }
}

public class Exc36 {
    public void play() throws OutOfTuneException, FileNotFoundException {
        throw new OutOfTuneException("Sour note!");
    }

    public static void main(String[] args) throws OutOfTuneException, FileNotFoundException {
        final Exc36 piano = new Exc36();
        try {
            piano.play();
        } catch (Exception e) {
            throw e;
        } finally {
            System.out.println("song finished");
        }
    }
}
