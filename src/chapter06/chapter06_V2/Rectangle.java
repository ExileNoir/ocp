package chapter06.chapter06_V2;

import static java.lang.System.out;

public class Rectangle {
    private int width, height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getArea() {
        assert isValid() : "Not a valid Rectangle";
        return width * height;
    }

    // class invariant Assert
    private boolean isValid() {
        return (width >= 0 && height >= 0);
    }

    public static void main(String[] args) {
        final Rectangle r1 = new Rectangle(5, 12);
        final Rectangle r2 = new Rectangle(-4, 10);
        out.println("Area one = " + r1.getArea());
        out.println("Area two = " + r2.getArea());      // if this is place one line higher up.. Assert is thr immediately
    }
}
