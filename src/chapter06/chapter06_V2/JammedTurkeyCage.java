package chapter06.chapter06_V2;

import static java.lang.System.out;

public class JammedTurkeyCage implements AutoCloseable {
    @Override
    public void close() throws IllegalStateException {
        throw new IllegalStateException("Cage door does not close");
    }


    public static void main(String[] args) {
        try (final JammedTurkeyCage cage = new JammedTurkeyCage()) {
            out.println("put turkey in");
            throw new IllegalStateException("Turkeys ran off NOOOO");
        } catch (IllegalStateException e) {
            out.println("Caught: " + e.getMessage());
            for (final Throwable t : e.getSuppressed()) {
                out.println(t.getMessage());
            }
            e.printStackTrace();
        }
    }
}
