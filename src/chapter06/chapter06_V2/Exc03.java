package chapter06.chapter06_V2;

class LostBallException extends Exception {

}

public class Exc03 {

    public void toss() throws LostBallException {
        throw new ArrayStoreException();
    }

    public static void main(String[] args) {
        try {
            new Exc03().toss();
        } catch (Throwable e) {       // Exception isA Throwable, thus LostBalException isA Throwable
            System.out.println("Caught");
        }

    }
}
