package chapter06.chapter06_V2;

import static java.lang.System.*;

class SpellingException extends RuntimeException {
}

public class Exc13 {
    public static void main(String[] args) {

        try {
            if (!"cat".equals("kat")) {
                new SpellingException();
            }
        } catch (SpellingException | NullPointerException e) {
            out.println("Spelling Problem");
        } catch (Exception e) {
            out.println("Unknown problem");
        } finally {
            out.println("Done Biatch");
        }
    }
}
