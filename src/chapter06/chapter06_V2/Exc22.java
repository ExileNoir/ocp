package chapter06.chapter06_V2;

public class Exc22 {
    public static void main(String[] args) {

        try (final AutoCloseable weatherTracker = new AutoCloseable() {
            public void close() throws RuntimeException {
            }
        }) {
            System.out.println(weatherTracker.toString());
        } catch (Exception e) {
//            if(weatherTracker != null)weatherTracker.close();
        } finally {
            System.out.println("Storm gone");
        }
    }
}
