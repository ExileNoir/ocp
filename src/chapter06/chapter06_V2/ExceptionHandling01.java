package chapter06.chapter06_V2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import static java.lang.System.out;

public class ExceptionHandling01 {
    public static void main(String[] args) {

        try {
            final Path path = Paths.get("dolphinsBorn.txt");
            final String txt = new String(Files.readAllBytes(path));
            final LocalDate date = LocalDate.parse(txt);
            out.println(date);
        } catch (DateTimeParseException /*| SQLException*/ | ArrayIndexOutOfBoundsException | IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

}
