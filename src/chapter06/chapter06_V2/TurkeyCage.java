package chapter06.chapter06_V2;

import static java.lang.System.*;

public class TurkeyCage implements AutoCloseable {
    @Override
    public void close() {
        out.println("Close gate");
    }

    public static void main(String[] args) {
        try (final TurkeyCage cage = new TurkeyCage()) {
            out.println("put Turkey in");
        }
    }
}
