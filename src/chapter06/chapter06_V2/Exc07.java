package chapter06.chapter06_V2;


class WhaleSharkException extends  Exception{
    public WhaleSharkException(){
        super("Friendly Shark");
    }

    public WhaleSharkException(String message){
        super(new Exception(new WhaleSharkException()));
    }

    public WhaleSharkException(Exception cause){

    }
}

public class Exc07 {
}
