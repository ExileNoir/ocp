package chapter06.chapter06_V2;

class Fetch{
     int play(String dogName)throws Exception{
        try {
            throw new RuntimeException(dogName);
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }
}

public class Exc27 {
    public static void main(String[] args)throws RuntimeException, Exception {
        new Fetch().play("Webby");
        new Fetch().play("Georgett");
    }
}
