package chapter06.chapter06_V2;

public class TestSeasons {
    public enum Seasons {
        SPRING,SUMMER,FALL, WINTER;
    }

    public static void test(TestSeasons.Seasons s){
        switch (s){
            case SPRING:
            case FALL:
                System.out.println("shorter hours");break;
            case SUMMER:
                System.out.println("Longer hours");break;
            default:
                assert false: "Invalid season";
        }
    }

    public static void main(String[] args) {
       test(Seasons.WINTER);
    }
}
