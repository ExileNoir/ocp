package chapter06.chapter06_V2;

import static java.lang.System.out;

public class SuppressedTurkey01 {
    public static void main(String[] args) {

        try (final JammedTurkeyCage t1 = new JammedTurkeyCage();
             final JammedTurkeyCage t2 = new JammedTurkeyCage()) {
            out.println("Turkeys entered cage...");
            throw new IllegalStateException("NOOOO turkeys are running wild out of golden cage");
        } catch (IllegalStateException e) {
            out.println("caught: " + e.getMessage());

            for (final Throwable t : e.getSuppressed()) {
                out.println(t.getMessage());
            }

            throw new RuntimeException("Did we catch my golden duck");      // not seen if finally has exc
        } finally {
            throw new RuntimeException("and we couldn't find my golden turkey");  // not suppressed
        }
    }
}
