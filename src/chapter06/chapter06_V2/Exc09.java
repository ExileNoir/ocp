package chapter06.chapter06_V2;

import java.io.IOException;

class Light {
    void turnOn() throws IOException {
        new IOException("Not Ready");
    }
}


public class Exc09 {
    public static void main(String[] args) throws Exception {

        try {
            new Light().turnOn();
        } catch (RuntimeException e) {
            System.out.println(e);
            throw new IOException();
        } finally {
            System.out.println("complete");
        }
    }
}
