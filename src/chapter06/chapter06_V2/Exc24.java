package chapter06.chapter06_V2;

import java.io.Closeable;

class ReadSign implements Closeable {
    public void close() {
    }

    public String get() {
        return "Hello";
    }
}

class MakeSign implements AutoCloseable {
    public void close() {
    }

    public void send(String message) {
        System.out.println(message);
    }
}

public class Exc24 {
    public static void main(String[] args) {
        try (ReadSign r = new ReadSign();
             MakeSign w = new MakeSign();) {
            w.send(r.get());
        }
    }
}
