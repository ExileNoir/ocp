package chapter06.chapter06_V2;

import static java.lang.System.*;

class MissingMoneyException extends Exception {
}

class MissingFoodException extends Exception {
}

public class Exc14 {
    public void doIHaveProblem() throws MissingMoneyException, MissingFoodException {
        out.println("No Problemo");
    }


    public static void main(String[] args) throws /*Exception*/ MissingMoneyException, MissingFoodException {
        try {
            final Exc14 problem = new Exc14();
            problem.doIHaveProblem();
        } catch (Exception e) {
            throw e;
        }
    }
}
