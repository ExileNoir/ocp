package chapter06.chapter06_V2;

import com.sun.xml.internal.ws.Closeable;

public class Exc11 {
    class Printer implements Closeable {
        public void print() {
            System.out.println("This just in!");
        }

        public void close() {
        }
    }

    public void printHeadLines() {
        try (Printer p = new Printer()) {
            p.print();
        }
    }

    public static void main(String[] args) {
        new Exc11().printHeadLines();
    }
}
