package chapter06.usingCollections.sortingCollectionsAndArrays.usingSets;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import static java.lang.System.out;

public class HashSetDemo {
    public static void main(String[] args) {

        /* Create a HashSet */
        final HashSet<String> hs = new HashSet<>();

        /* Add elements to the HashSet */
        hs.add("Beta");
        hs.add("Alpha");
        hs.add("Eta");
        hs.add("Gamma");
        hs.add("Epsilon");
        hs.add("Beta"); // no adding duplicates == returns false boolean
        hs.add("Omega");

        /* HashSet != Ordered */
        out.println(hs);

        /* Create a LinkedHashSet */
        final Set<String> lhs = new LinkedHashSet<>();

        /* Add elements to the LinkedHashSet */
        lhs.add("Beta");
        lhs.add("Alpha");
        lhs.add("Eta");
        lhs.add("Gamma");
        lhs.add("Epsilon");
        lhs.add("Beta"); // no adding duplicates == returns false boolean
        lhs.add("Omega");

        /* LinkedHashSet == Ordered by Insertion Order */
        out.println(lhs);
    }
}
