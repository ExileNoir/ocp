package chapter06.usingCollections.sortingCollectionsAndArrays.usingSets;

import java.util.TreeSet;

import static java.lang.System.out;

public class TreeSetDemo {
    public static void main(String[] args) {

        /* Create a TreeSet */
        final TreeSet<String> ts = new TreeSet<>();

        /* Add elements to the TreeSet */
        ts.add("C");
        ts.add("A");
        ts.add("B");
        ts.add("E");
        ts.add("F");
        ts.add("D");
        ts.add("A");    // no duplicates allowed

        out.println(ts);
    }
}
