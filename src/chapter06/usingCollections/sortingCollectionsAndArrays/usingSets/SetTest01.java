package chapter06.usingCollections.sortingCollectionsAndArrays.usingSets;

import java.util.Set;
import java.util.TreeSet;

import static java.lang.System.out;

public class SetTest01 {
    public static void main(String[] args) {

        final boolean[] ba = new boolean[5];
        // insert code here
        final Set s = new TreeSet();


        ba[0] = s.add("a");
        ba[1] = s.add(new Integer(42));  // ClassCastExp =>
        ba[2] = s.add("b");             // want a collection to be sorted! elements must be mutually comparable!!
        ba[3] = s.add("a");
        ba[4] = s.add(new Object());
        for (int x = 0; x < ba.length; x++) {
            out.print(ba[x] + " ");
        }
        out.println();

        for (final Object o : s) {
            out.print(o + " ");
        }
    }
}
