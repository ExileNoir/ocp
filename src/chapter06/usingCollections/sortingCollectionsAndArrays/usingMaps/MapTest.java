package chapter06.usingCollections.sortingCollectionsAndArrays.usingMaps;

import java.util.HashMap;
import java.util.Map;

import static java.lang.System.out;

class Dog {

    String name;

    Dog(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        return ((o instanceof Dog) && (((Dog) o).name == name));
    }

    @Override
    public int hashCode() {
        return name.length();
    }

    enum Pets {
        DOG("Dog"),
        CAT("Cat"),
        HORSE("Horse");

        String name;

        Pets(String n) {
            name = n;
        }
    }
}

class Cat {
}

public class MapTest {
    public static void main(String[] args) {
        final Map<Object, Object> map = new HashMap<>();

        map.put("k1", new Dog("Ceasar"));
        out.println(map.get("k1"));     // will give the class
        out.println(((Dog) map.get("k1")).name);

        map.put("k2", Dog.Pets.DOG);
        final String k2 = "k2";
        out.println("\n" + map.get(k2));
        out.println(((Dog.Pets) map.get(k2)).name);

        map.put(Dog.Pets.CAT, "CAT Key enum");
        final Dog.Pets p = Dog.Pets.CAT;
        out.println("\n" + map.get(p));

        final Dog d1 = new Dog("Magnolia");
        map.put(d1, "Dog Key");
        out.println("\n" + map.get(d1));

        map.put(new Cat(), "Cat Key");
        out.println("\n" + map.get(new Cat()));

        map.put(new Dog("Jules"), "Jules");
        out.println("\n" + map.get(new Dog("Jules")));

        out.println("\n" + map.size() + "\n");


        for (final Object o : map.keySet()) {
            if (o instanceof String)
                out.println(o);
            if (o instanceof Dog.Pets)
                out.println(((Dog.Pets) o).name);
            if (o instanceof Dog)
                out.println(((Dog) o).name);
            if (o instanceof Cat)
                out.println("MIAAAAAAUWWWWkes");
        }
    }
}
