package chapter06.usingCollections.sortingCollectionsAndArrays.ComparableAndComparator;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static java.lang.System.out;

public class HomeAppDVD {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        new HomeAppDVD().go();
    }

    private final String dvdInfoTxt = "dvdInfo.txt";
    private final ArrayList<DVDInfo> dvdList = new ArrayList<>();

    private void go() throws IOException, ClassNotFoundException {
        final DVDInfo one = new DVDInfo("Donnie Darko", "Sci-fi", "Gyllenhall, Jake");
        final DVDInfo two = new DVDInfo("Raiders of the Lost Ark", "Action", "Ford, Harrison");
        final DVDInfo three = new DVDInfo("2001", "Sci-fi", "??");
        final DVDInfo four = new DVDInfo("Star Wars", "Sci-fi", "Ford, Harrison");
        final DVDInfo five = new DVDInfo("Lost in Translation", "Comedy", "Murray, Bill");
        final DVDInfo six = new DVDInfo("Patriot Games", "Action", "Ford Harrison");
        final DVDInfo seven = new DVDInfo("CaddyShack", "Comedy", "Murray, Bill");
        final DVDInfo eight = new DVDInfo("Fear and Loathing Las Vegas", "Comedy", "Johnny Depp");

        writeToFile(one, two, three, four, five, six, seven, eight);

        populate(dvdList);
        out.println("Printed as input");
        out.println(dvdList);

        // Sorted by Title via << Comparable >> compareTo()
        Collections.sort(dvdList);
        out.println("Printed by Title\n" + dvdList);

        // Sorted by Genre via << Comparator >> compare()
//        Collections.sort(dvdList, new GenreSort());       // if we don't use lambda we use class with << Comparator >>

        // Sorting using lambda function
        Collections.sort(dvdList, (g1, g2) -> g1.getGenre().compareTo(g2.getGenre()));
        out.println("Printed by Genre\n" + dvdList);

        Collections.sort(dvdList, Comparator.comparing(DVDInfo::getLeadActor));

        out.println("Printed by leadActor\n" + dvdList);

        // Sorting an ArrayList using the sort()
        dvdList.sort((g1, g2) -> g1.getLeadActor().compareTo(g2.getLeadActor()));
        out.println("Printed by leadActor\n" + dvdList);



        /* Anonymous InnerClass */
        final Comparator<DVDInfo> genreSort = new Comparator<DVDInfo>() {
            @Override
            public int compare(DVDInfo dvd1, DVDInfo dvd2) {
                return dvd1.getGenre().compareTo(dvd2.getGenre());
            }
        };
        dvdList.sort(genreSort);

        /* replacing the Anonymous InnerClass */
        final Comparator<DVDInfo> genreSort1 = (dvd1, dvd2) -> dvd1.getGenre().compareTo(dvd2.getGenre());
        dvdList.sort(genreSort1);

    }

    private void writeToFile(final DVDInfo... one) throws IOException {
        try (final FileOutputStream fos = new FileOutputStream(dvdInfoTxt);
             final ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            for (final DVDInfo dvdInfo : one) {
                oos.writeObject(dvdInfo);
            }
        }
    }

    private void populate(final ArrayList<DVDInfo> dvdList) throws IOException, ClassNotFoundException {
        try (final FileInputStream fis = new FileInputStream(dvdInfoTxt);
             final ObjectInputStream ois = new ObjectInputStream(fis)) {
            while (fis.available() > 0) {
                final DVDInfo movie = (DVDInfo) ois.readObject();
                dvdList.add(movie);
            }
        }
    }
}

/* for << Comparable >> = modify the class whose instances you want to sort */
class DVDInfo implements Serializable, Comparable<DVDInfo> {

    private String title;
    private String genre;
    private String leadActor;

    DVDInfo(String t, String g, String a) {
        title = t;
        genre = g;
        leadActor = a;
    }

    @Override
    public int compareTo(DVDInfo o) {
        return title.compareTo(o.getTitle());
    }

    public String toString() {
        return title + " " + genre + " " + leadActor + "\n";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getLeadActor() {
        return leadActor;
    }

    public void setLeadActor(String leadActor) {
        this.leadActor = leadActor;
    }

}

/* for << Comparator >> = Build a class separate from the class whose instances you want to sort */
// or delete this class and directly use the lambda expression as << Comparator >> == Functional Interface
class GenreSort implements Comparator<DVDInfo> {
    @Override
    public int compare(DVDInfo o1, DVDInfo o2) {
        return o1.getGenre().compareTo(o2.getGenre());
    }
}
