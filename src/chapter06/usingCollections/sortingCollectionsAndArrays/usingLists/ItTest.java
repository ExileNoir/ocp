package chapter06.usingCollections.sortingCollectionsAndArrays.usingLists;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static java.lang.System.out;

class Dog {
    String name;

    Dog(String n) {
        name = n;
    }
}

public class ItTest {
    public static void main(String[] args) {
        final List<Dog> listDogs = new ArrayList<>();

        final Dog d1 = new Dog("Ceasar");

        listDogs.add(d1);
        listDogs.add(new Dog("Clover"));
        listDogs.add(new Dog("Magnolia"));

        final Iterator<Dog> i3 = listDogs.iterator();       // make an Iterator
        while (i3.hasNext()) {
            final Dog d2 = i3.next();                      // cast not required
            out.println(d2.name);
        }
        out.println("size: " + listDogs.size());
        out.println("get1:" + listDogs.get(1).name);      // it is an object inside with no override toString so .name is necessary
        out.println("aiko: " + listDogs.indexOf(d1));

        listDogs.remove(2);

        final Iterator i4 = listDogs.iterator();    // if not using generic syntax of type "Dog" we need to cas the << next >>
        while (i4.hasNext()) {
            final Dog d3 = (Dog) i4.next();       // cannot use the i3 second time
            out.println(d3.name);
        }

        final Object[] oa = listDogs.toArray();
        for (final Object o : oa) {
            out.println(((Dog) o).name);     // cast needed
        }
    }
}
