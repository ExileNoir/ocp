package chapter06.usingCollections.sortingCollectionsAndArrays.usingLists;

import java.util.LinkedList;

import static java.lang.System.out;

public class LinkedListDemo {
    public static void main(String[] args) {

        /* Create a LinkedList */
        final LinkedList<String> ll = new LinkedList<>();
        // We aware when setting reference var List/Deque/Queue for implementing methods

        /* Add elements to the LinkedList */
        ll.add("F");        // Collection Interface (List/Queue)
        ll.add("B");
        ll.add("D");
        ll.add("E");
        ll.add("C");
        ll.addLast("Z");                 // Deque Interface
        ll.addFirst("A");               // Deque Interface
        ll.add(1, "A2");    // List Interface

        ll.offerFirst("AA");        // Deque Interface
        ll.push("AAA");
        ll.offer("ZZ");            // Queue Interface

        out.println("Original content of ll: " + ll);

        /* Remove elements form the LinkedList */
        final boolean rOne = ll.remove("F");
        final String rTwo = ll.remove(2);   // List Interface

        out.println("Contents of ll after deletion: " + ll);

        /* Remove first and last elements */
        ll.removeFirst();   // Deque Interface
        ll.removeLast();

        out.println("Contents of ll after deleting first and last elements: " + ll);

        /* Get and Set a value */
        final String val = ll.get(2);
        ll.set(2, val + " Changed");

        out.println("ll after change val " + ll);
    }
}
