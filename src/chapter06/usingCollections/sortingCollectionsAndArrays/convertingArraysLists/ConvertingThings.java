package chapter06.usingCollections.sortingCollectionsAndArrays.convertingArraysLists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;

public class ConvertingThings {
    public static void main(String[] args) {

        final String[] sa = {"one", "two", "three", "four"};
        final List sList = Arrays.asList(sa);   // make a List

        out.println("size: " + sList.size());
        out.println("idx2: " + sList.get(2));
        sList.set(3, "six");
        sa[1] = "five";
        for (final String s : sa) {
            out.print(s + " ");
        }
        out.println("\n" + sList);
        out.println("s[1]: " + sList.get(1));
//        sList.add("three");          // RuntimeEx == UnsupportedOperationException
        // List is Fixed with size of Array


        final List<Integer> iL = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            iL.add(i);
        }
        out.println(iL);

        final Object[] oa = iL.toArray();       // create an Object Array
        Integer[] ia2 = new Integer[3];
        ia2 = iL.toArray(ia2);                // create an Integer Array

    }
}
