package chapter06.usingCollections.sortingCollectionsAndArrays.convertingArraysLists;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

public class ArrayListToArray {
    public static void main(String[] args) {

        /* Create an arrayList */
        final List<Integer> al = new ArrayList<>();

        /* Add elements to the arrayList */
        al.add(1);      // Autoboxing to Integers
        al.add(2);
        al.add(3);
        al.add(4);

        out.println("Contents of al: " + al);

        /* Get the Array */
        final Integer ia[] = al.toArray(new Integer[al.size()]);
//        Integer ia[] = new Integer[al.size()];
//        ia = al.toArray(ia);

        int sum = 0;

        // Sum the Array
        for (final int i : ia) {
            sum += i;
        }
        out.println("Sum is: " + sum);

    }
}
