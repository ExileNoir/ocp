package chapter06.usingCollections.sortingCollectionsAndArrays.binarySearch;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import static java.lang.System.out;

public class SearchObjArray {
    public static void main(String[] args) {

        final String[] sa = {"one", "two", "three", "four"};

        Arrays.sort(sa);
        for (final String s : sa) {
            out.print(s + " ");
        }
        out.println("\none = " + Arrays.binarySearch(sa, "one"));

        out.println("\nnow reverse sort");
        Arrays.sort(sa, Collections.reverseOrder());
        for (final String s : sa) {
            out.print(s + " ");
        }
        out.println("\none= " + Arrays.binarySearch(sa, "one"));    // does not give correct
        out.println("one= " + Arrays.binarySearch(sa, "one", Collections.reverseOrder()));

        Arrays.sort(sa, new ReSortComparator());
        for (final String s : sa) {
            out.print(s + " ");
        }
        out.println("\none= " + Arrays.binarySearch(sa, "one"));        // does not give correct
        out.println("one= " + Arrays.binarySearch(sa, "one", new ReSortComparator()));
    }

    static class ReSortComparator implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            return o2.compareTo(o1);
        }
    }
}
