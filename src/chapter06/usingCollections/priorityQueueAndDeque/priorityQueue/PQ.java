package chapter06.usingCollections.priorityQueueAndDeque.priorityQueue;

import java.util.Comparator;
import java.util.PriorityQueue;

import static java.lang.System.out;

public class PQ {

    static class PQsort implements Comparator<Integer> {  // inverse sort
        @Override
        public int compare(Integer one, Integer two) {
            return two - one;   // unboxing
        }
    }

    public static void main(String[] args) {

        final int[] ia = {1, 5, 3, 7, 6, 9, 8};                      // unordered date
        final PriorityQueue<Integer> pq1 = new PriorityQueue<>();   // use natural order

        for (final int x : ia)
            pq1.offer(x);                    // load queue  || add elements to the queue

        for (final int x : ia)
            out.print(pq1.poll() + " ");  // review queue && removes elements from the queue!!
        out.println();

        final PQsort pqs = new PQsort();
        final PriorityQueue<Integer> pq2 = new PriorityQueue<>(10, pqs);     // use Comparator
        final PriorityQueue<Integer> pq3 = new PriorityQueue<>(10, (one, two) -> two - one);  // using lambda

        for (final int x : ia)      // load queue || add elements to the queue
            pq2.offer(x);

//        for(final int x : ia)
//            out.print(pq2.poll()+" ");
//        out.println();

        out.println("size: " + pq2.size());
        out.println("peek: " + pq2.peek());     // Retrieves, but does not remove, the head of this queue, || returns null if this queue is empty
        out.println("size: " + pq2.size());
        out.println("poll: " + pq2.poll());    // Retrieves and removes the head of this queue, or returns null if this queue is empty
        out.println("size: " + pq2.size());

        for (final int x : ia)
            out.print(pq2.poll() + " ");        // review queue && removes elements from the queue!!

        out.println("\nsize: " + pq2.size());
    }
}
