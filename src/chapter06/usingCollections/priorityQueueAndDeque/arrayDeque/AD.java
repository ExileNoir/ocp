package chapter06.usingCollections.priorityQueueAndDeque.arrayDeque;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;

public class AD {
    public static void main(String[] args) {

        final List<Integer> nums = Arrays.asList(10, 9, 8, 7, 6, 5);
        // create several ArrayDeques, each with space for 2 items;

        final ArrayDeque<Integer> a = new ArrayDeque<>(2);
        final ArrayDeque<Integer> b = new ArrayDeque<>(2);
        final ArrayDeque<Integer> c = new ArrayDeque<>(2);
        final ArrayDeque<Integer> d = new ArrayDeque<>(2);
        final ArrayDeque<Integer> e = new ArrayDeque<>(2);

        // add 6 items to each Deque, each using different methods
        for (final Integer n : nums) {
            a.offer(n);          // add  element to the END of this Deque
            b.offerFirst(n);    // add element to the FRONT
            c.push(n);         // add element to the FRONT
            d.add(n);         // add element to the END
            e.addFirst(n);   // add element to the FRONT
        }

        // display the deques
        out.println("a: " + a);
        out.println("b: " + b);
        out.println("c: " + c);
        out.println("d: " + d);
        out.println("e: " + e);

        out.println("peek: returns FIRST element of e: " + e.peek());

        out.println("poll: REMOVES FIRST element and returns it: " + e.poll());
        out.println("e has been modified: " + e);

        out.println("pop: REMOVES FIRST element and returns it: " + e.pop());
        out.println("e has been modified: " + e);

        out.println("pollLast: REMOVES LAST element and returns it: " + e.pollLast());
        out.println("e has been modified: " + e);

        out.println("removeLast: REMOVES LAST element an returns it: " + e.removeLast() + " " + e.removeLast());
        out.println("e has been modified: " + e);
    }
}
/*
 * on an empty Deque we use following methods:
 * ** pop() || remove() ==> NoSuchElementException
 *
 * ** poll() ==> null
 *
 * */