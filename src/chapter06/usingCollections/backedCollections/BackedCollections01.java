package chapter06.usingCollections.backedCollections;

import java.util.SortedMap;
import java.util.TreeMap;

import static java.lang.System.out;

public class BackedCollections01 {
    public static void main(String[] args) {

        final TreeMap<String, String> map = new TreeMap<>();        // Sorted
        map.put("a", "ant");
        map.put("d", "dog");
        map.put("h", "horse");

        final SortedMap<String, String> subMap = map.subMap("b", "g");    // 1 Create a << BACKED COLLECTION >>

        out.println("Map: " + map + "\nSubmap: " + subMap);             // 2 Show content

        map.put("b", "Batman");                      // 3 add to original
        subMap.put("f", "fishStick");               // 4 add to copy

        out.println("\nMap: " + map + "\nSubMap: " + subMap);

        map.put("r", "raccoon");         // 5 add to original - out of range
//        subMap.put("p","piglet");     // 6 add to copy - out of range (G) ==> IllegalArgumentException: KEY OUT OF RANGE

        out.println("\nMap: " + map + "\nSubMap: " + subMap);

        out.println("\nceilingKey: " + map.ceilingKey("b"));   // returns key GREATER than || EQUAL to the given key
        out.println("higherKey: " + map.higherKey("b"));      // returns key GREATER than the given key

        out.println("floorKey: " + map.floorKey("s"));       // returns key LESSER than || EQUAL to the given key
        out.println("lowerKey: " + map.lowerKey("r"));      // returns key LESSER than the given key

    }
}
