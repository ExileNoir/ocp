package chapter06.usingCollections;

public class Tester {
    public static void main(String[] args) {
        doStuff(x); // throws NullPointerEx
    }

    static Integer x;

    static void doStuff(int z){
        int z2 = 5;
        System.out.println(z2);
    }
}
