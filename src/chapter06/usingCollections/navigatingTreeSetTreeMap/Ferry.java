package chapter06.usingCollections.navigatingTreeSetTreeMap;

import java.util.NavigableSet;
import java.util.TreeSet;

import static java.lang.System.out;

public class Ferry {
    public static void main(String[] args) {
        final TreeSet<Integer> times = new TreeSet<>();
        times.add(1205);    // add some departure times
        times.add(1505);
        times.add(1545);
        times.add(1830);
        times.add(2010);
        times.add(2100);

        // java v5
        final TreeSet<Integer> subset = (TreeSet) times.headSet(1600); // creates set for all Int < 1600
        out.println("J5 - last before 4pm is: " + subset.last());

        final TreeSet<Integer> sub2 = (TreeSet) times.tailSet(2000);
        out.println("J5 - first after 8pm is : " + sub2.first());


        // java v6 using the new lower() and higher() methods
        out.println("J6 - last before 4 pm is: " + times.lower(1600));   // returns the element LESS than given element
        out.println("J6 - first after 8pm is : " + times.higher(2000)); // returns the element GREATER than given element

        out.println("J6 - floor: " + times.floor(1600));       // returns the element LESS than || EQUAL to the given element
        out.println("J6 - ceiling: " + times.ceiling(2000));  // returns the element GREATER than || EQUAL to the given element

        out.println();
        out.println(times.pollLast());                   // retrieves && removes the last Entry
        for (final Integer time : times)
            out.print(time + " ");

        out.println();
        final Integer first = times.pollFirst();      // retrievers &&
        // removes the first Entry
        out.println(first);
        for (final Integer time : times)
            out.print(time + " ");

        out.println();
        final NavigableSet<Integer> reverseTimes = times.descendingSet();   // returns a << NavigableSet >> in reverse order
        for (final Integer time : reverseTimes)
            out.print(time + " ");


    }
}
