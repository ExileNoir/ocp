package chapter06.usingCollections;

import java.util.TreeSet;

import static java.lang.System.out;

public class SubTreeSet {
    public static void main(String[] args) {

        TreeSet<Integer> s = new TreeSet<>();
        TreeSet<Integer> sub = new TreeSet<>();

        for (int i = 324; i <= 328; i++) {
            s.add(i);
        }

        sub = (TreeSet) s.subSet(326, true, 328, true);
//        sub.add(329);       // IllegalArgumentException
        s.add(329);
        out.println(s + " " + sub);
    }
}
