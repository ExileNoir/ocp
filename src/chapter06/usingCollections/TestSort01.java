package chapter06.usingCollections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.System.out;

public class TestSort01 {
    public static void main(String[] args) {
        final ArrayList<String> stuff = new ArrayList<>();
        stuff.add("Denver");
        stuff.add("Boulder");
        stuff.add("Vail");
        stuff.add("Aspen");
        stuff.add("Telluride");

        out.println("Unsorted: "+stuff);

        Collections.sort(stuff);
        out.println("Sorted: "+stuff);
    }
}
