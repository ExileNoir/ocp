package chapter06.genericTypes.genericMethods;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

abstract class Animal {
    public abstract void checkup();
}

class Dog extends Animal {
    @Override
    public void checkup() {
        out.println("Dog checkup");
    }
}

class Cat extends Animal {
    @Override
    public void checkup() {
        out.println("Cat checkup");
    }
}

class Bird extends Animal {
    @Override
    public void checkup() {
        out.println("Bird checkup");
    }
}

public class AnimalDoctor {
    public void checkAnimal(final Animal animal) {
        animal.checkup();
    }

    public void checkAnimals(final Animal[] animals) {
        for (final Animal animal : animals) {
            animal.checkup();
        }
    }

    public void checkAnimalsBAD(final List<Animal> animals) {
        for (final Animal animal : animals) {
            animal.checkup();
        }
    }

    public void checkAnimalsGOOD(final List<? extends Animal> animals) {
        for (final Animal animal : animals) {
            animal.checkup();
        }
    }

    public void foo() {
        final Dog[] dogs = {new Dog(), new Dog()};
//        final Cat[] cats = {new Cat(), new Cat(), new Cat()};  // ArrayStoreException

        addAnimal(dogs);        // Adds a Dog but takes an argument of type Animal
        checkAnimals(dogs);
    }

    private void addAnimal(final Animal[] animals) {
        animals[0] = new Dog();
    }

    public void addAnimal(final List<? super Dog> animals) {
        animals.add(new Dog());
    }

    public static void main(String[] args) {

        final Dog[] dogs = {new Dog(), new Dog()};
        final Cat[] cats = {new Cat(), new Cat(), new Cat()};
        final Bird[] birds = {new Bird()};

        final AnimalDoctor doctor = new AnimalDoctor();
        doctor.checkAnimals(dogs);
        doctor.checkAnimals(cats);
        doctor.checkAnimals(birds);

        final ArrayList<Dog> listDogs = new ArrayList<>();
        listDogs.add(new Dog());
        listDogs.add(new Dog());
        final ArrayList<Animal> listAnimals = new ArrayList<>();
        listAnimals.add(new Dog());
        listAnimals.add(new Dog());

        out.println("bad");
        doctor.checkAnimalsBAD(listAnimals);        // cannot take listDogs as it requires Collection of Animals
        out.println("good");
        doctor.checkAnimalsGOOD(listDogs);          // accepts due to <<?>> but Cannot ADD!

        doctor.addAnimal(listAnimals);
        doctor.checkAnimalsGOOD(listAnimals);

        out.println();
        doctor.foo();
    }

}
