package chapter06.genericTypes.genericMethods;

import java.util.ArrayList;
import java.util.List;

public class GenericMethodMaking {

    public <T> void makeArrayList(final T t) {     // take an object of an unknown type and use a <<T>> to represent the type
        final List<T> list = new ArrayList<>();     // now we can create the list by using <<T>>
        list.add(t);
    }

    /* Same as preceding code, but with a known type */
    public void makeArrayList(final Dog t) {
        final List<Dog> list = new ArrayList<>();
        list.add(t);
    }
}


