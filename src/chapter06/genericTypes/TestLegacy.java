package chapter06.genericTypes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static java.lang.System.out;

public class TestLegacy {
    public static void main(String[] args) {

        final List<Integer> myList = new ArrayList<>(); // type safe collection
        myList.add(4);
        myList.add(6);

        int total = new Adder().addAll(myList);  // pass it to an untyped argument

        out.println(total);
    }
}

class Adder {
    int addAll(final List list) {
        /*
         * method with a non-generic List argument
         * but assumes (with NO guarantee) that it will be Integers
         * */

        final Iterator it = list.iterator();
        int total = 0;
        while (it.hasNext()) {
            int i = ((Integer) it.next()).intValue();
            total += i;
        }
        return total;
    }
}

/*
 * You can mix correct generic code with older non-generic code
 */