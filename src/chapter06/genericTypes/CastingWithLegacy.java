package chapter06.genericTypes;

import java.util.ArrayList;
import java.util.List;

public class CastingWithLegacy {
    public static void main(String[] args) {

        final List test = new ArrayList();
        test.add(43);
        int x = (Integer) test.get(0);  // Casting needed!! get returns Object != Integer

        final List<Integer> test01 = new ArrayList<>();
        test01.add(42);
        int x01 = test01.get(0);    // type-safe no cast needed

    }
}
