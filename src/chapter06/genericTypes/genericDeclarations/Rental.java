package chapter06.genericTypes.genericDeclarations;

import java.util.List;

import static java.lang.System.out;

class Car {
    private String name;

    @Override
    public String toString() {
        return name;
    }
}

public class Rental {
    private List rentalPool;
    private int maxNum;

    public Rental(int maxNum, List rentalPool) {
        this.maxNum = maxNum;
        this.rentalPool = rentalPool;
    }

    public Object getRental() {
        // blocks until there's something available
        return rentalPool.get(0);
    }

    public void returnRental(final Object o) {
        rentalPool.add(o);
    }
}

class CarRental extends Rental {

    public CarRental(int maxNum, List rentalPool) {
        super(maxNum, rentalPool);
    }

    public Car getRental() {
        return (Car) super.getRental();
    }

    public void returnRental(final Car c) {
        super.returnRental(c);
    }

    public void returnRental(final Object o) {
        if (o instanceof Car) {
            super.returnRental(o);
        } else {
            out.println("Cannot add a non-Car");
            // probably throw exception
        }
    }
}
