package chapter06.genericTypes.genericDeclarations;

import java.util.ArrayList;
import java.util.List;

public class RentalGeneric<T> {     // <<T>> is for the Type parameter

    private List<T> rentalPool;     // use the class type for the List type
    private int maxNum;

    public RentalGeneric(int maxNum, List<T> rentalPool) {    // Constructor takes a List of the class type
        this.maxNum = maxNum;
        this.rentalPool = rentalPool;
    }

    public T getRental() {       // we rent out a T
        // blocks until there's something available
        return rentalPool.get(0);
    }

    public void returnRental(final T returnedThing) {    // and the renter returns T
        rentalPool.add(returnedThing);
    }

    public static void main(String[] args) {
        // we make some cars for the pool
        final Car c1 = new Car();
        final Car c2 = new Car();

        final List<Car> carList = new ArrayList<>();
        carList.add(c1);
        carList.add(c2);

        final RentalGeneric<Car> carRental = new RentalGeneric<>(2, carList);

        // now get a car out, and it won't need a cast
        final Car carToRent = carRental.getRental();
        carRental.returnRental(carToRent);

        // can we stick something else in the original carList?
//        carList.add("hello");

    }
}
