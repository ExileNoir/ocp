package chapter06.genericTypes.genericsExample;

import static java.lang.System.out;

/*
 * A simple Generic class with two type parameters: T and V
 * */
class TwoGen<T, V> {

    T ob1;
    V ob2;

    TwoGen(T o1, V o2) {
        this.ob1 = o1;
        this.ob2 = o2;
    }

    void showTypes() {
        out.println("Type of T is: " + ob1.getClass().getName() + "\n" +
                "Type of V is: " + ob2.getClass().getName());
    }

    T getOb1() {
        return ob1;
    }

    V getOb2() {
        return ob2;
    }
}

public class TwoGenDemo {
    public static void main(String[] args) {

        final TwoGen<Integer, String> tgObj = new TwoGen<>(88, "Generics shit");

        tgObj.showTypes();

        final int value = tgObj.ob1;
        out.println("Value T: " + value);

        final String str = tgObj.ob2;
        out.println("Value V: " + str);
    }
}
