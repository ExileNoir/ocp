package chapter06.genericTypes.genericsExample;

import static java.lang.System.out;

/*
 * Bounded wildCard arguments
 * */
// two-dimensional coordinates
class TwoD {
    int x, y;

    TwoD(int a, int b) {
        this.x = a;
        this.y = b;
    }
}

// three-dimensional coordinates
class ThreeD extends TwoD {
    int z;

    ThreeD(int a, int b, int c) {
        super(a, b);
        this.z = c;
    }
}

// four-dimensional coordinates
class FourD extends ThreeD {
    int t;

    FourD(int a, int b, int c, int d) {
        super(a, b, c);
        this.t = d;
    }
}

// this class holds an Array of coordinates objects
class Coords<T extends TwoD> {
    T[] coords;

    Coords(T[] o) {
        this.coords = o;
    }
}


// Demonstrate a bounded wildcard
public class BoundedWildCardsDemo {
    static void showXY(final Coords<?> c) {
        out.println("X Y Coordinates: ");
        for (int i = 0; i < c.coords.length; i++) {
            out.println(c.coords[i].x + " " + c.coords[i].y + "\n");
        }
    }

    // UPPER BOUND <? extends superClass>
    static void showXYZ(final Coords<? extends ThreeD> c) {
        out.println("X Y Z Coordinates: ");
        for (int i = 0; i < c.coords.length; i++) {
            out.println(c.coords[i].x + " " + c.coords[i].y + " " + c.coords[i].z + "\n");
        }
    }

    static void showAll(final Coords<? extends FourD> c) {
        out.println("X Y Z T Coordinates; ");
        for (int i = 0; i < c.coords.length; i++) {
            out.println(c.coords[i].x + " " + c.coords[i].y + " " + c.coords[i].z + " " + c.coords[i].t + "\n");
        }
    }

    // LOWER BOUND <? super subClass>    // so no ThreeD || FourD
    static void showLowerXYZ(final Coords<? super ThreeD> c) {
        out.println("LOWER BOUND X Y Z ");
        for (int i = 0; i < c.coords.length; i++) {
            out.println(c.coords[i].x + " " + c.coords[i].y);
        }
    }


    public static void main(String[] args) {
        final TwoD td[] = {
                new TwoD(0, 0),
                new TwoD(7, 9),
                new TwoD(18, 4),
                new TwoD(-1, -23)
        };

        final Coords<TwoD> tdlocs = new Coords<>(td);

        out.println("Contents of tdlocs; ");
        showXY(tdlocs);
//        showXYZ(tdlocs);  // ERROR, not a ThreeD
//        showAll(tdlocs);  // ERROR, not a FourD

        final FourD fd[] = {
                new FourD(1, 2, 3, 4),
                new FourD(6, 8, 14, 8),
                new FourD(22, 9, 4, 9),
                new FourD(3, -2, -23, 17),
        };

        final Coords<FourD> fdlocs = new Coords<>(fd);
        out.println("Contents of fdlocs");
        // they work on all
        showXY(fdlocs);
        showXYZ(fdlocs);
        showAll(fdlocs);

        out.println();
        showLowerXYZ(tdlocs);

    }
}
