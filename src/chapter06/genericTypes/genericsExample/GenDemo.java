package chapter06.genericTypes.genericsExample;

import static java.lang.System.out;

/*
 * A simple Generic class.
 * T is a type parameter that
 * will be replaced by a real type
 * when an object of type gen is created
 * */
class Gen<T> {

    /* Declare an Object of type T */
    private T ob;

    /* Pass the constructor a reference to an Object of type T */
    Gen(T o) {
        this.ob = o;
    }

    /* Return ob */
    T getOb() {
        return ob;
    }

    /* Show type of T */
    void showType() {
        out.println("Type of T is " + ob.getClass().getName());
    }
}

/*
 * Demonstrate the Generic class.
 * */
public class GenDemo {
    public static void main(String[] args) {

        /* Create a Gen reference for Integers */
        Gen<Integer> iOb;

        /* Create a Gen<Integer> object and assign its
         * Reference to iOb. Notice the use of autoboxing
         * to encapsulate the value 88 within an Integer object.
         * */
        iOb = new Gen<>(88);

        /* Show the type of data used by iOb */
        iOb.showType();

        /* Get the value in iOb. Notice that no cast is needed */
        final int value = iOb.getOb();
        out.println("value: " + value + "\n");

        /* Create a Gen Object for Strings */
        final Gen<String> strOb = new Gen<>("Generics Test");

        /* Show the type of data used by strOb */
        strOb.showType();

        /* Get the value of strOb. Notice, again, that no cast is needed */
        final String str = strOb.getOb();
        out.println("value: " + str);


        /* Demonstration of class NonGen */
        out.println("\n\nDemo of class NonGen");

        NonGen nonGen;
        nonGen = new NonGen(88);

        nonGen.showType();
        int v = (Integer) nonGen.getOb();   // Here a CAST IS NEEDED
        out.println("Value: " + nonGen.getOb() + "\n");   // no cast needed when direct output

        final NonGen nonGenStr = new NonGen("Non-Generics test");
        nonGenStr.showType();
        String str1 = (String) nonGenStr.getOb();
        out.println("Value: " + nonGenStr.getOb());
    }
}

/*
 * NonGen is functionally equivalent to Gen
 * But does not use generics.
 * */
class NonGen {

    /* Ob is now of type Object (instead of T) */
    Object ob;

    /* Pass the constructor a reference to an Object of type Object */
    NonGen(Object o) {
        this.ob = o;
    }

    /* Return type Object*/
    Object getOb() {
        return ob;
    }

    /* Show type of ob*/
    void showType() {
        out.println("Type of ob is: " + ob.getClass().getName());
    }
}
