package chapter06.genericTypes.genericsExample;

import static java.lang.System.out;

/*
 * BOUNDED TYPES
 * create an upper bound that declares the superclass from
 * which all type arguments must be derived.
 * ** <T extends superClass>
 * */
public class Stats<T extends Number> {

    /* Array of Number || subClass */
    T[] nums;

    Stats(T[] o) {
        this.nums = o;
    }

    /* Return type double in all cases */
    double average() {
        double sum = 0.0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i].doubleValue();
        }
        return sum / nums.length;
    }

    /* Use of WildCard <<?>> arguments */
    boolean sameAvg(Stats<?> ob) {
        return average() == ob.average();
    }

    /* Demonstrate Stats */
    public static void main(String[] args) {

        final Integer inums[] = {1, 2, 3, 4, 5};
        final Stats<Integer> iob = new Stats<>(inums);
        final double value = iob.average();
        out.println("iob average is : " + value);

        final Double dnums[] = {1.1, 2.2, 3.3, 4.4, 5.5};
        final Stats<Double> dob = new Stats<>(dnums);
        final double valueD = dob.average();
        out.println("dob average is: " + valueD);

        // String array would NOT compile as it is not a subClass of Number!!

        final boolean sameAvg = iob.sameAvg(dob);
        out.println("Check for sameAvg: " + sameAvg);
    }
}
