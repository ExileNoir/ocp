package chapter06.genericTypes;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

public class TestBadLegacy {
    public static void main(String[] args) {
        final List<Integer> myList = new ArrayList<>();
        myList.add(4);
        myList.add(6);

        new Inserter().insert(myList);
        out.println(myList);

        new Inserter().insert01(myList);
        out.println(myList);
    }
}

class Inserter {
    /*
     * method with a non-generic List argument
     * */
    void insert(final List list) {
        list.add(new Integer(42));  // adds to the incoming list
    }

    void insert01(final List list) {
        list.add(new String("b"));
    }
    /*
     * Sadly it runs AND compiles == no runtime exception
     * Yet String input into type-safe ArrayList of <Integer>
     * */


    /*
     * IMPORTANT!!!!
     *
     * for BOTH insert() the compiler issues WARNINGS.
     * The compiler does not know whether the insert() is adding the right thing (Integer)
     * or the wrong thing (String)
     *
     * REASON compiler produces warming:
     * the insert() methods are ADDING something to the Collection.
     * Compiler knows there is a change the method might add the wrong thing to the Collection.
     *
     * */
}

