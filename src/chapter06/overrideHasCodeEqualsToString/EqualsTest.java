package chapter06.overrideHasCodeEqualsToString;

import static java.lang.System.out;

public class EqualsTest {
    public static void main(String[] args) {
        final Moof one = new Moof(8);
        final Moof two = new Moof(8);
        if (one.equals(two)) {        // with non override equals, boolean = false
            out.println("one and two are equal");
        }
        out.println("end");
    }
}

class Moof {
    private int moofValue;

    Moof(int val) {
        this.moofValue = val;
    }

    int getMoofValue() {
        return moofValue;
    }

    @Override
    public boolean equals(Object o) {
        return ((o instanceof Moof) && ((Moof) o).getMoofValue() == this.moofValue);
    }

    @Override
    public int hashCode() {
        return getMoofValue() * 17;
    }
}