package chapter06.overrideHasCodeEqualsToString;


import java.io.*;
import java.util.HashMap;

public class SaveMe implements Serializable {
    transient int x;        // returns to 0
    int y;
    transient int x10 = 10; // returns to 0 unless we uncomment lines 32 to 43 or we comment transient modifiers

    SaveMe(int xVal, int yVal, int x10Val) {
        x = xVal;
        y = yVal;
        x10 = x10Val;
    }

    @Override
    public int hashCode() {
        return (x ^ y);
    }

    @Override
    public boolean equals(Object o) {
        SaveMe test = (SaveMe) o;
        return (test.y == y && test.x == x && x10 == x10);
    }

    public String toString() {
        return "x: " + x + " y: " + y + " x10: " + x10;
    }


//    private void writeObject(final ObjectOutputStream os) throws IOException, ClassNotFoundException {
//        os.defaultWriteObject();
//        os.writeInt(x);
//        os.writeInt(x10);
//    }
//
//    // read the extra "int" I saved to the Dog Stream, and use it to create a new Coller Object and then assign to the Dog that's being de-serialized
//    private void readObject(final ObjectInputStream is) throws IOException, ClassNotFoundException {
//        is.defaultReadObject();
//        this.x = is.readInt();
//        this.x10 = is.readInt();
//    }


    public static void main(String[] args) throws Exception {
        SaveMe one = new SaveMe(20, 0, 50);
        SaveMe two = new SaveMe(20, 50, 50);


        HashMap<SaveMe, String> map = new HashMap<>();
        map.put(one, one.toString());
        map.put(two, two.toString());
        System.out.println("before: " + one.toString());
        System.out.println("before: " + two.toString());

        try (final FileOutputStream fos = new FileOutputStream("SaveMe01.ser");
             final ObjectOutputStream oos = new ObjectOutputStream(fos);
             final FileInputStream fis = new FileInputStream("SaveMe01.ser");
             final ObjectInputStream ois = new ObjectInputStream(fis)) {
            oos.writeObject(one);
            oos.writeObject(two);

            one = (SaveMe) ois.readObject();
            two = (SaveMe) ois.readObject();
        }
        System.out.println("after: " + one.toString());
        System.out.println("after: " + two.toString());
    }
}