package chapter06.overrideHasCodeEqualsToString;

import java.util.HashMap;
import java.util.Map;

public class DogHashMap {
    private String name;

    public DogHashMap(String name) {
        this.name = name;
    }

    public boolean equals(Object o) {
        return ((o instanceof DogHashMap) && (((DogHashMap) o).name.equals(this.name)));
    }

    @Override
    public int hashCode() {
        return name.length();
//        return 3;         // if chosen this, "constance will be found" else 6(from clover stays in hashKey)
    }

    public static void main(String[] args) {
        DogHashMap d1 = new DogHashMap("Clover");
        Map<Object, Object> map = new HashMap<>();
        map.put(d1, "Dog Value");
        System.out.println(map.get(d1));

        d1.name = "Constance";
        System.out.println(map.get(d1));
        d1.name = "Clover";
        System.out.println(map.get(new DogHashMap("Clover")));

    }
}
