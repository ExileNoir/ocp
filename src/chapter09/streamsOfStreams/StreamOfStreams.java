package chapter09.streamsOfStreams;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

import static java.lang.System.*;

public class StreamOfStreams {
    public static void main(String[] args) throws IOException {

        final String fileName = "words.txt";

        try(final Stream<String> myStream = Files.lines(Paths.get(fileName))){
            final Stream<String[]> inputStream = myStream.map(line->line.split("  "));
//            inputStream
//                    .map(array-> Arrays.stream(array))/*.forEach(out::println)*/;
//                    .map(Arrays::stream).forEach(out::println);

            final Stream<String> ss = inputStream
                    .flatMap(Arrays::stream);
            ss.forEach(out::println);
        }
    }
}
