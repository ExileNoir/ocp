package chapter09.streamsOfStreams;

import java.util.Arrays;
import java.util.stream.Stream;

public class StreamOfStreams02 {
    public static void main(String[] args) {

        final int[] n = {1, 2, 3};
        final int[] n1 = {4, 5, 6};

        final int[][] numbers = {n, n1};

        Stream.of(numbers)
                .flatMapToInt(s -> Arrays.stream(s))
                .forEach(System.out::print);
    }
}
