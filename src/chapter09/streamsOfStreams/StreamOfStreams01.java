package chapter09.streamsOfStreams;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import static java.lang.System.out;

public class StreamOfStreams01 {
    public static void main(String[] args) {

        final String fileName = "words.txt";

        try {
            final long numberOfJava = Files.lines(Paths.get(fileName))
                    .map(lines -> lines.split(" "))
                    .flatMap(Arrays::stream)                  // Stream Arrays, and flatten
                    .filter(w -> w.equalsIgnoreCase("java"))
                    .count();
            out.println("Number of times Java appears: " + numberOfJava);
        } catch (IOException e) {
            out.println("ERROR " + e.getMessage());
        }
    }
}
