package chapter09.generatingStreams;

import java.util.stream.IntStream;

import static java.lang.System.out;

public class GeneratingStreams02 {
    public static void main(String[] args) {

        /* range() */
        final IntStream numStream = IntStream
                .range(10, 15);      // generate numbers 10...14
        numStream.forEach(out::println);


        out.println();
        /* rangeClosed() */
        final IntStream numStreamInclusive = IntStream
                .rangeClosed(10, 15);        // generate numbers 10...15
        numStreamInclusive.forEach(out::println);


        out.println();
        /* limit() */
        final IntStream evensBefore10 = IntStream
                .rangeClosed(0, 20)
                .filter(i -> i % 2 == 0)
                .limit(5);      // limit to 5 results
        evensBefore10.forEach(out::println);


        out.println();
        /* skip() */
        final IntStream evensAfter10 = IntStream
                .rangeClosed(0, 20)
                .filter(i -> i % 2 == 0)
                .skip(5);       // skips first 5 results
        evensAfter10.forEach(out::println);
    }
}
