package chapter09.generatingStreams;

import java.util.Optional;
import java.util.stream.Stream;

import static java.lang.System.*;

public class Sensor {
    private String value = "up";
    private int i = 0;

    public String next() {
        i += 1;
        return i > 10 ? "Down" : "Up";
    }


    public static void main(String[] args) {
        final Sensor sensor = new Sensor();
        final Stream<String> sensorStream = Stream.generate(() -> sensor.next());       // Generates an infinite Stream
//      final Stream<String> sensorStream = Stream.generate(sensor::next);

        final Optional<String> result = sensorStream
                .filter(v->v.equalsIgnoreCase("down"))
                .findFirst();       // findFirst() returns an Optional
        result.ifPresent(out::println);


    }
}
