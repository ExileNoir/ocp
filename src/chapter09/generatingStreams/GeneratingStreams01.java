package chapter09.generatingStreams;

import java.util.stream.Stream;

import static java.lang.System.out;

public class GeneratingStreams01 {
    public static void main(String[] args) {

        Stream.iterate(0, s -> s + 1)       // creates an infinite Stream
                .limit(10)                       // BUT limits it to 10
                .forEach(out::println);
    }
}
