package chapter09.howToCreateStream.fromACollection;

import java.util.HashMap;
import java.util.Map;

import static java.lang.System.out;

public class CreateStreamCollection2 {
    public static void main(String[] args) {

        final Map<String, Integer> myMap = new HashMap<>();
        myMap.put("Boi", 6);
        myMap.put("Zooey", 3);
        myMap.put("Charis", 8);

        final long numElements = myMap
                .entrySet()                           // get a set of Map.Entry Objects
                .stream()                            // Stream the Set
                .filter(i -> i.getValue() > 4)      // filter the Map.Entry Objects
                .count();                          // count the objects

        out.println("Number of items in the map with value > 4: " + numElements);
    }
}
/*
 * If we Try to stream the Map directly -> Compile-time Error
 * => Map is not a Collection
 * solution: we first call the entrySet() on the Map and then call stream() on the resulting Set
 *
 * default Stream<E> stream()
 * */