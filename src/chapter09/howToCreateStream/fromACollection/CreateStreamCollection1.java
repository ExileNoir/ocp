package chapter09.howToCreateStream.fromACollection;

import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;

public class CreateStreamCollection1 {
    public static void main(String[] args) {

        final List<Double> tempsInPhoenix = Arrays.asList(123.6, 118.0, 113.0, 112.5, 115.8, 117.0, 110.2, 110.1, 106.0, 106.4);

        final long phoenixTemp = tempsInPhoenix
                .stream()                     // Stream the List of Doubles
                .filter(t -> t > 110.0)      // filter the stream
                .count();                   // count the Doubles that pass the filter test

        out.println("Number of days over 110 in 10 day period: " + phoenixTemp);

    }
}
