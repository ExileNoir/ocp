package chapter09.howToCreateStream.fromFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.out;

public class CreateStreamFile {
    public static void main(String[] args) throws IOException {

        final List<DVDInfo> dvds = loadDVDs("dvdInfo01.txt");     // load the DVDs from a file
        dvds.forEach(out::println);

    }

    static List<DVDInfo> loadDVDs1(final String fileName) throws IOException {      // Good way to fill the ArrayList
        final List<DVDInfo> dvds;

        try (final Stream<String> myStream = Files.lines(Paths.get(fileName))) {
            dvds = myStream.map(lines -> {
                final String[] dvdItems = lines.split("/");
                return new DVDInfo(dvdItems[0], dvdItems[1], dvdItems[2]);
            })
                    .collect(Collectors.toCollection(ArrayList::new));
        }
        return dvds;
    }

    static List<DVDInfo> loadDVDs(final String fileName) {      // Bad way as we modify an object outside the StreamPipeLine
        final List<DVDInfo> dvds = new ArrayList<>();

        // Stream a file, line by line
        try (final Stream<String> myStream = Files.lines(Paths.get(fileName))) {
            myStream.forEach(line -> {      // use forEach to display each line
                final String[] dvdItems = line.split("/");
                final DVDInfo dvd = new DVDInfo(dvdItems[0], dvdItems[1], dvdItems[2]);
                dvds.add(dvd);
            });
        } catch (IOException io) {
            out.println("Error reading DVD's");
            io.printStackTrace();
        }
        return dvds;
    }
}

class DVDInfo {
    private String title;
    private String genre;
    private String leadActor;

    public DVDInfo(String title, String genre, String leadActor) {
        this.title = title;
        this.genre = genre;
        this.leadActor = leadActor;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public String getLeadActor() {
        return leadActor;
    }

    @Override
    public String toString() {
        return title + " / " + genre + " /  " + leadActor;
    }
}