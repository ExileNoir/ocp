package chapter09.howToCreateStream.withStreamOf;

import java.util.stream.Stream;

import static java.lang.System.out;

public class CreateWithStreamOf1 {
    public static void main(String[] args) {

        final Integer[] myNums = {1, 2, 3};

//      final Stream<Integer> myStream = Arrays.stream(myNums);
        final Stream<Integer> myStream = Stream.of(myNums);

        final long numElements = myStream
                .filter(i -> i < 3)
                .count();
        out.println("\nNumber of items in collection < 3: " + numElements);

        /* Short way */
        final long numElements01 = Stream.of(1, 2, 3)
                .filter(i -> i < 3)
                .count();
        out.println("\nNumber of items in Collection01 < 3: " + numElements01);
    }
}
/*
 * works with any object values
 *
 *
 * static <T> Stream<T> of(T... values)
 * */