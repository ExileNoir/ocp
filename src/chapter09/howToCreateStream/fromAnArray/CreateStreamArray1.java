package chapter09.howToCreateStream.fromAnArray;

import java.util.Arrays;
import java.util.stream.Stream;

import static java.lang.System.out;

public class
CreateStreamArray1 {
    public static void main(String[] args) {

        final String[] dogs = {"Boi", "Zooey", "Charis"};

        final Stream<String> dogStream = Arrays.stream(dogs);
        final Stream<String> dogStream1 = Stream.of(dogs);

        out.println("Number of dogs in array: " + dogStream.count());
        out.println("\nNumber of dogs in array: " + dogStream1.count());
    }
}
