package chapter09.howToCreateStream.fromAnArray;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CreateStreamArray2 {
    public static void main(String[] args) {

        final int[] nums = {1, 2, 3, 4, 5, 6, 7, 8, 9};

        final Stream<int[]> stream = Stream.of(nums);

        final IntStream intStream = IntStream.of(nums);
        final IntStream intStream1 = Arrays.stream(nums);
    }
}
