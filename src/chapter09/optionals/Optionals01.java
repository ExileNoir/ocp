package chapter09.optionals;

import java.util.Optional;
import java.util.stream.Stream;

import static java.lang.System.out;

public class Optionals01 {
    public static void main(String[] args) {
        final Stream<Double> doubleStream = Stream.of(1.0, 2.0, 3.0, 4.0);

        final Optional<Double> aNum = doubleStream.findFirst(); // first the first double
//        if (aNum.isPresent())
//            out.println("First number from the double stream: " + aNum.get());
//        else
//            out.println("Doubles Stream is Empty");

        aNum.ifPresent(n -> out.println("First number from the double stream: " + n));
    }
}
