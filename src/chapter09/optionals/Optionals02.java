package chapter09.optionals;

import java.util.Optional;

import static java.lang.System.out;

public class Optionals02 {
    public static void main(String[] args) {

        final Dog dog = new Dog("Boi", 30, 6);
        final Optional<Dog> optionalDog = Optional.of(dog);
        optionalDog.ifPresent(out::println);


        // is Object is null above could be a potential problem --> NullPointerException
        final Dog dog1 = null;
        final Optional<Dog> optionalDog1 = Optional.ofNullable(dog1);   // check for Null
        optionalDog1.ifPresent(out::println);
        if (!optionalDog1.isPresent()) {
            out.println("Dog1 must be Null");
        }


        final Optional<Dog> emptyDog = Optional.empty();
        final Dog aDog = emptyDog.orElse(new Dog("Default Dog", 00, 00));
        out.println("A Dog: " + aDog);

    }
}

class Dog {
    private String name;
    private int age;
    private int weight;

    public Dog(String name, int age, int weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String toString() {
        return name + " is " + age + " years old and weighs " + weight + " pounds";
    }
}