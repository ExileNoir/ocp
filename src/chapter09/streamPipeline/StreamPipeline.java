package chapter09.streamPipeline;

import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;

public class StreamPipeline {
    public static void main(String[] args) {

        final List<String> names = Arrays.asList("Boi", "Charis", "Zooey", "Bokeh", "Clover", "Aiko");
        names
                .stream()                                                // the source
                .filter(s -> s.startsWith("B") || s.startsWith("C"))    // intermediate Op
                .filter(s -> s.length() > 3)                           // intermediate Op
                .forEach(out::println);                               // terminal Op
    }
}
/*
 * Stream Pipeline consists of 3 parts:
 * * Source : describes where the data is coming from
 * * Intermediate Operations : operate on the stream && produce another, perhaps modified, stream       (OPTIONAL)
 * * Terminal Operation : ends the stream && produces a value || some output
 *
 * Streams are LAZY
 * intermediate operations are NOT evaluated until terminal operation is invoked.
 * */