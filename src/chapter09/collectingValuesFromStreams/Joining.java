package chapter09.collectingValuesFromStreams;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.out;

public class Joining {
    public static void main(String[] args) {

        final List<Person> listPeople = Person.getPeople();

        final String older34 = listPeople
                .stream()
                .filter(person -> person.getAge() > 34)
                .map(Person::getName)
                .collect(Collectors.joining(", "));

        out.println("Names of People older than 34: " + older34);


        final String alfa = Stream.of("a", "b", "c", "d", "e")
                .collect(Collectors.joining(", "));
        out.println(alfa);

    }
}
