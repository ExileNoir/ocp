package chapter09.collectingValuesFromStreams;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.System.*;

public class SummingAndAveraging {
    public static void main(String[] args) {

        final List<Person> listPeople = Person.getPeople();

        final Map<String, Integer> sumOfBAges = listPeople
                .stream()
                .filter(person -> person.getName().startsWith("B"))
                .collect(Collectors.groupingBy(
                        Person::getName,
                        Collectors.summingInt(Person::getAge)
                ));

        out.println("People by sum of Age: ");
        sumOfBAges.forEach((k, v) -> out.println(k + " " + v));
    }
}
