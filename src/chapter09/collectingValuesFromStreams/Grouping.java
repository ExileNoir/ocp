package chapter09.collectingValuesFromStreams;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.System.out;

public class Grouping {
    public static void main(String[] args) {

        final List<Person> listPeople = Person.getPeople();

        final Map<Integer, List<Person>> peopleByAge = listPeople
                .stream()
                .collect(Collectors.groupingBy(         // groupingBy() 1
                        Person::getAge));        // Group by age

        peopleByAge.forEach((k, v) -> out.println("KeyAge: " + k + " Person: " + v));


        final Map<Integer, List<Person>> peopleByAgeV2 = listPeople
                .stream()
                .collect(Collectors.groupingBy(        // groupingBy() 2
                        Person::getAge,         // Group by age
                        Collectors.toCollection(ArrayList::new)));

        peopleByAgeV2.forEach((k, v) -> out.println("KeyAge: " + k + " Person: " + v));
    }
}
