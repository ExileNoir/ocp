package chapter09.collectingValuesFromStreams;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.System.out;

public class Grouping01 {
    public static void main(String[] args) {

        final List<Person> listPeople = Person.getPeople();

        final Map<Integer, Long> numPeopleWithAge = listPeople
                .stream()
                .collect(Collectors.groupingBy(       // groupingBy() takes a Function
                        Person::getAge,              // Group by age
                        Collectors.counting()));    // going to count rather than list persons

        numPeopleWithAge.forEach((k, v) -> out.println("Age: " + k + " totalPersons: " + v));
    }
}
