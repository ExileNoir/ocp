package chapter09.collectingValuesFromStreams;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Grouping02 {
    public static void main(String[] args) {

        final List<Person> listPeople = Person.getPeople();

        final Map<Integer, List<String>> namesByAge = listPeople
                .stream()
                .collect(Collectors.groupingBy(
                        Person::getAge,                // group by Age
                        Collectors.mapping(           // map from person to ...
                                Person::getName,     // ... name (String)
                                Collectors.toCollection(ArrayList::new)     // collect the names in a list
                        )
                ));

        namesByAge.forEach((k, v) -> System.out.println("keyAge: " + k + " names: " + v));
    }
}

/*
 * groupingBy() collector takes 2 arguments:
 * * Function: that determines how we're going to group (by age)
 * * Collector: that tells us how to reduce the values in the Map associated with each age
 * */