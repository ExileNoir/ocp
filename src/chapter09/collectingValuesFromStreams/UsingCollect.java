package chapter09.collectingValuesFromStreams;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.out;

public class UsingCollect {
    public static void main(String[] args) {

        Stream<Character> c = Stream.of('c','b','c');
        c.sorted().findAny().ifPresent(out::println);

        final String fileName = "names.txt";

        try (final Stream<String> myStream = Files.lines(Paths.get(fileName))) {
            final List<String> data = myStream
                    .collect(Collectors.toList());
            data.forEach(out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
