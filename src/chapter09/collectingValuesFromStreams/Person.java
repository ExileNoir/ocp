package chapter09.collectingValuesFromStreams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Person {
    private String name;
    private Integer age;

    public Person(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return this.getName() + " is " + this.getAge() + " years old";
    }


    public static void main(String[] args) {

        final List<Person> people = getPeople();

        final ArrayList<Person> peopleAge34 = people
                .stream()
                .filter(person -> person.getAge() == 34)
//                .collect(Collectors.toList());
                .collect(Collectors.toCollection(ArrayList::new));
        System.out.println("People Aged 34 " + peopleAge34);
    }

    static List<Person> getPeople() {
        final Person beth = new Person("Beth", 30);
        final Person beth2 = new Person("Beth", 45);
        final Person eric = new Person("Eric", 31);
        final Person deb = new Person("Deb", 31);
        final Person liz = new Person("Liz", 30);
        final Person windi = new Person("Wendi", 34);
        final Person kathy = new Person("Kathy", 35);
        final Person bert = new Person("Bert", 32);
        final Person bert2 = new Person("Bert", 38);
        final Person bill = new Person("Bill", 34);
        final Person bill2 = new Person("Bill", 40);
        final Person robert = new Person("Robert", 38);

        final List<Person> people = new ArrayList<>();
        people.add(beth);
        people.add(eric);
        people.add(deb);
        people.add(liz);
        people.add(windi);
        people.add(kathy);
        people.add(bert);
        people.add(bill);
        people.add(robert);
        people.add(beth2);
        people.add(bert2);
        people.add(bill2);
        
        return people;
    }

}
