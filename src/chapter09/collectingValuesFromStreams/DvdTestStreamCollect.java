package chapter09.collectingValuesFromStreams;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.out;

public class DvdTestStreamCollect {
    public static void main(String[] args) throws IOException {

        final String fileName = "dvdInfo01.txt";
        List<DVDInfo> listDvd1;

        try (final Stream<String> myStream = Files.lines(Paths.get(fileName))) {
            listDvd1 = myStream
                    .map(lines -> new DVDInfo(lines.split("/")[0], lines.split("/")[1], lines.split("/")[2]))
                    .collect(Collectors.toCollection(ArrayList::new));
        }
        listDvd1.forEach(out::println);
    }
}


class DVDInfo {
    private String title;
    private String genre;
    private String leadActor;

    public DVDInfo(String title, String genre, String leadActor) {
        this.title = title;
        this.genre = genre;
        this.leadActor = leadActor;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public String getLeadActor() {
        return leadActor;
    }

    @Override
    public String toString() {
        return title + " / " + genre + " /  " + leadActor;
    }
}