package chapter09.collectingValuesFromStreams;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.System.out;

public class SummingAndAveraging01 {
    public static void main(String[] args) {

        final List<Person> listPeople = Person.getPeople();

        final Map<String, Double> avgOfBAges = listPeople       // note: we Double not Integer for the values
                .stream()
                .filter(person -> person.getName().startsWith("B"))
                .collect(Collectors.groupingBy(
                        Person::getName,
                        Collectors.averagingInt(Person::getAge)
                ));

        out.println("People by average of Age");
        avgOfBAges.forEach((k, v) -> out.println(k + " " + v));
    }
}
