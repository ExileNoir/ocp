package chapter09.collectingValuesFromStreams;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class Partitioning {
    public static void main(String[] args) {

        final List<Person> listPeople = Person.getPeople();

        final Map<Boolean, List<Person>> peopleOlderThen34 = listPeople
                .stream()
                .collect(Collectors.partitioningBy(     // partitioningBy() takes a Predicate
                        p -> p.getAge() > 34));

        peopleOlderThen34.forEach((k, v) -> System.out.println("People > 34:\n" + k + " " + v));

        LongStream stream = LongStream.of(9);
        stream.mapToDouble(p->p).forEach(System.out::println);

        Stream<Character> stream1 = Stream.of('c','b','a');
        System.out.println(stream1.sorted().findFirst());


    }
}

