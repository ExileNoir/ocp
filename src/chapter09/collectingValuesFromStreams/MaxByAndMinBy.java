package chapter09.collectingValuesFromStreams;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.lang.System.out;

public class MaxByAndMinBy {
    public static void main(String[] args) {

        final List<Person> listPeople = Person.getPeople();

        final Optional<Person> oldest = listPeople
                .stream()
                .collect(Collectors.maxBy((p1, p2) -> p1.getAge() - p2.getAge()));      // maxBy() returns an Optional
//              .collect(Collectors.maxBy(Comparator.comparingInt(Person::getAge)));

        oldest.ifPresent(person -> out.println("Oldest Person: " + person));
    }
}
