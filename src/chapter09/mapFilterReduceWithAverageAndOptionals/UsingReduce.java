package chapter09.mapFilterReduceWithAverageAndOptionals;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;

import static chapter09.mapFilterReduceWithAverageAndOptionals.MapFilterReduceAverageAndOptionals.getReadings;
import static java.lang.System.out;

public class UsingReduce {
    public static void main(String[] args) {

        final List<Reading> readings = getReadings();
        final List<Reading> readings1 = Arrays.asList();


        final OptionalDouble sum = readings
                .stream()
                .mapToDouble(r -> r.getValue())
                .reduce((v1, v2) -> v1 + v2);
//              .reduce(Double::sum);

        if (sum.isPresent()) {
            out.println("Sum of all readings: " + sum.getAsDouble());
        }

        final double sum1 = readings
                .stream()
                .mapToDouble(r -> r.getValue())
                .reduce(5.0, (v1, v2) -> v1 + v2);     // provide an identity value (provides initial value
                                                              // || value to return is stream is empty

        out.println("Sum of all readings: " + sum1);
    }
}
