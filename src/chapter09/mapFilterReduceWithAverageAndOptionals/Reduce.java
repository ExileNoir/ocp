package chapter09.mapFilterReduceWithAverageAndOptionals;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;

import static chapter09.mapFilterReduceWithAverageAndOptionals.MapFilterReduceAverageAndOptionals.getReadings;
import static java.lang.System.out;

public class Reduce {
    public static void main(String[] args) {

        final List<Reading> readings = getReadings();

        final OptionalDouble max = readings
                .stream()
                .mapToDouble(Reading::getValue)
                .max();     // returns NullPointerException on Empty Stream, thus Optional is needed

        if (max.isPresent()) {
            out.println("Max of all readings: " + max);
        } else {
            out.println("Empty optional");
        }


        final List<Reading> readings2 = Arrays.asList();
        final double sum = readings2
                .stream()
                .mapToDouble(r -> r.getValue())
                .sum();     // sum() default value == 0, so no OptionalDouble needed

        out.println("Sum of all readings on empty list: " + sum);
    }

}
