package chapter09.mapFilterReduceWithAverageAndOptionals;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;

import static java.lang.System.out;

class Reading {
    private int year;
    private int month;
    private int day;
    private double value;

    public Reading(int year, int month, int day, double value) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.value = value;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}

public class MapFilterReduceAverageAndOptionals {
    public static void main(String[] args) {

        final List<Reading> readings = getReadings();

        // OptionalDouble == a double value that might || might not be there
        final OptionalDouble avg = readings
                .stream()
                .mapToDouble(Reading::getValue)
                .filter(v -> v >= 406.00 && v < 407.00)
                .average();

        out.println("Average of 406 readings: " + avg);
        out.println("Average of 406 readings: " + avg.getAsDouble());
        // if you try to call getAsDouble() on empty Stream == NoSuchElementException

        if (avg.isPresent()) {
            out.println("Average of 406 readings: " + avg.getAsDouble());
        } else
            out.println("Empty optional");

        avg.ifPresent(out::println);
    }


    static List<Reading> getReadings() {
        return Arrays.asList(
                new Reading(2017, 1, 1, 405.91),
                new Reading(2017, 1, 8, 405.98),
                new Reading(2017, 1, 15, 406.14),
                new Reading(2017, 1, 22, 406.48),
                new Reading(2017, 1, 29, 406.20),
                new Reading(2017, 2, 5, 407.12),
                new Reading(2017, 2, 12, 406.03)
        );
    }
}
