package chapter09.chapter09_V2.newStreamMethods;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

public class ListingDirContents {
    public static void main(String[] args) {

        final Path path = Paths.get("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\src\\chapter01");

        try {
            Files.list(path)        // searches only one dir deep
                    .filter(p -> !Files.isDirectory(p))
                    .map(Path::toAbsolutePath)
                    .forEach(out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
/*
 * Files.list()
 * * searches one level deep and is analogous to File.listFiles(), except it relies on streams.
 * * analogous to Files.walk(path, 1)
 * */