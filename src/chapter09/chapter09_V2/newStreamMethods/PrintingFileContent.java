package chapter09.chapter09_V2.newStreamMethods;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

public class PrintingFileContent {
    public static void main(String[] args) {

        final Path path = Paths.get("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01.txt");
        try {
//            final List<String> s = Files.readAllLines(path);      // returns List<String>
//            out.println(s);

            Files.lines(path)                                       // returns Stream<String>
                    .filter(s -> s.startsWith("Donnie ") || s.contains("Raiders"))
                    .forEach(out::println);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
/*
 * Files.readAllLines()
 * * OutOfMemoryError could be resulted when reading very large file.
 * */


/*
 * Files.lines()
 * * contents of file are read && processed lazily,
 * * only a small portion of the file is stored in memory at any given time.
 * */


/*
 * * Files.readAllLines(path).filter(s -> s.length()>2).forEach(System.out::println);
 *
 * * Files.lines(path).filter(s -> s.length()>2).forEach(System.out::println);
 *
 * * The first line does not compile because the filter() operation cannot be applied to a
 * Collection without first converting it to a Stream using the stream() method.
 * */