package chapter09.chapter09_V2.newStreamMethods;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

public class WalkingADir {
    public static void main(String[] args) {

        final Path path = Paths.get("C:", "Users", "Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep");

        try {
            Files.walk(path, 3)        // searches in all dir deep
                    .filter(p -> p.toString().endsWith(".java"))
                    .forEach(out::println);
        } catch (IOException i) {
            i.printStackTrace();
        }
    }
}
/*
 * Files.walk(Path)
 * * R a Stream<Path> that traverses the dir in a depth-first, LAZY manner.
 * * Lazy : set of elements is built && read while dir is being traversed.
 * * Until a specific subDir = reached, its child elements are not  loaded.
 * */

/*
 * FileVisitOption.FOLLOW_LINKS  (3rd argument)
 * * Change default behavior to traverse symbolic links.
 * * When option is used, walk() will track its visited paths,
 * throwing a FileSystemLoopException if a cycle = detected.
 * */