package chapter09.chapter09_V2.newStreamMethods;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

public class FindDir {
    public static void main(String[] args) {

        final Path path = Paths.get("C:", "Users", "Steven\\Desktop\\PROJECTS\\Java\\OCP");

        try {
            Files.find(path,
                    4,
                    (p, a) -> p.toString().endsWith(".java") && a.lastModifiedTime().toMillis() > 1420070400000L)      // BiPredicate
                    .forEach(out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
/*
 * Files.find(Path, int, BiPredicate)
 * * behaves like Files.walk(), except :
 * * it requires the depth value to be explicitly set && BiPredicate to filter the data.
 * * has as well a FileVisitOption.FOLLOW_LINKS as last argument (optional).
 * */