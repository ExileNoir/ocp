package chapter09.chapter09_V2.fileAttributes;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

import static java.lang.System.*;

public class BasicFileAttributesSample {
    public static void main(String[] args) throws IOException {

        final Path path = Paths.get("C:", "Users", "Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo.txt");

        final BasicFileAttributes data = Files.readAttributes(path, BasicFileAttributes.class);

        out.println("Is path a dir? " + data.isDirectory());
        out.println("is path a regular file: " + data.isRegularFile());
        out.println("is path a symbolic link: " + data.isSymbolicLink());
        out.println("path not a file, dir, nor symbolic link? " + data.isOther());
        out.println("size: (in bytes) " + data.size());
        out.println("Creation date/time: " + data.creationTime());
        out.println("Last modified date/time: " + data.lastModifiedTime());
        out.println("Last access date/time: " + data.lastAccessTime());
        out.println("unique file identifier (if available): " + data.fileKey());
    }
}
