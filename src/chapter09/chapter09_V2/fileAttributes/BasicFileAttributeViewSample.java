package chapter09.chapter09_V2.fileAttributes;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;

public class BasicFileAttributeViewSample {
    public static void main(String[] args) throws IOException {

        final Path path = Paths.get("C:", "Users", "Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo.txt");

        final BasicFileAttributeView view = Files.getFileAttributeView(path, BasicFileAttributeView.class);
        final BasicFileAttributes data = view.readAttributes();

        final FileTime lastModifiedTime = FileTime.fromMillis(data.lastModifiedTime().toMillis() + 10_000);

        view.setTimes(lastModifiedTime, null, null);
    }
}

/*
 * advantages of using NIO.2 views to read metadata
 * * It makes fewer round-trips to the file system.
 * * It can be used to access file system-dependent attributes.
 * * For reading multiple attributes, it is often more performant.
 * */