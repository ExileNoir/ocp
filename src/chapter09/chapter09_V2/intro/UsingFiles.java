package chapter09.chapter09_V2.intro;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.List;

import static java.lang.System.out;

public class UsingFiles {
    public static void main(String[] args) throws URISyntaxException {

        final Path path = Paths.get("C:", "Users", "Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01.txt");
        final Path path1 = Paths.get("C:", "Users", "Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01.txt");

        final File converted = path.toFile();
        final Path converted1 = converted.toPath();

        final Path path2 = FileSystems.getDefault().getPath("C:", "Users", "Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01.txt");
        final FileSystem fileSystem;
        try {
            fileSystem = FileSystems.getFileSystem(new URI("http://www.selikoff.net"));
            final Path path3 = fileSystem.getPath("duck.text");
        } catch (ProviderNotFoundException e) {
        }


        out.println(Files.exists(path));        // returns boolean
        try {
            out.println(Files.isSameFile(path, path1));     // does NOT verify contents only looks if paths are the same
//          Files.createDirectory(Paths.get("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01COPY01.txt"));      // FileAlreadyExistsException
//          Files.copy(path, Paths.get("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01COPY01.txt"));
        } catch (IOException e) {
            e.printStackTrace();        // if one of Paths != correct
        }


        try (final BufferedReader reader = Files.newBufferedReader(path, Charset.defaultCharset())) {
            String line;
            while ((line = reader.readLine()) != null)
                out.println(line);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        try {
//            Files.copy(Paths.get("C:", "Users", "Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01.txt"),
//                    Paths.get("C:", "Users", "Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo02.txt"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        try (final BufferedWriter writer = Files.newBufferedWriter(
                Paths.get("C:", "Users", "Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo02.txt"),
                Charset.defaultCharset())) {
            writer.write("HAIL LORD EXILE NOIR");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (final BufferedReader reader = Files.newBufferedReader(
                Paths.get("C:", "Users", "Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo02.txt"),
                Charset.defaultCharset())) {
            String line;
            while ((line = reader.readLine()) != null)
                out.println(line);
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            final List<String> lines = Files.readAllLines(
                    Paths.get("C:", "Users", "Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo02.txt"));
            out.println(lines);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            out.println(Files.getOwner(path).getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
