package chapter09.chapter09_V2.intro;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class ReaderOfLines {
    public static void main(String[] args) throws IOException {

        final String lines = "C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01.txt";

        final List<String> lineReader = Files.readAllLines(Paths.get(lines));
        lineReader.forEach(System.out::println);

    }
}
