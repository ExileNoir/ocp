package chapter09.chapter09_V2.intro;

import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

public class FindPath {
    public static void main(String[] args) {

        final Path path = Paths.get("C:", "Users", "Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01.txt");
        for (int i = 0; i < path.getNameCount(); i++) {                      // File System Root == excluded from the path component
            out.println(" Element " + i + " is: " + path.getName(i));       // getName(int) == zero-indexed
        }


        final Path file = path.getFileName();   // returns the farthest element from the root
        out.println("\n" + file.toString());

        final Path parent = path.getParent();
        out.println("\n" + parent.toString());

        final Path root = path.getRoot();
        out.println("\n" + root.toString());

        out.println("\npath is absolute? " + path.isAbsolute());
        final Path pathToAbsolute = Paths.get("PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01.txt");
        out.println("to Absolute: " + pathToAbsolute.toAbsolutePath());

        out.println("\nSubPath 0 to 3 " + path.subpath(0, 3));     // Root not included && end index excluded  && IllegalArgumentExc -> end&start are same index && end index is +1 (size is 3 max endIndex == 3)

        final Path p1 = Paths.get("Grouping.java");
        final Path p2 = Paths.get("collectingValuesFromStreams\\Grouping01.java");
        out.println(p1.relativize(p2));
        out.println(p2.relativize(p1));

        final Path p3 = Paths.get("E:/data");
        final Path p4 = Paths.get("E:/user/home");
        final Path relativePath = p3.relativize(p4);
        out.println("\n" + relativePath);
        out.println(p3.resolve(relativePath));
        out.println(p3.resolve(relativePath).normalize());


    }
}
