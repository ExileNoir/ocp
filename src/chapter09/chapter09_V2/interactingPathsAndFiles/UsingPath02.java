package chapter09.chapter09_V2.interactingPathsAndFiles;

import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

public class UsingPath02 {
    public static void main(String[] args) {

        final Path path = Paths.get("/land/hippo/harry.happy");
        out.println("the Path Name is: " + path);
        out.println("the Path Name is: " + path.toString());
        out.println("the Absolute Path Name is: " + path.toAbsolutePath());

        out.println("Path name of N2 = " + path.getName(2));        // returns a Path != a String
        out.println("Path name of N0 = " + path.getName(0) + "\n");     // Root element (c:) NOT included

        for (int i = 0; i < path.getNameCount(); i++) {
            out.println("     Element " + i + " is:  " + path.getName(i));      // zero indexed = getName(int)
        }
    }
}
