package chapter09.chapter09_V2.interactingPathsAndFiles;

import java.nio.file.Paths;

import static java.lang.System.*;

public class UsingPath01 {
    public static void main(String[] args) {

        final String s = Paths.get("/hello/zoo/../home").getParent().normalize().toAbsolutePath().toString();
        out.println(s);
    }
}
