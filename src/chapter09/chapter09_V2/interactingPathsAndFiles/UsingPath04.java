package chapter09.chapter09_V2.interactingPathsAndFiles;

import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

public class UsingPath04 {
    public static void main(String[] args) {

        final Path path = Paths.get("C:\\birds\\egret.txt");
        out.println("Path is Absolute? " + path.isAbsolute());
        out.println("Absolute Path: " + path.toAbsolutePath());

        final Path path1 = Paths.get("birds/condor.txt");
        out.println("Path1 is Absolute? " + path1.isAbsolute());
        out.println("Absolute Path1: " + path1.toAbsolutePath());
    }
}
