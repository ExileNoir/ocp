package chapter09.chapter09_V2.interactingPathsAndFiles;

import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

/* relativize(Path) */
public class UsingPath06 {
    public static void main(String[] args) {

        final Path p1 = Paths.get("animals/water/fish.txt");
        final Path p2 = Paths.get("animals/birds.txt");

        out.println(p1.relativize(p2));     // Takes a path instance NOT String
        out.println(p2.relativize(p1) + "\n");

        final Path p3 = Paths.get("E:/habitat");
        final Path p4 = Paths.get("E:/sanctuary/raven");
        out.println(p3.relativize(p4));
        out.println(p4.relativize(p3));
    }
}
/*
 * Both Paths need to be Absolute || Relative IF mixed ==> IllegalArgumentException
 * If Absolute paths are used on Windows both must use same root Directory | Drive letter  ==> IllegalArgumentException
 * */