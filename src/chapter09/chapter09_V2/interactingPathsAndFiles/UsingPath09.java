package chapter09.chapter09_V2.interactingPathsAndFiles;

import java.io.IOException;
import java.nio.file.Paths;

import static java.lang.System.out;

public class UsingPath09 {
    public static void main(String[] args) {

        try {
            out.println(Paths.get("/zebra/food.source").toAbsolutePath());
            out.println(Paths.get("/zebra/food.source").toRealPath());
        } catch (IOException e) {
            out.println("toRealPath does not exist");
        }
    }
}
