package chapter09.chapter09_V2.interactingPathsAndFiles;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class UsingFiles04 {
    public static void main(String[] args) {

        final String baseLink = "C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\";

        // Copy stream data to file
        try (final InputStream is = new FileInputStream(baseLink + "dvdInfo01.txt")) {
            Files.copy(
                    is,
                    Paths.get(baseLink + "copyUsingFilesIS.txt"));
        } catch (IOException e) {
            System.out.println("Error in IS");
            e.printStackTrace();        // FileAlreadyExistsException
        }


        // Copy file data to stream
        try (final OutputStream os = new FileOutputStream(baseLink + "copyUsingFilesOS.txt")) {
            Files.copy(
                    Paths.get(baseLink + "dvdInfo01.txt"),
                    os);
        } catch (IOException e) {
            System.out.println("Error in OS");
            e.printStackTrace();
        }
    }
}
