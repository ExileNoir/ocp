package chapter09.chapter09_V2.interactingPathsAndFiles;

import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

/* resolve(Path) */
public class UsingPath07 {
    public static void main(String[] args) {

        final Path p1 = Paths.get("/cats/../panther");
        final Path p2 = Paths.get("food");
        out.println(p1.resolve(p2));               // add P2 to P1 but does NOT cleanUp path (..)
        out.println(p1.resolve(p2).normalize());

        final Path p3 = Paths.get("dir1/dir2/foo.txt");
        final Path p4 = Paths.get("bar.txt");
        out.println(p3.resolveSibling(p4));     // Changes name of P3 to P4

        final Path p5 = Paths.get("E:/habitat");
        final Path p6 = Paths.get("E:/sanctuary/raven");
        out.println(p5.resolve(p6));         // P5 is ignored as both are absolute

    }
}
