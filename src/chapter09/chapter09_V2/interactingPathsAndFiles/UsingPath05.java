package chapter09.chapter09_V2.interactingPathsAndFiles;

import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

public class UsingPath05 {
    public static void main(String[] args) {

        final Path path = Paths.get("/mammal/carnivore/raccoon.image");
        out.println("Path is: " + path.toString());

        out.println("SubPath from 0 to 3 is: " + path.subpath(0, 3));       // NOT include root of file
        out.println("SubPath from 1 to 3 is: " + path.subpath(1, 3));
        out.println("SubPath from 1 to 2 is: " + path.subpath(1, 2));

//      out.println("SubPath from 1 to 10 is: "+path.subpath(1,10));      // IllegalArgumentException
//      out.println("SubPath from 1 to 1 is: "+path.subpath(1,1));      // IllegalArgumentException

    }
}
