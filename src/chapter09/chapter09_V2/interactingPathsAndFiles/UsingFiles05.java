package chapter09.chapter09_V2.interactingPathsAndFiles;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

public class UsingFiles05 {

    private static final String LINK = "C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01.txt";

    public static void main(String[] args) {

        final Path path = Paths.get(LINK);

        // read from the stream
        try (final BufferedReader reader = Files.newBufferedReader(
                path,
                Charset.defaultCharset())) {

            String currentLine;
            while ((currentLine = reader.readLine()) != null) {
                out.println(currentLine);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
