package chapter09.chapter09_V2.interactingPathsAndFiles;

import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

/* normalize() */
public class UsingPath08 {
    public static void main(String[] args) {

        final Path p1 = Paths.get("E:/date");
        final Path p2 = Paths.get("E:/user/home");

        final Path relativePath = p1.relativize(p2);
        out.println("Relative Path:     " + relativePath.toString());

        final Path resolvePath = p1.resolve(relativePath);
        out.println("Resolve Path:      " + p1.resolve(resolvePath));
        out.println("Normalized Path:   " + p1.resolve(resolvePath).normalize());

        out.println(p1.resolve(p2));
        out.println(p2.resolve(p1));
    }
}
/*
 * normalize()
 * * (like most methods in Path interface, does NOT modify the Path object,
 * BUT instead returns a new Path object
 * */