package chapter09.chapter09_V2.interactingPathsAndFiles;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.lang.System.out;

public class UsingFiles03 {

    private static final String BASE_LINK = "C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\";

    public static void main(String[] args) {

        try {
            Files.copy(
                    Paths.get(BASE_LINK, "dvdInfo01COPY01.txt"),
                    Paths.get(BASE_LINK, "dvdInfo01COPY11.txt"));
        } catch (IOException e) {     // when file || directory does NOT exist || cannot be read
            out.println("error");
        }
    }
}
/*
 * copy() == shallow
 * * files && subdirectories within the directory are NOT copied
 * */