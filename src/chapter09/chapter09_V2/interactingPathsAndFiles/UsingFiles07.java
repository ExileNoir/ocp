package chapter09.chapter09_V2.interactingPathsAndFiles;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class UsingFiles07 {

    private static final String LINK = "C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\UseFiles06.txt";

    public static void main(String[] args) {

        final Path path = Paths.get(LINK);

        try {
            final List<String> lines = Files.readAllLines(path);

            lines.forEach(System.out::println);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
