package chapter09.chapter09_V2.interactingPathsAndFiles;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.lang.System.out;

public class UsingFiles01 {

    private static final String LINK = "C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01.txt";

    public static void main(String[] args) {

        final boolean one = Files.exists(Paths.get("/ostrich/feathers.png"));
        out.println(one);

        final boolean two = Files.exists(Paths.get(LINK));
        out.println(two);

        try {
            final boolean three = Files.isSameFile(
                    Paths.get(LINK),
                    Paths.get("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01.txt"));
            out.println(three);

            final boolean four = Files.isSameFile(
                    Paths.get(LINK),
                    Paths.get("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo02.txt"));
            out.println(four);

            final boolean five = Files.isSameFile(
                    Paths.get(LINK),
                    Paths.get("C:\\Users\\Steven\\Desktop\\dvdInfo01.txt"));        // does not exist == exception
            out.println(five);      // isSameFile() does NOT compare the contents of the files!
        } catch (IOException e) {
            out.println("\nERROR isSameFile ==> one file does not exist");
        }
    }
}
