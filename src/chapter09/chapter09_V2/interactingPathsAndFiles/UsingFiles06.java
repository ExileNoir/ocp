package chapter09.chapter09_V2.interactingPathsAndFiles;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class UsingFiles06 {

    private static final String LINK = "C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\UseFiles06.txt";

    public static void main(String[] args) {

        final Path path = Paths.get(LINK);

        // Write to the stream
        try (final BufferedWriter writer = Files.newBufferedWriter(
                path,
                Charset.defaultCharset())) {

            writer.write("My Baby Girl...\n");
            writer.write("My Baby Girl...\n");
            writer.write("My OOOooo Baby Girl...\n");
            writer.write("you are my Baby Girl...");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
