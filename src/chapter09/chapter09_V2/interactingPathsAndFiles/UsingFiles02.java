package chapter09.chapter09_V2.interactingPathsAndFiles;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.lang.System.out;

public class UsingFiles02 {
    public static void main(String[] args) {

        try {
            Files.createDirectory(Paths.get("/bison/field"));           // assuming bison exists -> create directory field      // mkdir()

            Files.createDirectories(Paths.get("bison", "field/pasture", "green"));        // mkdirs()

        } catch (IOException e) {
            out.println("Error -> createDirectory");                      // bison does not exist upon createDirectory field
        }
    }
}
