package chapter09.chapter09_V2.interactingPathsAndFiles;

import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

public class UsingPath03 {
    private static void printPathInfo(final Path p) {
        out.println("FileName is: " + p.getFileName());     // getFilename() R a Path != a String!!!
        out.println("Root is: " + p.getRoot());

        Path currentParent = p;
        while ((currentParent = currentParent.getParent()) != null) {
            out.println("    Current parent is:  " + currentParent);
        }
    }

    public static void main(String[] args) {

        printPathInfo(Paths.get("/zoo/armadillo/shells.txt"));
        out.println("\n");

        printPathInfo(Paths.get("armadillo/shells.txt"));
        out.println("\n");

        printPathInfo(Paths.get("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01.txt"));
    }
}
