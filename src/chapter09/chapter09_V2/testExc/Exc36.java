package chapter09.chapter09_V2.testExc;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public class Exc36 {

    private static final String LINK = "C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\src\\chapter01";

    public static void main(String[] args) throws IOException {

        final Path p = Paths.get(LINK);

        Files.walk(p)
                .map(z -> z.toAbsolutePath().toString())
                .filter(s -> s.endsWith(".java"))
                .collect(Collectors.toList())
                .forEach(System.out::println);

        System.out.println();

        Files.find(p,
                Integer.MAX_VALUE,
                (w, a) -> w.toAbsolutePath().toString().endsWith(".java"))
                .collect(Collectors.toList())
                .forEach(System.out::println);
    }
}
