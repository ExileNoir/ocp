package chapter09.chapter09_V2.testExc;

import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.*;

public class Exc31 {
    private static Path rebuild(final Path p) {
        Path v = null;
        for (int i = 0; i < p.getNameCount(); i++) {
            if (v == null) {
                v = p.getName(i);
            } else {
                v = v.resolve(p.getName(i));
            }
        }
        return v;
    }

    public static void main(String[] args) {
        final Path original = Paths.get("/tissue/heart/chambers.txt");
        final Path repaired = rebuild(original);
        out.println(original);
        out.println(repaired);
        out.println(original.equals(repaired));
    }
}
