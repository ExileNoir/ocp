package chapter09.chapter09_V2.testExc;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Exc28 {

    private static final String LINK = "C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OCPprep\\dvdInfo01.txt";

    private static Path makeAbsolute(final Path p){
        if (p!= null && p.isAbsolute()){
            return p.toAbsolutePath();
        }
        return p;
    }


    public static void main(String[] args) {
        System.out.println(makeAbsolute(Paths.get(LINK)));
        System.out.println(makeAbsolute(Paths.get("dvdInfo01.txt")));
    }
}
