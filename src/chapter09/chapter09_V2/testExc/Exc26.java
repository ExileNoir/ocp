package chapter09.chapter09_V2.testExc;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.lang.System.*;

public class Exc26 {
    private void tendGarden(final Path p) throws IOException {
        Files.walk(p, 1)
                .map(path -> {
                    try {
                        return path.toRealPath();        // toRealPath() interacts with File System and therefore
                    } catch (IOException e) {           // throws a IOException && needs to be handles INSIDE lambda
                        e.printStackTrace();
                    }
                    return path;
                })
                .forEach(out::println);
    }
}
