package chapter09.chapter09_V2.testExc;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Exc03 {
    private static void findHiddenFile(final Path p) throws IOException {
        if (Files.isHidden(p)) {
            System.out.println("Found!");
        }
    }

    public static void main(String[] args) throws IOException {
        findHiddenFile(Paths.get("C:\\Users\\Steven\\Desktop\\PROJECTS\\Java\\OCP\\OcpTxtCreateIO\\hide.txt"));
    }
}
