package chapter09.chapter09_V2;

import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.*;

public class Exc01 {
    public static void main(String[] args) {

       final String p1 ="/user/.././root";
       final String p2 = "../bear.txt";

       final Path path = Paths.get(p1,p2);
       final Path path1 = Paths.get(p1);
       final Path path2 = Paths.get(p2);

        out.println(path);
        out.println(path1.normalize());
        out.println(path2.normalize());
        out.println(path.normalize());

        out.println(path.relativize(Paths.get("/lion")));


        Path p = Paths.get("/user/.././root","koalabear.txt");
        p.normalize().relativize(Paths.get("/lion"));
        out.println(p);
        Path pNew = p.normalize();
        out.println(pNew);
        out.println(pNew.relativize(Paths.get("/lion")));
    }
}
