package chapter09.whatIsAStream;

import java.util.Arrays;
import java.util.stream.Stream;

import static java.lang.System.out;

public class Streams01 {
    public static void main(String[] args) {

        final Integer[] myNums = {1, 2, 3};

        final Stream<Integer> myStream = Arrays.stream(myNums);     // Stream the Array
        out.println(myStream);

        final long numElements = myStream
                .count();                    // get the number of elements in the stream
        out.println("Number of elements in the Steam: " + numElements);


        final long numElements1 = Arrays.stream(myNums)
                .filter(i -> i > 1)     // add an INTERMEDIATE OPERATION to filter the stream
                .count();              // TERMINAL OPERATION, counts the elements in a stream
        out.println("\nNumber of elements > 1: " + numElements1);


        final long numElements2 = Arrays.stream(myNums)     // 1. create the stream
                .filter((i) -> i > 2)                      // 2. filter the stream // filter by > 2 on a whole new system
                .count();                                 // 3. count the elements
        out.println("\nNumber of elements > 2: " + numElements2);

    }
}
/*
 * A stream is an object that gets its data from a source, BUT it doesn't store any data itself!
 * */