package chapter09.searchingAndSorting;

import java.util.List;
import java.util.Optional;

import static java.lang.System.out;

public class Searching02 {
    public static void main(String[] args) {

        final List<Dog> dogs = Searching01.getDogs();

        final Optional<Dog> c50 = dogs
                .stream()
                .filter(dog -> dog.getWeight() > 50)
                .filter(dog -> dog.getName().startsWith("c"))
//                .filter(dog -> dog.getName().startsWith("c") && dog.getWeight() > 50)
                .findAny();
        c50.ifPresent(out::println);

        final Optional<Dog> d5 = dogs
                .stream()
                .filter(dog -> dog.getAge() > 5)
                .peek(out::println)
                .findAny();
        d5.ifPresent(out::println);
    }
}
