package chapter09.searchingAndSorting;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

public class Searching01 {
    public static void main(String[] args) {
        final List<Dog> dogs = getDogs();

        // anyMatch()
        final boolean hasCNames = dogs
                .stream()
                .filter(dog -> dog.getWeight() > 50)
                .anyMatch(dog -> dog.getName().startsWith("c"));
        out.println("Are there any dogs > 50 pounds whose name starts with 'C'? " + hasCNames);


        // allMatch()
        final boolean isOlder = dogs
                .stream()
//              .allMatch(dog -> dog.getAge() > 5);
//              .map(Dog::getAge)
                .mapToInt(dog -> dog.getAge())
                .allMatch(age -> age > 5);
        out.println("Are all the dogs age older then 5? " + isOlder);


        // noneMatch()
        final boolean notRed = dogs
                .stream()
                .map(Dog::getName)
                .noneMatch(n -> n.equalsIgnoreCase("red"));
        out.println("None of the dogs are red: " + notRed);
    }

    static List<Dog> getDogs() {
        final Dog boi = new Dog("boi", 6, 30);
        final Dog clover = new Dog("clover", 12, 35);
        final Dog aiko = new Dog("aiko", 10, 50);
        final Dog zooey = new Dog("zooey", 8, 45);
        final Dog charis = new Dog("charis", 7, 120);

        final List<Dog> dogs = new ArrayList<>();
        dogs.add(boi);
        dogs.add(clover);
        dogs.add(aiko);
        dogs.add(zooey);
        dogs.add(charis);

        return dogs;
    }
}

class Dog {
    private String name;
    private int age;
    private int weight;

    public Dog(String name, int age, int weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String toString() {
        return name + " is " + age + " years old and weighs " + weight + " pounds";
    }
}