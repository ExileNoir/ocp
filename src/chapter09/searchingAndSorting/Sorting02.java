package chapter09.searchingAndSorting;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

import static java.lang.System.out;

public class Sorting02 {
    public static void main(String[] args) {

        List<Duck> ducks = Arrays.asList(
                new Duck("Jerry", "yellow", 3),
                new Duck("George", "brown", 4),
                new Duck("Kramer", "mottled", 6),
                new Duck("Elaine", "white", 2)

        );

        out.println("Natual Order Name");
        ducks
                .stream()
                .sorted()       // sorts ducks by name (compareTo())
                .forEach(out::println);


        out.println("\nSort by Age");
        ducks
                .stream()
                .sorted((d1, d2) -> d1.getAge() - d2.getAge())
//              .sorted(Comparator.comparingInt(Duck::getAge))
                .forEach(out::println);

        out.println("\nSort by Color (reversed)");
//      final Comparator<Duck> byColorLambda = (c1, c2) -> c1.getColor().compareTo(c2.getColor());
        final Comparator<Duck> byColorLambda = Comparator.comparing(Duck::getColor);
        ducks
                .stream()
                .sorted(byColorLambda.reversed())       // reversed()
                .forEach(out::println);


        out.println("\nSort by Name && thenComparing sort by AgeReversed");
        final Comparator<Duck> byNameLambda = Comparator.comparing(Duck::getName);
        final Comparator<Duck> byAgeLambda = Comparator.comparing(Duck::getAge);

        // thenComparing()
        ducks = Arrays.asList(
                new Duck("Jerry", "yellow", 3),
                new Duck("George", "brown", 4),
                new Duck("Kramer", "mottled", 6),
                new Duck("Elaine", "white", 2),

                new Duck("Jerry", "mottled", 10),
                new Duck("George", "white", 12),
                new Duck("Kramer", "brown", 11),
                new Duck("Elaine", "brown", 13)
        );
        ducks
                .stream()
                .sorted(byNameLambda.thenComparing(byAgeLambda.reversed()))
                .forEach(out::println);


        out.println("\nSort by Color && thenComparing sort by Age");
        ducks
                .stream()
                .sorted(byColorLambda.thenComparing(byAgeLambda))
                .forEach(out::println);


        out.println("\nDistinct by Color");
        // distinct()
        ducks
                .stream()
                .map(Duck::getColor)
                .distinct()
                .forEach(out::println);

        out.println("\nDistinct by Name");
        ducks
                .stream()
                .map(Duck::getName)
                .distinct()
                .forEach(out::println);


        Stream.of(1, 1, 1, 2, 2, 3, 3, 3, 4, 4, 4, 4).distinct().forEach(out::println);
    }
}

class Duck implements Comparable<Duck> {
    private String name;
    private String color;
    private int age;

    public Duck(String name, String color, int age) {
        this.name = name;
        this.color = color;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return (getName() + " is " + getColor() + " and is " + getAge() + " years old.");
    }

    @Override
    public int compareTo(final Duck duck) {
        return this.getName().compareTo(duck.getName());
    }
}
