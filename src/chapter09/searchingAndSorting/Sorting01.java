package chapter09.searchingAndSorting;

import java.util.stream.Stream;

import static java.lang.System.out;

public class Sorting01 {
    public static void main(String[] args) {

        // Sorting by natural order: sorted() operator
        Stream.of("Jerry", "George", "Kramer", "Elaine")
                .sorted()
                .forEach(out::println);


        out.println(Stream.of(1,2,3).findAny());
    }
}
