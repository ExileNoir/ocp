package chapter09.searchingAndSorting;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static chapter09.searchingAndSorting.Searching01.getDogs;
import static java.lang.System.out;

public class Collect01 {
    public static void main(String[] args) {

        final List<Dog> dogs = getDogs();

        final List<Dog> heavyDogs = dogs
                .stream()
                .filter(dog -> dog.getWeight() >= 50)
                .collect(Collectors.toList());  // collects the filtered dogs into a new list
//        .collect(Collectors.toCollection(ArrayList::new));

        heavyDogs.forEach(out::println);
    }
}

