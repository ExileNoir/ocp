package chapter09.operatingOnStreams;

import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;

public class MapFilterReduce {
    public static void main(String[] args) {

        final List<Integer> nums = Arrays.asList(1, 2, 3, 4, 5, 6);

        final long result = nums
                .stream()
                .map(n -> n * n)         // map values in stream to squares (map intermediate op)
                .filter(n -> n > 20)    // keep only squares > 20 (filter intermediate op)
                .count();              // count the squares > 20 (reduction op || final op)

        out.println("Result (stream): " + result + "\n");

        final long result01 = nums
                .stream()
                .peek(n -> out.println("Number is: " + n + ", "))        // print the number
                .map(n -> n * n)
                .filter(n -> n > 20)
                .peek(n -> out.println("Square is: " + n + ", "))    // intermediate Op && allows you to peek into the stream, takes Consumer
                .count();                                           // && produces the same exact stream as it's called on

        out.println();
        // Java 7 way without streams
        long resultOldWay = 0;
        for (final Integer num : nums) {
            final int square = num * num;
            if (square > 20) {
                resultOldWay++;
                out.println("Square of " + num + " is: " + square);
            }
        }
        out.println("Result old way: " + resultOldWay);
    }
}
