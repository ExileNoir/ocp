package chapter11.executorsAndThreadPools;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.System.out;

public class ExecutorDemo {
    public static void main(String[] args) {
        // create a fixed thread pool with maximum three threads
        final ExecutorService executor = Executors.newFixedThreadPool(3);
//        final ExecutorService executor = Executors.newCachedThreadPool();

        // submit runnable tasks to the executor
        executor.execute(new PrintChar('a',100));
        executor.execute(new PrintChar('b',100));
        executor.execute(new PrintNum(100));

        executor.shutdown();
    }
}

class PrintChar implements Runnable {
    private char charToPrint;
    private int times;

    PrintChar(final char c, final int t) {
        this.charToPrint = c;
        this.times = t;
    }

    @Override
    public void run() {
        for (int i = 0; i < times; i++) {
            out.print(charToPrint);
        }
    }
}

class PrintNum implements Runnable {
    private int lastNum;

    PrintNum(final int n) {
        this.lastNum = n;
    }

    @Override
    public void run() {
        for (int i = 1; i <= lastNum; i++) {
            out.print(" " + i);
        }
    }
}