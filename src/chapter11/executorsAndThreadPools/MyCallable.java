package chapter11.executorsAndThreadPools;

import java.util.concurrent.*;

import static java.lang.System.out;

public class MyCallable implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        // obtain a random number from 1 to 10
        final int count = ThreadLocalRandom.current().nextInt(1, 11);
        for (int i = 1; i <= count; i++) {
            out.println("Running..." + i);
        }
        return count;
    }

    public static void main(String[] args) {
        final Callable<Integer> c = new MyCallable();
        final ExecutorService ex = Executors.newCachedThreadPool();
        final Future<Integer> f = ex.submit(c);     // finishes in the future

        final Future<Integer> f1 = ex.submit(() -> {
            int count = ThreadLocalRandom.current().nextInt(1, 11);
            for (int i = 0; i <= count; i++) {
                out.println("Running in Lambda... " + i);
            }
            return count;
        });

        try {
            final Integer v = f1.get();      // blocks until done
            out.println("Ran: " + v);
        } catch (InterruptedException | ExecutionException e) {
            out.println("Failed");
        }

        ScheduledExecutorService ftses = Executors.newScheduledThreadPool(4);
        ftses.schedule(c,5,TimeUnit.SECONDS);
        ftses.submit(c);
        ftses.scheduleAtFixedRate(new PrintNum(10),5,2,TimeUnit.SECONDS);
        ftses.scheduleWithFixedDelay(new PrintNum(10),6,3,TimeUnit.SECONDS);
    }
}
