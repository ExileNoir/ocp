package chapter11.testExc;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static java.lang.System.out;

public class Exc06 {
    public static void main(String[] args) {

        ConcurrentMap<String, Integer> map = new ConcurrentHashMap<>();
        map.put("Constance", 8);
        map.putIfAbsent("Steven", 36);

        map.remove("Steven",36);
//        map.remove("Steven",35);
//        map.remove("Steven");
        out.println(map);

    }
}
