package chapter11.testExc;

import java.util.Optional;
import java.util.stream.Stream;

import static java.lang.System.out;

public class Exc16 {
    public static void main(String[] args) {

        final Stream<Integer> nums = Stream.of(10, 5, 3, 2);
        final Optional<Integer> result = nums
                .parallel()
                .map(n -> n * 10)
                .reduce((n1, n2) -> n1 - n2);
        result.ifPresent(n -> out.print("Result: " + n));
    }
}
/*
 * Result == unpredictable,
 * Reduction function IS NOT associative && stream is parallel
 * */