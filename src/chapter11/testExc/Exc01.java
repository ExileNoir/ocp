package chapter11.testExc;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

import static java.lang.System.out;

public class Exc01 {
    public static void main(String[] args) {

        final CopyOnWriteArrayList<Integer> cowList = new CopyOnWriteArrayList<>();
//        List<Integer> cowList = new ArrayList<>();        // ConcurrentModificationExc (Runtime)
        cowList.add(4);
        cowList.add(2);
        final Iterator<Integer> it = cowList.iterator();
        cowList.add(6);
        while (it.hasNext()) {
            out.print(it.next() + " ");
        }
    }
}
