package chapter11.testExc;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import static java.lang.System.out;

enum Singleton {
    INSTANCE;
    int value = 0;

    private void doSomethingWithValue() {
        value += 1;
    }

    public synchronized int updateValue() {
        doSomethingWithValue();
        return value;
    }

    public int getValue() {
        return value;
    }
}

public class Exc05 {
    public static void main(String[] args) {

        final CyclicBarrier barrier = new CyclicBarrier(3, () -> out.println(Singleton.INSTANCE.getValue()));

        final Runnable r = () -> {
            for (int i = 0; i < 100; i++) {
                Singleton.INSTANCE.updateValue();
            }
            try {
                barrier.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
        };

        final Thread t1 = new Thread(r);
        final Thread t2 = new Thread(r);
        final Thread t3 = new Thread(r);
        t1.start();
        t2.start();
        t3.start();
        out.println("Main Thread is Complete");
    }
}
