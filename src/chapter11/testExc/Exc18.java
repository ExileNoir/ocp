package chapter11.testExc;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

public class Exc18 {
    public static void main(String[] args) {

        final Stream<List<String>> sDogNames = Arrays.asList(
                Arrays.asList("boi", "aiko", "charis", "zooey", "clover"),
                Arrays.asList("boi", "aiko", "charis", "zooey", "clover"))
                .stream()
                .unordered();

        sDogNames.parallel()
                .flatMap(Collection::stream)
                .map(String::toUpperCase)
                .forEach(s -> System.out.print(s + " "));

    }
}
// result == unpredictable