package chapter11.testExc;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

import static java.lang.System.out;

public class Exc02 {
    public static void main(String[] args) {

        final CopyOnWriteArrayList<Integer> cowList = new CopyOnWriteArrayList<>();
        cowList.add(4);
        cowList.add(2);
        cowList.add(6);
        final Iterator<Integer> it = cowList.iterator();
        cowList.remove(2);
        while (it.hasNext()) out.print(it.next() + " ");

        out.println();
        final Iterator<Integer> it1 = cowList.iterator();
        while (it1.hasNext()) out.print(it1.next() + " ");
    }
}
