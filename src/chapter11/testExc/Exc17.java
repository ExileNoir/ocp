package chapter11.testExc;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import static java.lang.System.out;

public class Exc17 {
    public static void main(String[] args) {

        final Stream<List<String>> sDogNames = Stream
                .generate(() -> Arrays.asList("boi", "aiko", "charis", "constance", "clover"))
                .limit(2)
                .unordered();

        sDogNames.parallel()
                .flatMap(Collection::stream)
                .map(String::toUpperCase)
                .forEach(s -> out.print(s + " "));
    }

    // result == unpredictable
}
