package chapter11.applyAtomicVarAndLocks.locks;


import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.System.*;

public class ConsumerProducer {
    private static Buffer buffer = new Buffer();

    public static void main(String[] args) {
        // Create a ThreadPool with 2 threads
        final ExecutorService executor = Executors.newFixedThreadPool(2);
        executor.execute(new ConsumerTask());
        executor.execute(new ProducerTask());

        executor.shutdown();
    }


    /* A task for adding an int to the buffer */
    private static class ProducerTask implements Runnable {
        public void run() {
            try {
                int i = 1;
                while (true) {
                    out.println("Producer writes " + i);
                    buffer.write(i++);      // add a value to the buffer
                    // Put the thread into sleep
                    Thread.sleep((int) (Math.random() * 10000));
                }
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

    /* A task for reading and deleting an int from the buffer */
    private static class ConsumerTask implements Runnable {
        public void run() {
            try {
                while (true) {
                    out.println("\t\t\tConsumer reads " + buffer.read());
                    // Put the thread into sleep
                    Thread.sleep((int) (Math.random() * 10000));
                }
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

    /* An inner class for Buffer */
    private static class Buffer {
        private static final int CAPACITY = 1; // buffer size
        private final LinkedList<Integer> queue = new LinkedList<>();

        // Create a new Lock
        private static Lock lock = new ReentrantLock();

        // Create two Conditions
        private static Condition notEmpty = lock.newCondition();
        private static Condition isFull = lock.newCondition();

        public void write(final int value) {
            lock.lock();    // Acquire the lock
            try {
                while (queue.size() == CAPACITY) {
                    out.println("Wait for notFull condition");
                    isFull.await();
                }
                queue.offer(value);
                notEmpty.signalAll();       // Signal notEmpty condition
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            } finally {
                lock.unlock();      // release the lock
            }
        }

        public int read() {
            int value = 0;
            lock.lock();        // Acquire the lock
            try {
                while (queue.isEmpty()) {
                    out.println("\t\t\tWait for notEmpty condition");
                    notEmpty.await();
                }
                value = queue.remove();
                isFull.signal();       // Signal notFull condition
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            } finally {
                lock.unlock();
                return value;
            }
        }
    }
}
