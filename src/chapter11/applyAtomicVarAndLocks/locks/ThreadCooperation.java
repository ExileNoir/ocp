package chapter11.applyAtomicVarAndLocks.locks;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.System.*;

public class ThreadCooperation {
    private static Account account = new Account();

    public static void main(String[] args) {
        final ExecutorService executor = Executors.newFixedThreadPool(2);
        executor.execute(new DepositTask());
        executor.execute(new WithDrawTask());
        executor.shutdown();

        out.println("Thread 1\t\tThread 2\t\tBalance");
    }

    public static class DepositTask implements Runnable {
        // keep adding an amount to the account
        @Override
        public void run() {
            try {       // Purposely delay it to let the withdraw method proceed
                while (true) {
                    account.deposit((int) (Math.random() * 10) + 1);
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static class WithDrawTask implements Runnable {
        // keep subtracting an amount from the account
        @Override
        public void run() {
            while (true)
                account.withdraw((int) (Math.random() * 10) + 1);
        }
    }

    private static class Account {
        // Create a new Lock
        private static Lock lock = new ReentrantLock();

        // Create a Condition
        private static Condition newDeposit = lock.newCondition();

        private int balance = 0;

        public int getBalance() {
            return balance;
        }

        public void withdraw(final int amount) {
            lock.lock();
            try {
                while (balance < amount) {
                    out.println("\t\t\tWait for a deposit");
                    newDeposit.await();
                }

                balance -= amount;
                out.println("\t\t\tWithdraw: " + amount + "\t\t\t\t" + getBalance());
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            } finally {
                lock.unlock();
            }
        }

        public void deposit(final int amount) {
            lock.lock();
            try {
                balance += amount;
                out.println("Deposit: " + amount + "\t\t\t\t\t\t\t" + getBalance());

                // Signal thread waiting on the Condition
                newDeposit.signalAll();
            } finally {
                lock.unlock();
            }
        }
    }
}
