package chapter11.applyAtomicVarAndLocks.atomicVar;

import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.System.out;

class IncrementThread extends Thread {

    private Counter counter;

    public IncrementThread(final Counter counter) {
        this.counter = counter;
    }

    // "i" == local == therefore Thread Safe
    public void run() {
        for (int i = 0; i < 1000; i++) {
            counter.increment();
        }
    }
}

public class Counter {

    //    private int count;
    private AtomicInteger count = new AtomicInteger();

    public void increment() {
//        count++;
        count.getAndIncrement();
    }

    public int getCount() {
        return count.intValue();
    }

    public static void main(String[] args) {
        final Counter counter = new Counter();
        final IncrementThread it1 = new IncrementThread(counter);
        final IncrementThread it2 = new IncrementThread(counter);

        it1.start();
        it2.start();

        try {
            it1.join();
            it2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        out.println(counter.getCount());
    }
}
