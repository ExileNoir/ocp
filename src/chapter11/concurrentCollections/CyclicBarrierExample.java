package chapter11.concurrentCollections;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import static java.lang.System.out;

public class CyclicBarrierExample {

    final List<String> result = new ArrayList<>();
    static String[] dogs1 = {"boi", "clover", "charis"};
    static String[] dogs2 = {"aiko", "zooey", "biscuit"};
    final CyclicBarrier barrier;                 // the barrier for the threads

    // Runnable
    class ProcessDogs implements Runnable {    // Each thread will process an array of dogs
        private String[] dogs;

        ProcessDogs(final String[] d) {
            dogs = d;
        }

// #4
        /* 2 threads begin running && process their respective arrays. */
        @Override
        public void run() {     // convert first chars into uppercase
            for (int i = 0; i < dogs.length; i++) {
                final String dogName = dogs[i];
                final String newDogName = dogName.substring(0, 1).toUpperCase() + dogName.substring(1);
                dogs[i] = newDogName;
            }
            try {
// #5
                /* loop is complete, thread then waits at the barrier by calling await */
                barrier.await();     // wait at the Barrier
            } catch (InterruptedException | BrokenBarrierException ie) {
                ie.printStackTrace();
            }
// #7
            /* neither thread can print out until BOTH threads have reached the barrier && barrier Runnable is complete */
            out.println(Thread.currentThread().getName() + " is done!");
        }
    }

    public CyclicBarrierExample() {
// #1  ---------------------------------- the 2nd argument code runs later
        /* new CyclicBarrier: passing number of threads that will wait at the barrier (await())
         * && a Runnable that will run by the last thread to reach the barrier.
         * */
        barrier = new CyclicBarrier(2, () -> {   // 2 threads, 1 runnable
// #6
            /* Once both threads reach the barrier, then the Runnable passed in CyclicBarrier constructor is executed by last thread to reach barrier */
            // copy results to the list
            for (final String s : dogs1) {
                result.add(s);                 // add dogs from array 1
            }
            for (int i = 0; i < dogs2.length; i++) {
                result.add(dogs2[i]);       // add dogs from array 2
            }
            // print the thread name and result
            out.println(Thread.currentThread().getName() + " Result: " + result);
        });
// #2
        final Thread t1 = new Thread(new ProcessDogs(dogs1), "t1");
        final Thread t2 = new Thread(new ProcessDogs(dogs2), "t2");
// #3
        t1.start();
        t2.start();
        out.println("Main Thread is done");
    }

    public static void main(String[] args) {
        new CyclicBarrierExample();
    }
}
