package chapter11.concurrentCollections;

import java.util.concurrent.*;

import static java.lang.System.out;
import static java.lang.System.setOut;

public class BlockingQueues {
    public static void main(String[] args) throws InterruptedException {

        BlockingQueue<Integer> bq = new ArrayBlockingQueue<>(5);    // needs to be bounded
        BlockingQueue<Integer> bq1 = new LinkedBlockingQueue<>();
        BlockingQueue<Integer> bq2 = new LinkedBlockingDeque<>();

        bq.add(3);           // IllegalStateExc if queue is bounded && full
        bq.offer(1);     // R false if queue is bounded && full
        bq.put(2);      // R void, will block until space in queue = available

        bq.poll();       // removes 1El || R null = queue.isEmpty
        bq.take();      // removes 1El || blocks until object comes available

        bq.element();        // get head of queue && NoSuchElementExc = queue.isEmpty
        bq.peek();          // get head of queue && R null = queue.isEmpty


        BlockingQueue<Integer> bq3 = new PriorityBlockingQueue<>();
        final int[] ia = {1, 5, 3, 7, 6, 9, 8, 2, 4,10};                      // unordered date

        for (final int x : ia)
            bq3.offer(x);                    // load queue  || add elements to the queue

        out.println(bq3);

        for (Integer integer : bq3) {
            out.print(integer+" ");
        }
        out.println(bq3.take());;

        out.println();
        for (final int x : ia)
            out.print(bq3.poll() + " ");  // review queue && removes elements from the queue!!
        out.println();
    }
}
