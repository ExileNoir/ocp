package chapter11.concurrentCollections;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ArrayListRunnable implements Runnable {
    // shared by all threads
//    private List<Integer> list = new ArrayList<>();
//    private List<Integer> list = Collections.synchronizedList(new ArrayList<>());
    private List<Integer> list = new CopyOnWriteArrayList<>();

    public ArrayListRunnable() {
        // add some elements
        for (int i = 0; i < 1000; i++) {
            list.add(i);
        }
    }

    // might run concurrently,
    // you cannot be sure to be safe, you must assume it does
    public void run() {
        final String tName = Thread.currentThread().getName();
        while (!list.isEmpty()) {
            System.out.println(tName + " removed " + list.remove(0));
        }
    }

    public static void main(String[] args) {
        final ArrayListRunnable alr = new ArrayListRunnable();
        final Thread t1 = new Thread(alr);
        final Thread t2 = new Thread(alr);

        t1.start();
        t2.start();
    }
}
