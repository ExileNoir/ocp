package chapter11.concurrentCollections;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.System.out;



public class ConsumerProducerUsingBlockingQueue {
    private static ArrayBlockingQueue<Integer> buffer = new ArrayBlockingQueue<>(2);

    public static void main(String[] args) {
        // Create a Thread Pool with 2 threads
        final ExecutorService executor = Executors.newFixedThreadPool(2);
        executor.submit(new ProducerTask());
        executor.execute(new ConsumerTask());

        executor.shutdown();
    }

    /* A task for adding an int to the buffer */
    private static class ProducerTask implements Runnable {
        public void run() {
            try {
                int i = 1;
                while (true) {
                    out.println("Producer writes " + i);
                    buffer.put(i++);        // add any value tot he buffer, say 1
                    // put the Thead to sleep
                    Thread.sleep((int) (Math.random() * 10000));
                }
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

    /* A task for reading and deleting an int from the buffer */
    private static class ConsumerTask implements Runnable {
        public void run() {
            try {
                while (true) {
                    out.println("\t\t\tConsumer reads " + buffer.take());
                    // Put the Thread to sleep
                    Thread.sleep((int) (Math.random() * 10000));
                }
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }
}
