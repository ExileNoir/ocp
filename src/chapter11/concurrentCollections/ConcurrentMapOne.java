package chapter11.concurrentCollections;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.System.*;

public class ConcurrentMapOne {
    public static void main(String[] args) {

        final Map<String, String> map = new ConcurrentHashMap<>();
        map.put("Hello", "Hello Constance");

        map.putIfAbsent("Steven", "Lord ExileNoir");

        out.println(map);
    }
}
