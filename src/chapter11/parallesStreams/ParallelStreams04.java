package chapter11.parallesStreams;

import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;

public class ParallelStreams04 {
    public static void main(String[] args) {
        final List<Integer> nums = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);


        long sum = nums.stream()
                .unordered()        // make the stream unordered
                .parallel()
                .mapToInt(n -> n)
                .filter(i -> i % 2 == 1 ? true : false)
                .sum();

        out.println("Sum: " + sum);
    }
}
