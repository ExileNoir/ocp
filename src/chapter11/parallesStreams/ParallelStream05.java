package chapter11.parallesStreams;

import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;

public class ParallelStream05 {
    public static void main(String[] args) {
        final List<Integer> nums = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        final int product = nums.stream()
                .unordered()                                  // unordered for efficiency
                .parallel()                                  // make the stream parallel
                .reduce(1, (i1, i2) -> i1 * i2);    // reduce to product
        out.println("Product reduction: " + product);
    }
}
