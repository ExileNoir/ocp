package chapter11.parallesStreams;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

import static java.lang.System.*;

public class ParallelStreams02 {
    public static void main(String[] args) {
        // tell the pc exactly how many workers to use by creating a custom ForkJoinPool &&
        // then submitting a task to the pool for execution
        final List<Integer> nums = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        final ForkJoinPool fjPool = new ForkJoinPool(2);

        try {
            final int sum =
                    fjPool.submit(                     // returns a Future (FutureTask)
                            () -> nums.stream()       // a Callable (Value returning task)
                                    .parallel()      // make the Stream parallel
                                    .peek(integer ->
                                            out.println(integer + " : " + Thread.currentThread().getName()))
                                    .mapToInt(n -> n)
                                    .sum()
                    ).get();                // From Future; get() waits for computation to complete and gets result
            out.println("FJP with 2 workers, sum is: " + sum);
        } catch (Exception e) {
            out.println("Error executing stream sum");
            e.printStackTrace();
        }
    }
}
