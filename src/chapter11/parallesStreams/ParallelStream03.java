package chapter11.parallesStreams;

import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;

public class ParallelStream03 {
    public static void main(String[] args) {
        // combine stream() && parallel() into one method
        final List<Integer> nums = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        final int sum = nums.parallelStream()       // make a parallel stream
                .peek(integer ->
                        out.println(integer + " : " + Thread.currentThread().getName()))
                .mapToInt(n -> n)
                .sum();
        out.println("Sum: " + sum);
    }
}
