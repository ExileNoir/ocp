package chapter11.parallelForkJoinFramework;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

import static java.lang.System.out;

public class FindMaxPositionRecursiveTask extends RecursiveTask<Integer> {

    private static final int THRESHOLD = 10000;
    private int[] data;
    private int start;
    private int end;

    public FindMaxPositionRecursiveTask(final int[] data, final int start, final int end) {
        this.data = data;
        this.start = start;
        this.end = end;
    }

    @Override
    protected Integer compute() {                // return type matches the <generic> type
        if (end - start <= THRESHOLD) {         // is it a manageable amount of work?
            int position = 0;                  // if all values are equal, return position 0

            for (int i = start; i < end; i++) {
                if (data[i] > data[position]) {
                    position = i;
                }
            }
            return position;
        } else {        // task too big, split it
            final int halfWay = ((end - start) / 2) + start;

            final FindMaxPositionRecursiveTask t1 = new FindMaxPositionRecursiveTask(data, start, halfWay);
            final FindMaxPositionRecursiveTask t2 = new FindMaxPositionRecursiveTask(data, halfWay, end);

            t1.fork();
            final int position2 = t2.compute();      // work on right half of task
            final int position1 = t1.join();        // wait for queued task to be completed

            // of of the position in two subsection which is greater?
            if (data[position1] > data[position2]) {
                return position1;
            } else if (data[position1] < data[position2]) {
                return position2;
            } else {
                return position1 < position2 ? position1 : position2;
            }
        }
    }


    public static void main(String[] args) {
        // Same code as in RandomInitRecursiveAction class
        // create large array and initialize it using Fork/Join
        // After initializing array with random values, we reuse ForkJoinPool with our RecursiveTask to find position of greatest value.
        final int[] data = new int[10_000_000];
        final ForkJoinPool fjPool = new ForkJoinPool();
        final RandomInitRecursiveAction action = new RandomInitRecursiveAction(data, 0, data.length);

        fjPool.invoke(action);

        // new Code begins here
        final FindMaxPositionRecursiveTask task = new FindMaxPositionRecursiveTask(data, 0, data.length);
        final Integer position = fjPool.invoke(task);
        out.println("Position " + position + " Value: " + data[position]);
    }
}
