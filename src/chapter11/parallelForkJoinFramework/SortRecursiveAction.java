package chapter11.parallelForkJoinFramework;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

import static java.lang.System.arraycopy;
import static java.lang.System.out;

public class SortRecursiveAction extends RecursiveAction {

    private static final int THRESHOLD = 1000;
    private int[] data;
    private int start;
    private int end;

    public SortRecursiveAction(final int[] data, final int start, final int end) {
        this.data = data;
        this.start = start;
        this.end = end;
    }

    @Override
    public void compute() {
        if (end - start <= THRESHOLD) {
            Arrays.sort(data, start, end);
        } else {
            final int halfWay = ((end - start) / 2) + start;
            final SortRecursiveAction a1 = new SortRecursiveAction(data, start, halfWay);
            final SortRecursiveAction a2 = new SortRecursiveAction(data, halfWay, end);

            invokeAll(a1, a2);       // shortcut for fork() && join()


            if (data[halfWay - 1] <= data[halfWay]) {           // from here all is related to merging 2 sorted subsections of an array into a single larger sorted subsection
                return;     // already sorted
            }
            // merging of sorted subsections begins here
            int[] temp = new int[end - start];
            int s1 = start;
            int s2 = halfWay;
            int d = 0;

            while (s1 < halfWay && s2 < end) {
                if (data[s1] < data[s2]) {
                    temp[d++] = data[s1++];
                } else if (data[s1] > data[s2]) {
                    temp[d++] = data[s2++];
                } else {
                    temp[d++] = data[s2++];
                }
            }
            if (s1 != halfWay) {
                arraycopy(data, s1, temp, d, temp.length - d);
            }
            if (s2 != end) {
                arraycopy(data, s2, temp, d, temp.length - d);
            }
            arraycopy(temp, 0, d, s1, temp.length);
        }

    }


    public static void main(String[] args) {
        final int[] data = {5, 3, 2, 10, 8, 7, 1, 9, 4, 6};
        final ForkJoinPool pjPool = new ForkJoinPool();

        final SortRecursiveAction sort = new SortRecursiveAction(data, 0, data.length);
        pjPool.invoke(sort);

        for (int datum : data) {
            out.print(datum + " ");
        }

    }

}
