package chapter11.parallelForkJoinFramework;

import java.time.Instant;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

import static java.lang.System.out;

/* Sum numbers in an array of SIZE random numbers
 * from 1 to MAX if number > NUM */
public class SumRecursiveTask extends RecursiveTask<Long> {

    public static final int SIZE = 400_00_000;
    public static final int THRESHOLD = 1_000;
    public static final int MAX = 10;        // array of numbers, 1 - 10
    public static final int NUM = 5;        // sum numbers > 5
    private int[] data;
    private int start;
    private int end;

    public SumRecursiveTask(int[] data, int start, int end) {
        this.data = data;
        this.start = start;
        this.end = end;
    }

    @Override
    protected Long compute() {
        long tempSum = 0;
        if (end - start <= THRESHOLD) {
            for (int i = start; i < end; i++) {
                if (data[i] > NUM) {
                    tempSum += data[i];
                }
            }
            return tempSum;
        } else {
            final int halfWay = ((end - start) / 2) + start;
            final SumRecursiveTask t1 = new SumRecursiveTask(data, start, halfWay);
            final SumRecursiveTask t2 = new SumRecursiveTask(data, halfWay, end);

            t1.fork();                            // queue left half of the task
            final long sum2 = t2.compute();      // compute  right half
            final long sum1 = t1.join();        // compute left half && join
            return sum1 + sum2;

        }
    }

    public static void main(String[] args) {
        final int[] data2Sum = new int[SIZE];
        long sum = 0, startTime, endTime, duration;

        // create an Array of random numbers between 1 and MAX
        for (int i = 0; i < SIZE; i++) {
            data2Sum[i] = ThreadLocalRandom.current().nextInt(MAX) + 1;
        }
        startTime = Instant.now().toEpochMilli();


        // sum numbers with plain old for loop
        for (int i = 0; i < data2Sum.length; i++) {
            if (data2Sum[i] > NUM) {
                sum += data2Sum[i];
            }
        }
        endTime = Instant.now().toEpochMilli();
        duration = endTime - startTime;
        out.println("Summed with for loop in: " + duration + " milliSec; sum is: " + sum);


        // sum numbers with RecursiveTask
        final ForkJoinPool fjp = new ForkJoinPool();
        final SumRecursiveTask action = new SumRecursiveTask(data2Sum, 0, data2Sum.length);
        startTime = Instant.now().toEpochMilli();
        sum = fjp.invoke(action);
        endTime = Instant.now().toEpochMilli();
        duration = endTime - startTime;
        out.println("Summed with Recursive Task in: " + duration + " milliSec; sum is: " + sum);


        // sum numbers with a parallel stream (on the default ForkJoinPool, using all 12 cores of machine)
        final IntStream stream2Sum = IntStream.of(data2Sum);
        startTime = Instant.now().toEpochMilli();
        sum = stream2Sum
                .unordered()
                .parallel()
                .filter(i -> i > NUM)
                .sum();
        endTime = Instant.now().toEpochMilli();
        duration = endTime - startTime;
        out.println("Summed with Parallel Stream in: " + duration + " milliSec; sum is: " + sum);


        // sum numbers with parallel stream, Limiting workers
        final ForkJoinPool fjp1 = new ForkJoinPool(4);
        final IntStream stream2Sum1 = IntStream.of(data2Sum);
        try {
            startTime = Instant.now().toEpochMilli();
            sum = fjp1.submit(
                    () -> stream2Sum1
                            .unordered()
                            .parallel()
                            .filter(i -> i > NUM)
                            .sum()
            ).get();
            endTime = Instant.now().toEpochMilli();
            duration = endTime - startTime;
            out.println("Summed with Parallel Stream with Limited workers in : " + duration + " milliSec; sum is: " + sum);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
